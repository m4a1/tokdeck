 'use strict';

var config = require('../../config');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var AccountFacebook = require('../../models').AccountFacebook;
var Account = require('../../models').Account;
var User = require('../../models').User;
var UserService = require('../user');
var Facebook = require('fb');
var async = require('async');
var logger = require('../../lib/logger')({
    prefix: 'service.accounts.facebook'
});

var AccountFacebookService = function() {

    passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.apiUrl + '/v1/user/signin/facebook/callback'
    }, function(token, tokenSecret, profile, done) {

        profile.token = token;
        profile.tokenSecret = tokenSecret;

        return done(null, profile);
    }));
};

AccountFacebookService.prototype.signin = function(param, callback) {

    logger.info('AccountFacebookService', 'signin', 'start');

    var req = param.req;
    var res = param.res;
    var profile = null;

    return async.series([
        function(next) {

            return passport.authenticate('facebook', {
                scope: ['email', 'public_profile']
            })(req, res, function() {

                profile = req.user;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountFacebookService', 'signin', 'finish');

        if (err) {

            logger.error('AccountFacebookService', 'signin', err);

            return callback(err);
        }

        return callback(null, profile);
    });
};

AccountFacebookService.prototype.getAccountSpecific = function(param, callback) {

    logger.info('AccountFacebookService', 'getAccountSpecific', 'start');

    var guid = param.guid;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountFacebook = null;

    return async.series([
        function(next) {

            var w = {};
            var wUser = {};

            if (userId) {
                wUser = {
                    id: userId
                };
            } else {
                w = {
                    guid: guid
                };
            }

            return AccountFacebook.find({
                where: w,
                include: [
                    {
                        model: Account,
                        required: true,
                        include: [
                            {
                                model: User,
                                required: true,
                                where: wUser
                            }
                        ]
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_accountFacebook) {

                if (!_accountFacebook) {
                    return next({code: 4, message: 'No accountFacebook found', guid: guid}) || null;
                }

                accountFacebook = _accountFacebook;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountFacebookService', 'getAccountSpecific', 'finish');

        if (err) {

            logger.error('AccountFacebookService', 'getAccountSpecific', err);

            return callback(err);
        }

        return callback(null, accountFacebook);
    });
};

AccountFacebookService.prototype.createAccount = function(param, callback) {

    logger.info('AccountFacebookService', 'createAccount', 'start');

    var that = this;
    var profile = param.profile;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountSpecific = null;
    var profileFormatted = {};

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var inc = [];

            if (userId) {
                inc = [{
                    model: Account,
                    where: {
                        userId: userId
                    }
                }];
            }

            return AccountFacebook.findOrCreate({
                where: {
                    guid: profileFormatted.guid
                },
                include: inc,
                defaults: profileFormatted,
                transaction: transaction
            }).spread(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountFacebookService', 'createAccount', 'finish');

        if (err) {

            logger.error('AccountFacebookService', 'createAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific, profileFormatted);
    });
};

AccountFacebookService.prototype.updateAccount = function(param, callback) {

    logger.info('AccountFacebookService', 'updateAccount', 'start');

    var that = this;
    var accountSpecific = param.accountSpecific;
    var transaction = param.transaction;
    var profile = param.profile;
    var profileFormatted = null;

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var d = {};

            ['token', 'name', 'url'].forEach(function(k) {

                if (profileFormatted[k]) {
                    d[k] = profileFormatted[k];
                }
            });

            return accountSpecific.update(d, Object.keys(d), {
                transaction: transaction
            }).then(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountFacebookService', 'updateAccount', 'finish');

        if (err) {

            logger.error('AccountFacebookService', 'updateAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific);
    });
};

AccountFacebookService.prototype.extractProfile = function(param, callback) {

    logger.info('AccountFacebookService', 'extractProfile', 'start');

    var profile = param.profile;
    var profileFormatted = {};

    return async.series([
        function(next) {

            profileFormatted = {
                guid: profile.id,
                name: profile.displayName,
                url: profile.profileUrl,
                imageUrl: null,
                token: profile.token,
                tokenSecret: null,
                password: null
            };

            if (profile.emails && profile.emails.length) {
                profileFormatted.email = profile.emails[0].value;
            }

            return next();
        },
        function(next) {

            Facebook.setAccessToken(profileFormatted.token);

            return Facebook.api(profileFormatted.guid + '/picture', {
                redirect: false
            }, function(result) {

                if (!result || !result.data || !result.data.url) {
                    return next();
                }

                profileFormatted.imageUrl = result.data.url;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountFacebookService', 'extractProfile', 'finish');

        if (err) {

            logger.error('AccountFacebookService', 'extractProfile', err);

            return callback(err);
        }

        return callback(null, profileFormatted);
    });
};

module.exports = AccountFacebookService;