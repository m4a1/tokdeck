 'use strict';

var config = require('../../config');
var passport = require('passport');
var GooglePlusStrategy = require('passport-google-plus');
var OAuth2 = require('googleapis').auth.OAuth2;
var AccountGoogle = require('../../models').AccountGoogle;
var Account = require('../../models').Account;
var User = require('../../models').User;
var UserService = require('../user');
var async = require('async');
var logger = require('../../lib/logger')({
    prefix: 'service.accounts.google'
});

var AccountGoogleService = function() {

    passport.use(new GooglePlusStrategy({
        clientId: config.google.clientId,
        clientSecret: config.google.clientSecret,
        redirectUri: config.apiUrl + '/v1/user/signin/google/callback'
    }, function(tokens, profile, done) {

        profile.token = tokens.access_token;

        return done(null, profile);
    }));
};

AccountGoogleService.prototype.signin = function(params, callback) {

    logger.info('AccountGoogleService', 'signin', 'start');

    var req = params.req;
    var res = params.res;
    var action = params.action;
    var profile = null;

    // This is fix for swagger + passport-google-plus params issue
    req.params = req.body = { code: null };

    return async.series([
        function(next) {

            if (action) {
                return next();
            }

            var oauth2Client = new OAuth2(config.google.clientId, config.google.clientSecret, config.apiUrl + '/v1/user/signin/google/callback');

            return res.redirect(oauth2Client.generateAuthUrl({ scope: [ 'email' ] }));
        },
        function(next) {

            return passport.authenticate('google')(req, res, function() {

                profile = req.user;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountGoogleService', 'signin', 'finish');

        if (err) {

            logger.error('AccountGoogleService', 'signin', err);

            return callback(err);
        }

        return callback(null, profile);
    });
};

AccountGoogleService.prototype.getAccountSpecific = function(param, callback) {

    logger.info('AccountGoogleService', 'getAccountSpecific', 'start');

    var guid = param.guid;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountGoogle = null;

    return async.series([
        function(next) {

            var w = {};
            var wUser = {};

            if (userId) {
                wUser = {
                    id: userId
                };
            } else {
                w = {
                    guid: guid
                };
            }

            return AccountGoogle.find({
                where: w,
                include: [
                    {
                        model: Account,
                        required: true,
                        include: [
                            {
                                model: User,
                                required: true,
                                where: wUser
                            }
                        ]
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_accountGoogle) {

                if (!_accountGoogle) {
                    return next({code: 4, message: 'No accountGoogle found', guid: guid}) || null;
                }

                accountGoogle = _accountGoogle;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountGoogleService', 'getAccountSpecific', 'finish');

        if (err) {

            logger.error('AccountGoogleService', 'getAccountSpecific', err);

            return callback(err);
        }

        return callback(null, accountGoogle);
    });
};

AccountGoogleService.prototype.createAccount = function(param, callback) {

    logger.info('AccountGoogleService', 'createAccount', 'start');

    var that = this;
    var userId = param.userId;
    var profile = param.profile;
    var transaction = param.transaction;
    var accountSpecific = null;
    var profileFormatted = {};

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var inc = [];

            if (userId) {
                inc = [{
                    model: Account,
                    where: {
                        userId: userId
                    }
                }];
            }

            return AccountGoogle.findOrCreate({
                where: {
                    guid: profileFormatted.guid
                },
                include: inc,
                defaults: profileFormatted,
                transaction: transaction
            }).spread(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next();
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err});
            });
        }
    ], function(err) {

        logger.info('AccountGoogleService', 'createAccount', 'finish');

        if (err) {

            logger.error('AccountGoogleService', 'createAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific, profileFormatted);
    });
};

AccountGoogleService.prototype.updateAccount = function(param, callback) {

    logger.info('AccountGoogleService', 'updateAccount', 'start');

    var that = this;
    var accountSpecific = param.accountSpecific;
    var transaction = param.transaction;
    var profile = param.profile;
    var profileFormatted = null;

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var d = {};

            ['token', 'name', 'url'].forEach(function(k) {

                if (profileFormatted[k]) {
                    d[k] = profileFormatted[k];
                }
            });

            return accountSpecific.update(d, Object.keys(d), {
                transaction: transaction
            }).then(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountGoogleService', 'updateAccount', 'finish');

        if (err) {

            logger.error('AccountGoogleService', 'updateAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific);
    });
};

AccountGoogleService.prototype.extractProfile = function(param, callback) {

    logger.info('AccountGoogleService', 'extractProfile', 'start');

    var profile = param.profile;
    var profileFormatted = {};

    return async.series([
        function(next) {

            profileFormatted = {
                guid: profile.id,
                name: profile.displayName,
                url: profile.url,
                email: profile.email,
                imageUrl: null,
                token: profile.token,
                tokenSecret: null,
                password: null
            };

            if (profile.image && profile.image.url) {
                profileFormatted.imageUrl = profile.image.url.replace(/(\?|\&)sz\=\d+/gi, '');
            }

            return next();
        }
    ], function(err) {

        logger.info('AccountGoogleService', 'extractProfile', 'finish');

        if (err) {

            logger.error('AccountGoogleService', 'extractProfile', err);

            return callback(err);
        }

        return callback(null, profileFormatted);
    });
};

module.exports = AccountGoogleService;