 'use strict';

var config = require('../../config');
var credential = require('credential');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var AccountLocal = require('../../models').AccountLocal;
var Account = require('../../models').Account;
var User = require('../../models').User;
var UserService = require('../../services/user');
var async = require('async');
var logger = require('../../lib/logger')({
    prefix: 'service.accounts.local'
});

var AccountLocalService = function() {

    var that = this;

    passport.use(new LocalStrategy({
        passReqToCallback: true
    }, function(req, username, password, done) {

        if (typeof req.body.name === 'undefined') {

            return that.verifySignin({
                profile: req.body
            }, done);
        }

        return that.verifySignup({
            profile: req.body
        }, done);
    }));
};

AccountLocalService.prototype.signin = function(param, callback) {

    logger.info('AccountLocalService', 'signin', 'start');

    var req = param.req;
    var res = param.res;
    var profile = null;

    return async.series([
        function(next) {

            return passport.authenticate('local')(req, res, function(err) {

                if (err) {
                    return next(err);
                }

                if (!req.user) {
                    return next({code: 0, message: 'Profile Not Found'});
                }

                profile = req.user;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'signin', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'signin', err);

            return callback(err);
        }

        return callback(null, profile);
    });
};

AccountLocalService.prototype.getAccountSpecific = function(param, callback) {

    logger.info('AccountLocalService', 'getAccountSpecific', 'start');

    var guid = param.guid;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountLocal = null;

    return async.series([
        function(next) {

            var w = {};
            var wUser = {};

            if (userId) {
                wUser = {
                    id: userId
                };
            } else {
                w = {
                    guid: guid
                };
            }

            return AccountLocal.find({
                where: w,
                include: [
                    {
                        model: Account,
                        required: true,
                        include: [
                            {
                                model: User,
                                required: true,
                                where: wUser
                            }
                        ]
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_accountLocal) {

                if (!_accountLocal) {
                    return next({code: 4, message: 'No accountLocal found', guid: guid}) || null;
                }

                accountLocal = _accountLocal;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'getAccountSpecific', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'getAccountSpecific', err);

            return callback(err);
        }

        return callback(null, accountLocal);
    });
};

AccountLocalService.prototype.createAccount = function(param, callback) {

    logger.info('AccountLocalService', 'createAccount', 'start');

    var that = this;
    var userService = new UserService();
    var profile = param.profile;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountSpecific = null;
    var profileFormatted = {};

    return async.series([
        function (next) {

            return userService.validateUsername(profile.username, {}, next);
        },
        function(next) {

            return userService.validatePassword(profile.password, next);
        },
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var inc = [];

            if (userId) {
                inc = [{
                    model: Account,
                    where: {
                        userId: userId
                    }
                }];
            }

            return AccountLocal.findOrCreate({
                where: {
                    guid: profileFormatted.guid
                },
                include: inc,
                defaults: profileFormatted,
                transaction: transaction
            }).spread(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'createAccount', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'createAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific, profileFormatted);
    });
};

AccountLocalService.prototype.updateAccount = function(param, callback) {

    logger.info('AccountLocalService', 'updateAccount', 'start');

    var that = this;
    var userService = new UserService();
    var accountSpecific = param.accountSpecific;
    var transaction = param.transaction;
    var profile = param.profile;
    var profileFormatted = null;

    return async.series([
        function (next) {

            return userService.validateUsername(profile.username, accountSpecific, next);
        },
        function(next) {

            return userService.validatePassword(profile.password, next);
        },
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var d = {};

            ['name', 'password'].forEach(function(k) {

                if (profileFormatted[k]) {
                    d[k] = profileFormatted[k];
                }
            });

            return accountSpecific.update(d, Object.keys(d), {
                transaction: transaction
            }).then(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'updateAccount', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'updateAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific);
    });
};

AccountLocalService.prototype.extractProfile = function(param, callback) {

    logger.info('AccountLocalService', 'extractProfile', 'start');

    var profile = param.profile;
    var profileFormatted = {};

    return async.series([
        function(next) {

            profileFormatted = {
                guid: profile.username,
                username: profile.username,
                name: profile.name,
                email: profile.email,
                url: config.siteUrl + '/users/' + profile.username,
                imageUrl: null,
                token: null,
                tokenSecret: null,
                password: null
            };

            if (!profile.password) {
                return next();
            }

            return credential.hash(profile.password, function (err, hash) {

                if (err) {
                    return next(err);
                }

                profileFormatted.password = hash;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'extractProfile', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'extractProfile', err);

            return callback(err);
        }

        return callback(null, profileFormatted);
    });
};

AccountLocalService.prototype.verifySignin = function(param, callback) {

    logger.info('AccountLocalService', 'verifySignin', 'start');

    var profile = param.profile;
    var result = null;
    var accountLocal = null;

    return async.series([
        function(next) {

            return AccountLocal.find({
                where: {
                    guid: profile.username
                }
            }).then(function(_accountLocal) {

                if (!_accountLocal) {
                    return next({code: 0, message: 'User Not Found'}) || null;
                }

                accountLocal = _accountLocal;

                return next();
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return credential.verify(accountLocal.password, profile.password, function (err, isValid) {

                if (err) {
                    return next(err);
                }

                if (!isValid) {
                    return next({code: 0, message: 'Password Incorrect'});
                }

                result = profile;
                result.id = profile.username;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountLocalService', 'verifySignin', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'verifySignin', err);

            return callback(err);
        }

        return callback(null, result);
    });
};

AccountLocalService.prototype.verifySignup = function(param, callback) {

    logger.info('AccountLocalService', 'verifySignup', 'start');

    var profile = param.profile;
    var result = null;

    return async.series([
        function(next) {

            result = profile;
            result.id = profile.username;

            return next();
        }
    ], function(err) {

        logger.info('AccountLocalService', 'verifySignup', 'finish');

        if (err) {

            logger.error('AccountLocalService', 'verifySignup', err);

            return callback(err);
        }

        return callback(null, result);
    });
};

module.exports = AccountLocalService;