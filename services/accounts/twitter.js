 'use strict';

var config = require('../../config');
var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;
var AccountTwitter = require('../../models').AccountTwitter;
var Account = require('../../models').Account;
var User = require('../../models').User;
var UserService = require('../user');
var async = require('async');
var logger = require('../../lib/logger')({
    prefix: 'service.accounts.twitter'
});

var AccountTwitterService = function() {

    passport.use(new TwitterStrategy({
        consumerKey: config.twitter.consumerKey,
        consumerSecret: config.twitter.consumerSecret,
        callbackURL: config.apiUrl + '/v1/user/signin/twitter/callback'
    }, function(token, tokenSecret, profile, done) {

        profile.token = token;
        profile.tokenSecret = tokenSecret;

        return done(null, profile);
    }));
};

AccountTwitterService.prototype.signin = function(param, callback) {

    logger.info('AccountTwitterService', 'signin', 'start');

    var req = param.req;
    var res = param.res;
    var profile = null;

    return async.series([
        function(next) {

            return passport.authenticate('twitter')(req, res, function() {

                profile = req.user;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountTwitterService', 'signin', 'finish');

        if (err) {

            logger.error('AccountTwitterService', 'signin', err);

            return callback(err);
        }

        return callback(null, profile);
    });
};

AccountTwitterService.prototype.getAccountSpecific = function(param, callback) {

    logger.info('AccountTwitterService', 'getAccountSpecific', 'start');

    var guid = param.guid;
    var userId = param.userId;
    var transaction = param.transaction;
    var accountTwitter = null;

    return async.series([
        function(next) {

            var w = {};
            var wUser = {};

            if (userId) {
                wUser = {
                    id: userId
                };
            } else {
                w = {
                    guid: guid
                };
            }

            return AccountTwitter.find({
                where: w,
                include: [
                    {
                        model: Account,
                        required: true,
                        include: [
                            {
                                model: User,
                                required: true,
                                where: wUser
                            }
                        ]
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_accountTwitter) {

                if (!_accountTwitter) {
                    return next({code: 4, message: 'No accountTwitter found', guid: guid}) || null;
                }

                accountTwitter = _accountTwitter;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountTwitterService', 'getAccountSpecific', 'finish');

        if (err) {

            logger.error('AccountTwitterService', 'getAccountSpecific', err);

            return callback(err);
        }

        return callback(null, accountTwitter);
    });
};

AccountTwitterService.prototype.createAccount = function(param, callback) {

    logger.info('AccountTwitterService', 'createAccount', 'start');

    var that = this;
    var userId = param.userId;
    var profile = param.profile;
    var transaction = param.transaction;
    var accountSpecific = null;
    var profileFormatted = {};

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var inc = [];

            if (userId) {
                inc = [{
                    model: Account,
                    where: {
                        userId: userId
                    }
                }];
            }

            return AccountTwitter.findOrCreate({
                where: {
                    guid: profileFormatted.guid
                },
                include: inc,
                defaults: profileFormatted,
                transaction: transaction
            }).spread(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountTwitterService', 'createAccount', 'finish');

        if (err) {

            logger.error('AccountTwitterService', 'createAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific, profileFormatted);
    });
};

AccountTwitterService.prototype.updateAccount = function(param, callback) {

    logger.info('AccountTwitterService', 'updateAccount', 'start');

    var that = this;
    var accountSpecific = param.accountSpecific;
    var transaction = param.transaction;
    var profile = param.profile;
    var profileFormatted = null;

    return async.series([
        function(next) {

            return that.extractProfile({
                profile: profile
            }, function(err, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            var d = {};

            ['token', 'tokenSecret', 'name', 'url'].forEach(function(k) {

                if (profileFormatted[k]) {
                    d[k] = profileFormatted[k];
                }
            });

            return accountSpecific.update(d, Object.keys(d), {
                transaction: transaction
            }).then(function(_accountSpecific) {

                accountSpecific = _accountSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AccountTwitterService', 'updateAccount', 'finish');

        if (err) {

            logger.error('AccountTwitterService', 'updateAccount', err);

            return callback(err);
        }

        return callback(null, accountSpecific);
    });
};

AccountTwitterService.prototype.extractProfile = function(param, callback) {

    logger.info('AccountTwitterService', 'extractProfile', 'start');

    var profile = param.profile;
    var profileFormatted = {};

    return async.series([
        function(next) {

            profileFormatted = {
                guid: profile.id,
                username: profile.username,
                name: profile.displayName,
                url: 'https://twitter.com/' + profile.username,
                imageUrl: null,
                token: profile.token,
                tokenSecret: profile.tokenSecret,
                password: null
            };

            if (profile._json && profile._json.profile_image_url) {
                profileFormatted.imageUrl = profile._json.profile_image_url;
            }

            return next();
        }
    ], function(err) {

        logger.info('AccountTwitterService', 'extractProfile', 'finish');

        if (err) {

            logger.error('AccountTwitterService', 'extractProfile', err);

            return callback(err);
        }

        return callback(null, profileFormatted);
    });
};

module.exports = AccountTwitterService;