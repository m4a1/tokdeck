'use strict';

var config = require('../config');
var _ = require('lodash');
var Lang = require('../models').Lang;
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.lang'
});

var LangService = function() {

    var that = this;

    this.getLangs = function(param, callback) {

        var list = [];

        return async.series([
            function(next) {

                return Lang.findAll({
                    order: '`isDefault` DESC'
                }).then(function(_langs) {

                    if (_langs) {
                        list = _langs;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {
            return callback(error, list);
        });
    };

    this.getLang = function(param, callback) {

        var langId = param.langId;
        var isDefault = param.isDefault;
        var lang = null;

        return async.series([
            function(next) {

                var w = null;

                if (langId) {
                    w = {
                        id: langId
                    };
                }

                if (isDefault) {
                    w = {
                        isDefault: true
                    };
                }

                if (!w) {
                    return next({code: 0, message: 'Lang not found'});
                }

                return Lang.find({
                    where: w
                }).then(function(_lang) {

                    if (!_lang) {
                        return next({code: 0, message: 'Lang not found'}) || null;
                    }

                    lang = _lang;

                    return next() || null;

                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {
            return callback(error, lang);
        });
    };
};

module.exports = LangService;