'use strict';

var config = require('../config');
var _ = require('lodash');
var Lang = require('../models').Lang;
var Locale = require('../models').Locale;
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.locale'
});

var LocaleService = function() {

    var that = this;

    this.getLocale = function(param, callback) {

        var langId = param.langId;
        var stitle = param.stitle;
        var langDefault = null;
        var list = {};

        async.series([
            function(next) {

                return Lang.find({
                    where: {
                        isDefault: true
                    }
                }).then(function(_lang) {

                    if (!_lang) {
                        return next({code: 0, message: 'Lang default not found'}) || null;
                    }

                    langDefault = _lang;

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                var w = {
                    langId: langDefault.id
                };

                if (stitle) {
                    w.stitle = stitle;
                }

                return Locale.findAll({
                    where: w
                }).then(function(_locales) {

                    if (!_locales) {
                        return next() || null;
                    }

                    _locales.forEach(function(locale) {
                        list[locale.stitle] = locale.value;
                    });

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                if (langDefault.id === langId || !langId) {
                    return next();
                }

                var w = {
                    langId: langId
                };

                if (stitle) {
                    w.stitle = stitle;
                }

                return Locale.findAll({
                    where: w
                }).then(function(_locales) {

                    if (!_locales) {
                        return next() || null;
                    }

                    _locales.forEach(function(locale) {
                        list[locale.stitle] = locale.value;
                    });

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {
            return callback(error, list);
        });
    };
};

module.exports = LocaleService;