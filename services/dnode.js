'use strict';

var config = require('../config');
var dnode = require('dnode');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.dnode'
});

var DnodeService = function() {

    var points = {};
    var connections = {};

    this.start = function(name, callback) {

        if (!config.dnode.instances[name]) {

            if (callback) {
                callback({code: 0, message: 'unknown instance'});
            }

            return;
        }

        var server = dnode({
            notify : function (request, cb) {

                var secret = request.secret;
                var point = request.point;
                var data = request.data;

                if (secret !== config.dnode.secret) return cb({error: {code: 0, message: 'secret incorrect'}});
                if (!points[point]) return cb({error: {code: 0, message: 'point not found'}});

                points[point].call(null, data, function(error, result) {

                    if (error) {
                        return cb({error: error});
                    }

                    return cb({result: result});
                });
            }
        });

        server.listen(config.dnode.instances[name].port);

        if (callback) {
            return callback();
        }
    };

    this.notify = function(name, point, data, callback) {

        if (!config.dnode.instances[name]) {
            return callback ? callback({code: 0, message: 'unknown instance'}) : null;
        }

        return async.series([function(next) {

            if (connections[name]) {
                return next();
            }

            var d = dnode.connect(config.dnode.instances[name].port);

            d.on('end', function () {

                if (typeof connections[name] === 'object') {
                    delete connections[name];
                }
            });

            d.on('fail', function () {

                if (typeof connections[name] === 'object') {
                    delete connections[name];
                }

                if (callback) {
                    return callback({code: 0, message: name + ' is offline'});
                }
            });

            d.on('error', function () {

                if (typeof connections[name] === 'object') {
                    delete connections[name];
                }

                if (callback) {
                    return callback({code: 0, message: name + ' is offline'});
                }
            });

            d.on('remote', function (remote) {

                connections[name] = remote;

                return next();
            });
        }], function() {

            if (!connections[name]) {
                return callback ? callback({code: 0, message: 'connection error'}) : null;
            }

            connections[name].notify({
                secret: config.dnode.secret,
                point: point,
                data: data
            }, function(result) {

                if (callback) {
                    return callback(result.error, result.result);
                }
            });
        });
    };

    this.on = function(point, callback) {
        points[point] = callback;
    };
};

module.exports = DnodeService;
