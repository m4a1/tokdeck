'use strict';

var config = require('../config');
var Mail = require('../models').Mail;
var WebService = require('./web');
var async = require('async');
var _ = require('lodash');
var LocaleService = require('./locale');
var SparkPost = require('sparkpost');
var logger = require('../lib/logger')({
    prefix: 'service.mail'
});

var MailService = function(){

    var that = this;

    this.getById = function(id, callback) {

        return Mail.find({
            where: {
                id: id
            }
        }).then(function(_mail) {

            return callback(null, _mail) || null;
        }).catch(function(err) {
            
            return callback({code: 0, message: 'DB error', original: err}) || null;
        });
    };

    this.renderMail = function(param, callback) {

        var webService = new WebService();
        var template = param.template, data = param.data;
        var html = null;
        var content = '';

        return async.series([
            function(next) {

                if (!template) {
                    return next({code: 0, message: 'No template provided'});
                }

                return webService.render({
                    file: 'mails/' + template,
                    data: data
                }, function(error, result) {

                    if (error) {
                        return next(error);
                    }

                    content = result.html;

                    return next();
                });
            },
            function(next) {

                return webService.render({
                    file: 'mails/layout',
                    data: {
                        content: content
                    }
                }, function(error, result) {

                    if (error) {
                        return next(error);
                    }

                    html = result.html;

                    return next();
                });
            }
        ], function(error) {
            return callback(error, html);
        });
    };

    this.sendMail = function(param, callback) {

        var template = param.template;
        var data = param.data;
        var email = param.email;
        var name = param.name;
        var subject = param.subject;
        var html = null;
        var result = null;

        async.series([
            function(next) {

                return that.renderMail({
                    template: template,
                    data: data
                }, function(error, _html) {

                    if (error) {
                        return next(error);
                    }

                    html = _html;

                    return next();
                });
            },
            function(next) {

                var sparkpost_client = new SparkPost(config.sparkpost.apikey);

                return sparkpost_client.transmissions.send({
                    transmissionBody: {
                        content: {
                            subject: subject,
                            html: html,
                            from: {
                                email: config.sparkpost.from_email,
                                name: config.sparkpost.from_name
                            }
                        },
                        recipients: [
                            {
                                address: {
                                    email: email,
                                    name: name
                                }
                            }
                        ]
                    }
                }, function(err, _result) {

                    if (err) {
                        return next(err);
                    }

                    result = _result;

                    return next();
                });
            }
        ], function(error) {
            return callback(error, result);
        });
    };

    this.mailUserConfirmation = function(param, callback) {

        var localeService = new LocaleService();

        var user = param.user;
        var confirmationLink = param.confirmationLink;
        var confirmationPin = param.confirmationPin;
        var changed = param.changed;
        var locale = {};
        var result = null;

        return async.series([
            function(next) {

                return localeService.getLocale({
                    langId: user.langId,
                    stitle: ['mail_userconfirmation_subject', 'mail_userconfirmation_subject_changed']
                }, function(error, _locale) {

                    if (error) {
                        return next(error);
                    }

                    locale = _locale;

                    return next();
                });
            },
            function(next) {

                return that.sendMail({
                    subject: changed ? locale.mail_userconfirmation_subject_changed : locale.mail_userconfirmation_subject,
                    email: user.email,
                    name: user.name,
                    template: 'userconfirmation',
                    data: {
                        name: user.name,
                        confirmationPin: confirmationPin,
                        confirmationLink: confirmationLink,
                        changed: changed
                    }
                }, function(error, _result) {

                    if (error) {
                        return next(error);
                    }

                    if (_result) {
                        result = _result;
                    }

                    return next();
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, result);
            }
        });
    };

    this.mailUserRemind = function(param, callback) {

        var localeService = new LocaleService();
        var user = param.user;
        var remindLink = param.remindLink;
        var remindPin = param.remindPin;
        var locale = {};
        var result = null;

        return async.series([
            function(next) {

                return localeService.getLocale({
                    langId: user.langId,
                    stitle: 'mail_userremind_subject'
                }, function(error, _locale) {

                    if (error) {
                        return next(error);
                    }

                    locale = _locale;

                    return next();
                });
            },
            function(next) {

                return that.sendMail({
                    subject: locale.mail_userremind_subject,
                    email: user.email,
                    name: user.name,
                    template: 'userremind',
                    data: {
                        name: user.name,
                        remindPin: remindPin,
                        remindLink: remindLink
                    }
                }, function(error, _result) {

                    if (error) {
                        return next(error);
                    }

                    if (_result) {
                        result = _result;
                    }

                    return next();
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, result);
            }
        });
    };
};

module.exports = MailService;