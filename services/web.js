'use strict';

var config = require('../config');
var _ = require('lodash');
var ejs = require('ejs');
var fs = require('fs');
var async = require('async');
var LocaleService = require('./locale');
var CacheService = require('./cache');
var logger = require('../lib/logger')({
    prefix: 'service.web'
});

var WebService = function () {};

WebService.prototype.render = function (param, callback) {

    var langId = param.langId;
    var data = param.data || {};
    var template = param.template;
    var file = param.file;
    var localeService = new LocaleService();
    var cacheService = new CacheService();
    var html = '';
    var locale = null;

    return async.series([
        function(next) {

            return cacheService.get({
                type: 'locale_' + langId
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                if (result) {
                    locale = result;
                }

                return next();
            });
        },
        function(next) {

            if (locale) {
                return next();
            }

            return localeService.getLocale({
                langId: langId
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                if (!result) {
                    return next({code: 0, message: 'Locale not loaded'});
                }

                locale = result;

                return cacheService.set({
                    type: 'locale_' + langId,
                    data: result
                }, next);
            });
        },
        function(next) {

            _.extend(data, {
                config: config,
                locale: locale,
                nav: [
                    {
                        url: '/dashboard',
                        title: locale.page_dashboard_title
                    },
                    {
                        url: '/about',
                        title: locale.page_about_title
                    },
                    {
                        url: '/contacts',
                        title: locale.page_contacts_title
                    }
                ]
            });

            return next();
        },
        function(next) {

            if (!file) {
                return next();
            }

            return fs.readFile('./views/' + file + '.ejs', function(err, _template) {

                if (err) {
                    return next({code: 0, message: 'Template not found', original: err});
                }

                template = new String(_template);

                return next();
            });
        },
        function(next) {

            try {

                html = ejs.render(template, data, {
                    filename: './views/index.ejs'
                });
            }
            catch (e) {
                return next({code: 0, message: 'Template error', original: e});
            }

            return next();
        }
    ], function(error) {

        return callback(error, {
            html: html
        });
    });
};

module.exports = WebService;