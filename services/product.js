'use strict';

var config = require('../config');
var Product = require('../models').Product;
var Auction = require('../models').Auction;
var async = require('async');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'service.product'
});

var ProductService = function() {};

ProductService.prototype.addProduct = function(param, callback) {

    var product = null;
    var userId = parseInt(param.userId);
    var auctionId = parseInt(param.auctionId);
    var oid = param.oid;
    var d = _.extend({}, param);

    return async.series([
        function(next) {

            d.price = parseFloat(d.price);

            if (!userId) {
                return next({code: 0, message: 'User id not set'});
            }

            if (!auctionId) {
                return next({code: 0, message: 'Auction id not set'});
            }

            if (!oid) {
                return next({code: 0, message: 'Oid is not set'});
            }

            if (!d.price) {
                return next({code: 0, message: 'Price is not set'});
            }

            return next();
        },
        function(next) {

            return Auction.find({
                where: {
                    id: auctionId
                }
            }).then(function(auction) {

                if (auction.userId !== userId) {
                    return next({code: 0, message: 'Product\'s Auction does not belong to you'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return Product.count({
                where: {
                    auctionId: auctionId,
                    oid: oid
                }
            }).then(function(exists) {

                if (exists) {
                    return next({code: 0, message: 'Product with this oid already exists'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return Product.create(d).then(function(_product) {

                if (!_product) {
                    return next({code: 0, message: 'Product was not created'}) || null;
                }

                product = _product;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, product);
        }
    });
};

ProductService.prototype.updateProduct = function(param, callback) {

    var that = this;
    var product = null;
    var productId = parseInt(param.productId);
    var userId = parseInt(param.userId);
    var oid = param.oid;
    var price = param.price;
    var d = _.extend({}, param);

    return async.series([
        function(next) {

            for (var k in d) {

                if (typeof d[k] === 'undefined') {
                    delete d[k];
                } else if (!String(d[k]).length || String(d[k]) === String(config.auction.def[k])) {
                    d[k] = null;
                }
            }

            if (!productId) {
                return next({code: 0, message: 'Product id not set'});
            }

            if (typeof oid !== 'undefined' && !oid) {
                return next({code: 0, message: 'Oid is not set'});
            }

            if (typeof price !== 'undefined' && !parseFloat(price)) {
                return next({code: 0, message: 'Price is not set'});
            }

            return next();
        },
        function(next) {

            return that.getProduct({
                productId: productId,
                userId: userId
            }, function(err, _product) {

                if (err) {
                    return next(err);
                }

                if (!_product) {
                    return next({code: 0, message: 'Product not found'});
                }

                product = _product;

                return next();
            });
        },
        function(next) {

            if (typeof oid === 'undefined') {
                return next();
            }

            return Product.count({
                where: {
                    auctionId: product.auctionId,
                    id: {
                        ne: productId
                    },
                    oid: oid
                }
            }).then(function(exists) {

                if (exists) {
                    return next({code: 0, message: 'Product with this oid already exists'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return product.updateAttributes(d, Object.keys(d)).then(function(_product) {

                product = _product;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, product);
        }
    });
};

ProductService.prototype.removeProduct = function(param, callback) {

    var that = this;
    var product = null;
    var userId = param.userId;
    var productId = parseInt(param.productId);

    return async.series([
        function(next) {

            if (!productId) {
                return next({code: 0, message: 'Product id not set'});
            }

            return next();
        },
        function(next) {

            return that.getProduct({
                productId: productId,
                userId: userId
            }, function(err, _product) {

                if (err) {
                    return next(err);
                }

                if (!_product) {
                    return next({code: 0, message: 'Product not found'});
                }

                product = _product;

                return next();
            });
        },
        function(next) {

            return product.destroy().then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, error ? false : true);
        }
    });
};

ProductService.prototype.getProducts = function(param, callback) {

    var list = [];
    var userId = parseInt(param.userId);
    var auctionId = parseInt(param.auctionId);

    return async.series([
        function(next) {

            if (!userId) {
                return next({code: 0, message: 'User id not set'});
            }

            if (!auctionId) {
                return next({code: 0, message: 'Auction id not set'});
            }

            return next();
        },
        function(next) {

            return Product.findAll({
                where: {
                    auctionId: auctionId
                },
                include: [
                    {
                        model: Auction,
                        where: {
                            userId: userId
                        }
                    }
                ]
            }).then(function(_list) {

                if (_list && _list.length) {
                    list = _list;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, list);
        }
    });
};

ProductService.prototype.getProduct = function(param, callback) {

    var product = null;
    var productId = parseInt(param.productId);
    var userId = parseInt(param.userId);

    return async.series([
        function(next) {

            if (!productId) {
                return next({code: 0, message: 'Product id not set'});
            }

            return next();
        },
        function(next) {

            return Product.find({
                where: {
                    id: productId
                },
                include: [
                    {
                        model: Auction
                    }
                ]
            }).then(function(_product) {

                if (!_product) {
                    return next({code: 0, message: 'Product Not Found'});
                }

                product = _product;


                if (product.Auction.userId !== userId) {
                    return next({code: 0, message: 'Product does not belong to you'});
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, product);
        }
    });
};

module.exports = ProductService;