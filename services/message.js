'use strict';

var config = require('../config');
var Message = require('../models').Message;
var DnodeService = require('./dnode');
var SocketService = require('./socket');
var async = require('async');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'service.message'
});

var MessageService = function(){

    var that = this;

    this.getById = function(id, callback) {

        return Message.findById(id).then(function(_message) {

            return callback(null, _message) || null;
        }).catch(function(err) {

            return callback({code: 0, message: 'DB error', original: err}) || null;
        });
    };

    this.getMessagesUnread = function(param, callback) {

        var w = '`isRead` = 0 AND (`validUntil` = "0000-00-00 00:00:00" OR `validUntil` > NOW())';

        var w_add = [];

        if (param.userId) {
            w_add.push('`userId` = ' + param.userId);
        }

        if (param.tid) {
            w_add.push('`tid` = "' + param.tid + '"');
        }

        w += ' AND (' + (w_add.length ? w_add.join(' OR ') : '1=0') + ')';

        return Message.findAll({
            where: [w]
        }).then(function(_messages) {

            return callback(null, _messages) || null;
        }).catch(function(err) {

            return callback({code: 0, message: 'DB error', original: err}) || null;
        });
    };

    this.addMessage = function(param, callback) {

        var userId = param.userId, tid = param.tid;
        var message = null;
        var d = _.extend({}, param);

        return async.series([
            function(next) {

                if (!d.stitle) {
                    return next();
                }

                return that.readMessage({
                    messageStitle: d.stitle,
                    userId: userId,
                    tid: tid
                }, function() {
                    return next();
                });
            },
            function(next) {
                Message.create(d).then(function(_message) {

                    if (_message) {
                        message = _message;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                if (!message) {
                    return next();
                }

                return that.sendMessage({
                    messageId: message.id
                }, next);
            }
        ], function(error) {

            if (callback) {
                return callback(error, message);
            }
        });
    };

  this.sendMessage = function(param, callback) {

        var dnodeService = new DnodeService();
        var messageId = param.messageId;
        var message = null;

        return async.series([
            function(next) {

                return dnodeService.notify('socket', 'messageSent', {id: messageId}, function(error, _message) {

                    if (error) {
                        return next(error);
                    }

                    message = _message;

                    return next();
                });
            }
        ], function(error) {
            return callback(error, message);
        });
    };

    this.readMessage = function(param, callback) {

        var messageId = param.messageId;
        var userId = param.userId;
        var tid = param.tid;
        var messageStitle = param.messageStitle;
        var messages = null;

        return async.series([
            function(next) {

                var w = '`isRead` = 0';

                if (messageId) {
                    w += ' AND `id` = ' + messageId;
                } else if ((userId || tid) && messageStitle) {

                    w += ' AND `stitle` = "' + messageStitle + '"';

                    var w_add = [];

                    if (userId) {
                        w_add.push('`userId` = ' + userId);
                    }

                    if (tid) {
                        w_add.push('`tid` = "' + tid + '"');
                    }

                    w += ' AND (' + w_add.join(' OR ') + ')';
                } else {
                    return next({code: 0, message: 'Condition invalid'});
                }

                return Message.findAll({
                    where: w
                }).then(function(_messages) {

                    messages = _messages;

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                if (!messages || !messages.length) {
                    return next();
                }

                return async.eachSeries(messages, function(message, _next) {

                    return message.update({
                        isRead: true
                    }, [
                        'isRead'
                    ]).then(function() {

                        return _next() || null;
                    }).catch(function() {

                        return _next() || null;
                    });
                }, function(error) {
                    return next(error);
                });
            }
        ], function(error) {
            return callback(error, messages);
        });
    };
};

module.exports = MessageService;