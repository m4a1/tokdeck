'use strict';

var config = require('../config');
var sorted = require('sorted');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.queue'
});

var QueueService = function(resolution) {

    var that = this;

    this.resolution = resolution || 100;
    this.started = false;
    this.queueArray = sorted([]);
    this.queue = {};

    this.add = function(name, date, func) {

        var key = date.getTime();

        if (typeof that.queue[key] === 'undefined') {

            that.queue[key] = {};
            that.queue[key][name] = func;

            that.queueArray.push(key);
        } else {
            that.queue[key][name] = func;
        }

        if (!that.started) {

            that.started = true;

            that.tick();
        }
    };

    this.remove = function(name, date) {

        var key = date.getTime();

        if (typeof that.queue[key] !== 'undefined') {

            var cnt = 0;

            if (name) {

                delete that.queue[key][name];

                for (var k in that.queue[key]) {

                    cnt += 1;
                    k += 1;

                    break;
                }
            }

            if (!cnt) {

                var ind = that.queueArray.indexOf(key);

                if (ind !== -1) {
                    that.queueArray.splice(ind, 1);
                }

                delete that.queue[key];

                if (!that.queueArray.length) {
                    that.started = false;
                }
            }
        }
    };

    this.queueFirst = function() {
        return that.queueArray.length ? that.queueArray.get(0) : 0;
    };

    this.tick = function() {

        var now = (new Date()).getTime();
        var first = that.queueFirst();

        if (first && first <= now) {

            for (var k in that.queue[first]) {
                that.queue[first][k].call(this);
            }

            that.remove(null, new Date(first));
        }

        if (that.started) {

            return setTimeout(function() {
                return that.tick();
            }, that.resolution);
        }
    };
};

module.exports = QueueService;
