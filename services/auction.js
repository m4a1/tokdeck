'use strict';

var config = require('../config');
var Auction = require('../models').Auction;
var async = require('async');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'service.auction'
});

var AuctionService = function() {};

AuctionService.prototype.addAuction = function(param, callback) {

    var auction = null;
    var userId = parseInt(param.userId);
    var title = param.title;
    var d = _.extend({}, param);

    return async.series([
        function(next) {

            if (!userId) {
                return next({code: 0, message: 'User id not set'});
            }

            if (!title) {
                return next({code: 0, message: 'Title is not set'});
            }

            return next();
        },
        function(next) {

            return Auction.count({
                where: {
                    userId: userId,
                    title: title
                }
            }).then(function(exists) {

                if (exists) {
                    return next({code: 0, message: 'Auction with this title already exists'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return Auction.create(d).then(function(_auction) {

                if (!_auction) {
                    return next({code: 0, message: 'Auction was not created'}) || null;
                }

                auction = _auction;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, auction);
        }
    });
};

AuctionService.prototype.updateAuction = function(param, callback) {

    var that = this;
    var auction = null;
    var userId = parseInt(param.userId);
    var auctionId = parseInt(param.auctionId);
    var title = param.title;
    var d = _.extend({}, param);

    return async.series([
        function(next) {

            for (var k in d) {

                if (typeof d[k] === 'undefined') {
                    delete d[k];
                } else if (!String(d[k]).length || String(d[k]) === String(config.auction.def[k])) {
                    d[k] = null;
                }
            }

            if (!auctionId) {
                return next({code: 0, message: 'Auction id not set'});
            }

            if (typeof title !== 'undefined' && !title) {
                return next({code: 0, message: 'Title is not set'});
            }

            if (typeof d.status !== 'undefined' && ['stopped', 'running'].indexOf(d.status) === -1) {
                return next({code: 0, message: 'Incorrect status'});
            }

            return next();
        },
        function(next) {

            if (typeof title === 'undefined') {
                return next();
            }

            return Auction.count({
                where: {
                    userId: userId,
                    id: {
                        ne: auctionId
                    },
                    title: title
                }
            }).then(function(exists) {

                if (exists) {
                    return next({code: 0, message: 'Auction with this title already exists'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return that.getAuction({
                auctionId: auctionId
            }, function(err, _auction) {

                if (err) {
                    return next(err);
                }

                if (!_auction) {
                    return next({code: 0, message: 'Auction not found'});
                }

                if (_auction.userId !== userId) {
                    return next({code: 0, message: 'Auction does not belong to you'});
                }

                auction = _auction;

                return next();
            });
        },
        function(next) {

            return auction.updateAttributes(d, Object.keys(d)).then(function(_auction) {

                auction = _auction;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, auction);
        }
    });
};

AuctionService.prototype.removeAuction = function(param, callback) {

    var that = this;
    var auction = null;
    var userId = parseInt(param.userId);
    var auctionId = parseInt(param.auctionId);

    return async.series([
        function(next) {

            if (!auctionId) {
                return next({code: 0, message: 'Auction id not set'});
            }

            return next();
        },
        function(next) {

            return that.getAuction({
                auctionId: auctionId
            }, function(err, _auction) {

                if (err) {
                    return next(err);
                }

                if (!_auction) {
                    return next({code: 0, message: 'Auction not found'});
                }

                if (_auction.userId !== userId) {
                    return next({code: 0, message: 'Auction does not belong to you'});
                }

                auction = _auction;

                return next();
            });
        },
        function(next) {

            return auction.destroy().then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, error ? false : true);
        }
    });
};

AuctionService.prototype.getAuctions = function(param, callback) {

    var list = [];
    var userId = parseInt(param.userId);

    return async.series([
        function(next) {

            if (!userId) {
                return next({code: 0, message: 'User id not set'});
            }

            return next();
        },
        function(next) {

            return Auction.findAll({
                where: {
                    userId: userId
                }
            }).then(function(_list) {

                if (_list && _list.length) {
                    list = _list;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, list);
        }
    });
};

AuctionService.prototype.getAuction = function(param, callback) {

    var auction = null;
    var auctionId = parseInt(param.auctionId);
    var userId = parseInt(param.userId);

    return async.series([
        function(next) {

            if (!auctionId) {
                return next({code: 0, message: 'Auction id not set'});
            }

            return next();
        },
        function(next) {

            return Auction.find({
                where: {
                    id: auctionId
                }
            }).then(function(_auction) {

                if (!_auction) {
                    return next({code: 0, message: 'Auction Not Found'}) || null;
                }

                if (userId && _auction.userId !== userId) {
                    return next({code: 0, message: 'Auction does not belong to you'});
                }

                auction = _auction;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, auction);
        }
    });
};

module.exports = AuctionService;