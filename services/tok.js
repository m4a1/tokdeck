'use strict';

var config = require('../config');
var Tok = require('../models').Tok;
var Auction = require('../models').Auction;
var async = require('async');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'service.tok'
});

var TokService = function(){

    var that = this;

    this.addTok = function(param, callback) {

        var tok = null;
        var userId = parseInt(param.userId);
        var auctionId = parseInt(param.auctionId);
        var oid = param.oid;
        var d = _.extend({}, param);

        return async.series([
            function(next) {

                if (!userId) {
                    return next({code: 0, message: 'User id not set'});
                }

                if (!auctionId) {
                    return next({code: 0, message: 'Auction id not set'});
                }

                if (!oid) {
                    return next({code: 0, message: 'Oid is not set'});
                }

                return next();
            },
            function(next) {

                return Tok.count({
                    where: {
                        auctionId: auctionId,
                        oid: oid
                    },
                    include: [
                        {
                            model: Auction,
                            where: {
                                userId: userId
                            }
                        }
                    ]
                }).then(function(exists) {

                    if (exists) {
                        return next({code: 0, message: 'Tok with this oid already exists'}) || null;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                return Tok.create(d).then(function(_tok) {

                    if (!_tok) {
                        return next({code: 0, message: 'Tok was not created'}) || null;
                    }

                    tok = _tok;

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, tok);
            }
        });
    };

    this.updateTok = function(param, callback) {

        var tok = null;
        var auctionId = parseInt(param.auctionId);
        var tokId = parseInt(param.tokId);
        var userId = parseInt(param.userId);
        var oid = param.oid;
        var d = _.extend({}, param);

        return async.series([
            function(next) {

                for (var k in d) {

                    if (typeof d[k] === 'undefined') {
                        delete d[k];
                    }
                }

                if (!tokId) {
                    return next({code: 0, message: 'Tok id not set'});
                }

                if (!auctionId) {
                    return next({code: 0, message: 'Auction id not set'});
                }

                if (typeof oid !== 'undefined' && !oid) {
                    return next({code: 0, message: 'Oid is not set'});
                }

                return next();
            },
            function(next) {

                if (typeof oid === 'undefined') {
                    return next();
                }

                return Tok.count({
                    where: {
                        auctionId: auctionId,
                        id: {
                            ne: tokId
                        },
                        oid: oid
                    },
                    include: [
                        {
                            model: Auction,
                            where: {
                                userId: userId
                            }
                        }
                    ]
                }).then(function(exists) {

                    if (exists) {
                        return next({code: 0, message: 'Tok with this oid already exists'}) || null;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                return that.getTok({
                    tokId: tokId,
                    auctionId: auctionId
                }, function(err, _tok) {

                    if (err) {
                        return next(err);
                    }

                    if (!_tok) {
                        return next({code: 0, message: 'Tok not found'});
                    }

                    if (_tok.auctionId !== auctionId) {
                        return next({code: 0, message: 'Tok does not belong to auction'});
                    }

                    if (_tok.Auction.userId !== userId) {
                        return next({code: 0, message: 'Tok does not belong to you'});
                    }

                    tok = _tok;

                    return next();
                });
            },
            function(next) {

                return tok.updateAttributes(d, Object.keys(d)).then(function(_tok) {

                    tok = _tok;

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, tok);
            }
        });
    };

    this.removeTok = function(param, callback) {

        var tok = null;
        var userId = param.userId;
        var tokId = parseInt(param.tokId);
        var auctionId = parseInt(param.auctionId);

        return async.series([
            function(next) {

                if (!tokId) {
                    return next({code: 0, message: 'Tok id not set'});
                }

                if (!auctionId) {
                    return next({code: 0, message: 'Auction id not set'});
                }

                return next();
            },
            function(next) {

                return that.getTok({
                    tokId: tokId,
                    auctionId: auctionId
                }, function(err, _tok) {

                    if (err) {
                        return next(err);
                    }

                    if (!_tok) {
                        return next({code: 0, message: 'Tok not found'});
                    }

                    if (_tok.auctionId !== auctionId) {
                        return next({code: 0, message: 'Tok does not belong to auction'});
                    }

                    if (_tok.Auction.userId !== userId) {
                        return next({code: 0, message: 'Tok does not belong to you'});
                    }

                    tok = _tok;

                    return next();
                });
            },
            function(next) {

                return tok.destroy().then(function() {

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {

            if (callback) {
                callback(error, error ? false : true);
            }
        });
    };

    this.getToks = function(param, callback) {

        var list = [];
        var userId = parseInt(param.userId);
        var auctionId = parseInt(param.auctionId);

        return async.series([
            function(next) {

                if (!userId) {
                    return next({code: 0, message: 'User id not set'});
                }

                if (!auctionId) {
                    return next({code: 0, message: 'Auction id not set'});
                }

                return next();
            },
            function(next) {

                return Tok.findAll({
                    where: {
                        auctionId: auctionId
                    },
                    include: [
                        {
                            model: Auction,
                            where: {
                                userId: userId
                            }
                        }
                    ]
                }).then(function(_list) {

                    if (_list && _list.length) {
                        list = _list;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, list);
            }
        });
    };

    this.getTok = function(param, callback) {

        var tok = null;
        var tokId = parseInt(param.tokId);
        var auctionId = parseInt(param.auctionId);

        return async.series([
            function(next) {

                if (!tokId) {
                    return next({code: 0, message: 'Tok id not set'});
                }

                if (!auctionId) {
                    return next({code: 0, message: 'Auction id not set'});
                }

                return next();
            },
            function(next) {

                return Tok.find({
                    where: {
                        id: tokId
                    },
                    include: [
                        {
                            model: Auction,
                            where: {
                                id: auctionId
                            }
                        }
                    ]
                }).then(function(_tok) {

                    if (_tok) {
                        tok = _tok;
                    }

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, tok);
            }
        });
    };
};

module.exports = TokService;