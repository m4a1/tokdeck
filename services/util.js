'use strict';

var config = require('../config');
var async = require('async');
var domain = require('domain');
var request = require('request');
var temp = require('temp');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'service.util'
});

var UtilService = function() {};

UtilService.prototype.downloadImg = function(param, callback) {

    var url = param.url;

    if (!url) {
        return callback({code: 1, message: 'invalid image url'});
    }

    var head = null;
    var path = null;

    return async.series([
        function(next) {

            var domain_dnl = domain.create();

            domain_dnl.on('error', function(error) {

                domain_dnl.exit();

                return next({code: 6, message: 'socket error', original: error});
            });

            domain_dnl.run(function() {

                return request.head({
                    url: url,
                    timeout: 60000
                }, function (error, res) {

                    domain_dnl.exit();

                    if (error || !res) {
                        return next({code: 2, message: 'download error', original: error});
                    }

                    head = res;

                    return next();
                });
            });
        },
        function(next) {

            if (!head.headers || typeof head.headers['content-type'] === 'undefined'/* || typeof head.headers['content-length'] === 'undefined'*/) {
                return next({code: 3, message: 'invalid image headers'});
            }

            var allow = ['image', 'jpeg'];

            var contentType = String(head.headers['content-type']).toLowerCase();

            var found = _.find(allow, function(string) {
                return contentType.indexOf(string) >= 0;
            });

            if (!found/* || head.headers['content-length'] < 100*/) {
                return next({code: 4, message: 'invalid image', contentType: contentType/*, contentLength: res.headers['content-length']*/});
            }

            return next();
        },
        function(next) {

            temp.track();

            var stream = temp.createWriteStream();

            var domain_dnl = domain.create();

            domain_dnl.on('error', function(error) {

                domain_dnl.exit();

                return next({code: 6, message: 'socket error', original: error});
            });

            domain_dnl.run(function() {

                stream.on('error', function (error) {

                    domain_dnl.exit();

                    return next({code: 5, message: 'socket error', original: error});
                });

                stream.on('finish', function () {

                    domain_dnl.exit();

                    path = stream.path;

                    return next();
                });

                return request({
                    url: url,
                    timeout: 60000
                }).pipe(stream);
            });
        }
    ], function(error) {
        return callback(error, path);
    });
};

UtilService.prototype.percentParam = function(v, total) {

	v = String(v);

	if (v.indexOf('%') !== -1) {
        v = v.slice(0, -1) * total / 100;
    }

	return this.isNumeric(v)
        ? (this.isFloat(v)
            ? parseFloat(v)
            : parseInt(v)
        )
        : v;
};

UtilService.prototype.isNumeric = function(v) {

    return v - parseFloat(v) + 1 >= 0;
};

UtilService.prototype.isFloat = function(v) {

    return Number(v) === v && v % 1 !== 0;
};

UtilService.prototype.randomString = function(len) {

    var buf = [];
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charlen = chars.length;

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    for (var i = 0; i < len; i++) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
};

UtilService.prototype.formatParams = function(params, ex) {

    var ret = {};

    if (!params) {
        params = {};
    }

    for (var k in params) {
        ret[k] = params[k].value;
    }

    if (ex) {
        _.extend(ret, ex);
    }

    return ret;
};

UtilService.prototype.checkUser = function(req, done) {

    var params = this.formatParams(req.swagger.params);

    if (req.user && req.user.userId) {

        if (params.userId && params.userId !== 'me') {

            return done({
                code: 403,
                message: 'You are not allowed to view other customers'
            });
        }

        params.userId = req.user.userId;
    }

    return done(null, params);
};

module.exports = UtilService;