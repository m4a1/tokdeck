'use strict';

module.exports = {
    use: function(provider) {
        return require('./' + provider);
    }
};