 'use strict';

var config = require('../../config');
var async = require('async');
var moment = require('moment');
var IntegrationShopify = require('../../models').IntegrationShopify;
var Integration = require('../../models').Integration;
var User = require('../../models').User;
var IntegrationService = require('../../services/integration');
var UserService = require('../../services/user');
var AuthService = require('../../services/auth');
var shopifyAPI = require('shopify-node-api');
var logger = require('../../lib/logger')({
    prefix: 'service.integrations.shopify'
});

var IntegrationShopifyService = function() {};

IntegrationShopifyService.prototype.auth = function(param, callback) {

    logger.info('IntegrationShopifyService', 'auth', 'start');

    var that = this;
    var result = {
        redirectUrl: null,
        auth: null
    };
    var req = param.req;
    var action = param.action;
    var shop = req.query.shop;
    var hmac = req.query.hmac;
    var timestamp = req.query.timestamp;
    var code = req.query.code;
    var token = null;
    var user = null;
    var email = null;
    var integrationService = new IntegrationService();
    var userService = new UserService();
    var authService = new AuthService();
    var Shopify = new shopifyAPI({
        verbose: false,
        shop: shop,
        shopify_api_key: config.integration.shopify.key,
        shopify_shared_secret: config.integration.shopify.secret,
        shopify_scope: config.integration.shopify.scopes.join(','),
        redirect_uri: config.apiUrl + '/v1/integration/auth/shopify/callback'
    });

    function signIn(done) {

        var tokenUser = null;

        return async.series([
            function (next) {

                if (action) {

                    result.redirectUrl = 'https://' + shop + '/admin/apps/' + config.integration.shopify.name;

                    return next(true);
                }

                return authService.oauthCreateToken({
                    scope: 'all',
                    redirect_uri: true,
                    state: config.oauth.state,
                    realm: config.oauth.realm,
                    userId: user.id,
                    accessTokenExpire: config.oauth.accessTokenExpire
                }, function(err, _result) {

                    if (err) {
                        return next(err);
                    }

                    tokenUser = _result.token;

                    return next();
                });
            },
            function(next) {

                var expires = new moment();

                return userService.formatUser(user, function(err, _result) {

                    if (err) {
                        return next(err);
                    }

                    _result.token = tokenUser;

                    _result.cookie = {
                        name: 'token',
                        domain: config.user.cookieDomain,
                        expires: expires.add(config.user.cookieExpire / 1000, 's').toDate().toUTCString()
                    };

                    result.auth = encodeURIComponent((new Buffer(JSON.stringify(_result))).toString('base64'));

                    return next();
                });
            }
        ], function(err) {

            return done(err || true);
        });
    }

    function checkToken(done) {

        return async.series([
            function(next) {

                Shopify = new shopifyAPI({
                    verbose: false,
                    shop: shop,
                    shopify_api_key: config.integration.shopify.key,
                    shopify_shared_secret: config.integration.shopify.secret,
                    shopify_scope: config.integration.shopify.scopes.join(','),
                    redirect_uri: config.apiUrl + '/v1/integration/auth/shopify/callback',
                    access_token: token
                });

                return Shopify.get('/admin/shop.json', function(err, data) {

                    if (err || !data || !data.shop) {
                        return redirectIt(next);
                    }
console.log(data.shop);
                    email = data.shop.email;

                    return next();
                });
            }
        ], done);
    }

    function redirectIt(done) {

        result.redirectUrl = Shopify.buildAuthURL();

        return done(true);
    }

    return async.series([
        function(next) {

            return that.getIntegrationSpecific({
                guid: shop
            }, function(err, integrationSpecific) {

                if (err && err.code !== 4) {
                    return next(err);
                }

                if (!integrationSpecific) {

                    if (action) {
                        return next();
                    }

                    return redirectIt(next);
                }

                user = integrationSpecific.Integration.User;
                token = integrationSpecific.token;

                return checkToken(function(err) {

                    if (err) {
                        return redirectIt(next);
                    }

                    return signIn(next);
                });
            });
        },
        function(next) {

            return Shopify.exchange_temporary_token({
                shop: shop,
                hmac: hmac,
                timestamp: timestamp,
                code: code
            }, function(err, result) {

                if (err) {
                    return next({code: 0, message: 'Auth error', original: err});
                }

                if (!result || !result.access_token) {
                    return next({code: 0, message: 'Token not found'});
                }

                token = result.access_token;

                return checkToken(next);
            });
        },
        function(next) {

            if (user) {
                return next();
            }

            return integrationService.setupIntegration({
                provider: 'shopify',
                profile: {
                    id: shop,
                    guid: shop,
                    username: '%integration_shopify_' + shop.replace('.', '_') + '%',
                    name: '%integration_shopify_' + shop.replace('.', '_') + '%',
                    email: email,
                    password: String((new Date()).getTime()),
                    token: token,
                    scope: config.integration.shopify.scopes.join(',')
                }
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        signIn
    ], function(err) {

        logger.info('IntegrationShopifyService', 'auth', 'finish');

        if (err && err !== true) {

            logger.error('IntegrationShopifyService', 'auth', err);

            return callback(err);
        }

        return callback(null, result);
    });
};

IntegrationShopifyService.prototype.getIntegrationSpecific = function(param, callback) {

    logger.info('IntegrationShopifyService', 'getIntegrationSpecific', 'start');

    var guid = param.guid;
    var userId = param.userId;
    var transaction = param.transaction;
    var integrationSpecific = null;

    return async.series([
        function(next) {

            var w = {};
            var wUser = {};

            if (userId) {
                wUser = {
                    id: userId
                };
            } else {
                w = {
                    guid: guid
                };
            }

            return IntegrationShopify.find({
                where: w,
                include: [
                    {
                        model: Integration,
                        required: true,
                        include: [
                            {
                                model: User,
                                required: true,
                                where: wUser
                            }
                        ]
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_integrationSpecific) {

                if (!_integrationSpecific) {
                    return next({code: 4, message: 'No integrationSpecific found', guid: guid}) || null;
                }

                integrationSpecific = _integrationSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('IntegrationShopifyService', 'getIntegrationSpecific', 'finish');

        if (err) {

            logger.error('IntegrationShopifyService', 'getIntegrationSpecific', err);

            return callback(err);
        }

        return callback(null, integrationSpecific);
    });
};

IntegrationShopifyService.prototype.createIntegration = function(param, callback) {

    logger.info('IntegrationShopifyService', 'createIntegration', 'start');

    var profile = param.profile;
    var transaction = param.transaction;
    var integrationSpecific = null;

    return async.series([
        function(next) {

            return IntegrationShopify.findOrCreate({
                where: {
                    guid: profile.guid
                },
                defaults: profile,
                transaction: transaction
            }).spread(function(_integrationSpecific) {

                integrationSpecific = _integrationSpecific;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('IntegrationShopifyService', 'createIntegration', 'finish');

        if (err) {

            logger.error('IntegrationShopifyService', 'createIntegration', err);

            return callback(err);
        }

        return callback(null, integrationSpecific);
    });
};

module.exports = IntegrationShopifyService;