'use strict';

/* global sequelize */

var config = require('../config');
var Integration = require('../models').Integration;
var User = require('../models').User;
var integrationStrategyService = require('./integrations');
var AccountService = require('./account');
var UserService = require('./user');

var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.integration'
});

var IntegrationService = function() {};

IntegrationService.prototype.getIntegration = function(param, callback) {

    logger.info('IntegrationService', 'getIntegration', 'start');

    var that = this;
    var id = param.id;
    var integration = null;

    return async.series([
        function(next) {

            return that.getIntegrations({
                id: id
            }, function(error, integrations) {

                if (error) {
                    return next(error);
                }

                if (!integrations || !integrations.length) {
                    return next({code: 0, message: 'No integration found'});
                }

                integration = integrations[0];

                return next();
            });
        }
    ], function(err) {

        logger.info('IntegrationService', 'getIntegration', 'finish');

        if (err) {

            logger.error('IntegrationService', 'getIntegration', err);

            return callback(err);
        }

        return callback(null, integration);
    });
};

IntegrationService.prototype.createIntegration = function(param, callback) {

    logger.info('IntegrationService', 'createIntegration', 'start');

    var accountService = new AccountService();
    var userService = new UserService();
    var profile = param.profile;
    var provider = param.provider;
    var userId = param.userId;
    var integration = null;
    var user = null;
    var integrationStrategy = null;
    var integrationSpecific = null;
    var transaction = null;

    return async.series([
        function(next) {

            return sequelize.sequelize.transaction().then(function(_transaction) {

                transaction = _transaction;

                return next() || null;
            });
        },
        function(next) {

            if (!userId) {
                return next();
            }

            return userService.getUser({
                id: userId,
                transaction: transaction
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            var IntegrationStrategy = integrationStrategyService.use(provider);

            if (!IntegrationStrategy) {
                return next({code: 0, message: 'No IntegrationStrategy found', provider: provider});
            }

            integrationStrategy = new IntegrationStrategy();

            return next();
        },
        function(next) {

            return Integration.findOrCreate({
                where: {
                    userId: userId,
                    type: provider
                },
                defaults: {
                    userId: userId,
                    type: provider
                },
                transaction: transaction
            }).spread(function(_integration) {

                integration = _integration;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            // TODO: this is hack, without this it does not save integrationId for integrationSpecific
            profile.integrationId = integration.id;

            return integrationStrategy.createIntegration({
                userId: userId,
                profile: profile,
                transaction: transaction
            }, function(err, _integrationSpecific) {

                if (err) {
                    return next(err);
                }

                integrationSpecific = _integrationSpecific;

                return next();
            });
        },
        function(next) {

            if (user) {
                return next();
            }

            return accountService.setupAccount({
                provider: 'local',
                profile: profile,
                transaction: transaction
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            return integration.setUser(user, {
                transaction: transaction
            }).then(function() {

                integration.User = user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }/*,
        function(next) {

            return integrationSpecific.setIntegration(integration, {
                transaction: transaction
            }).then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }*/,
        function(next) {

            return transaction.commit().done(function () {

                transaction = null;

                return next() || null;
            });
        }
    ], function(err) {

        logger.info('IntegrationService', 'createIntegration', 'finish');

        if (err) {

            if (transaction) {
                transaction.rollback();
            }

            logger.error('IntegrationService', 'createIntegration', err);

            return callback(err);
        }

        return callback(null, integration);
    });
};

IntegrationService.prototype.updateIntegration = function(param, callback) {

    logger.info('IntegrationService', 'updateIntegration', 'start');

    var profile = param.profile;
    var integrationSpecific = param.integrationSpecific;
    var integration = null;
    var integrationStrategy = null;

    return async.series([
        function(next) {

            return Integration.find({
                where: {
                    id: integrationSpecific.integrationId
                },
                include: [
                    {
                        model: User,
                        required: true
                    }
                ]
            }).then(function(_integration) {

                integration = _integration;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            var IntegrationStrategy = integrationStrategyService.use(integration.type);

            if (!IntegrationStrategy) {
                return next({code: 0, message: 'No IntegrationStrategy found', provider: integration.type});
            }

            integrationStrategy = new IntegrationStrategy();

            return next();
        },
        function(next) {

            return integrationStrategy.updateIntegration({
                integrationSpecific: integrationSpecific,
                profile: profile
            }, function(err, _integrationSpecific) {

                if (err) {
                    return next(err);
                }

                integrationSpecific = _integrationSpecific;

                return next();
            });

            return next();
        }
    ], function(err) {

        logger.info('IntegrationService', 'updateIntegration', 'finish');

        if (err) {

            logger.error('IntegrationService', 'updateIntegration', err);

            return callback(err);
        }

        return callback(null, integration);
    });
};

IntegrationService.prototype.setupIntegration = function(param, callback) {

    logger.info('IntegrationService', 'setupIntegration', 'start');

    var that = this;

    var provider = param.provider;
    var profile = param.profile;
    var userId = param.userId;
    var integrationStrategy = null;
    var integrationSpecific = null;
    var user = null;

    return async.series([
        function(next) {

            var IntegrationStrategy = integrationStrategyService.use(provider);

            if (!IntegrationStrategy) {
                return next({code: 0, message: 'No IntegrationStrategy found', provider: provider});
            }

            integrationStrategy = new IntegrationStrategy();

            return next();
        },
        function(next) {

            if (!profile) {
                return next({code: 0, message: 'Authorization Failed'});
            }

            return integrationStrategy.getIntegrationSpecific({
                userId: userId,
                guid: profile.id
            }, function(err, _integrationSpecific) {

                if (err && err.code !== 4) {
                    return next(err);
                }

                integrationSpecific = _integrationSpecific;

                return next();
            });
        },
        function(next) {

            if (!integrationSpecific) {
                return next();
            }

            return that.updateIntegration({
                integrationSpecific: integrationSpecific,
                profile: profile
            }, function(err, integration) {

                if (err) {
                    return next(err);
                }

                user = integration.User;

                return next();
            });
        },
        function(next) {

            if (integrationSpecific) {
                return next();
            }

            return that.createIntegration({
                provider: provider,
                profile: profile,
                userId: userId
            }, function(err, integration) {

                if (err) {
                    return next(err);
                }

                user = integration.User;

                return next();
            });
        }
    ], function(err) {

        logger.info('IntegrationService', 'setupIntegration', 'finish');

        if (err) {

            logger.error('IntegrationService', 'setupIntegration', err);

            return callback(err);
        }

        return callback(null, user);
    });
};

module.exports = IntegrationService;