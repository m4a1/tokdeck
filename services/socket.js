'use strict';

var config = require('../config');
var DnodeService = require('./dnode');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.socket'
});

var SocketService = function () {

    var that = this;
    this.byUserId = {};
    this.byTid = {};

    this.init = function (io) {

        var dnodeService = new DnodeService();

        io.on('connection', function (socket) {

            socket.userId = null;
            socket.tid = null;

            socket.on('setUser', function (data) {

                if (!data) {
                    return;
                }

                if (data.id) {

                    socket.userId = data.id;

                    if (!that.byUserId[socket.userId]) {
                        that.byUserId[socket.userId] = {};
                    }

                    that.byUserId[socket.userId][socket.id] = socket;
                }

                if (data.tid) {

                    socket.tid = data.tid;

                    if (!that.byTid[socket.tid]) {
                        that.byTid[socket.tid] = {};
                    }

                    that.byTid[socket.tid][socket.id] = socket;
                }

                if (data.id || data.tid) {

                    that.messagesSend({
                        userId: data.id,
                        tid: data.tid
                    });
                }
            });

            socket.on('messageRead', function (data) {

                if (!data || !data.id) {
                    return;
                }

                return that.messageRead(data.id);
            });

            socket.on('disconnect', function () {

                if (socket.userId) {

                    for (var k in that.byUserId[socket.userId]) {

                        if (k === socket.id) {
                            delete that.byUserId[socket.userId][k];
                        }
                    }
                }

                if (socket.tid) {

                    for (var k in that.byTid[socket.tid]) {

                        if (k === socket.id) {
                            delete that.byTid[socket.tid][k];
                        }
                    }
                }
            });
        });

        return dnodeService.start('socket', function() {

            dnodeService.on('messageSent', function(data, callback) {

                return that.messageSent(data.id, function(error, result) {

                    return callback(error, result ? true : false);
                });
            });
        });
    };

    this.messageSent = function(messageId, callback) {

        var sockets = [];
        var message = null;

        return async.series([
            function(next) {

                var MessageService = require('./message');

                var messageService = new MessageService();

                return messageService.getById(messageId, function(error, _message) {

                    if (error) {
                        return next(error);
                    }

                    if (!_message) {
                        return next({code: 0, message: 'No message found'});
                    }

                    message = _message;

                    return next();
                });
            },
            function(next) {

                var ex = [];

                if (that.byUserId[message.userId]) {

                    for (var k in that.byUserId[message.userId]) {

                        if (ex.indexOf(that.byUserId[message.userId][k].id) === -1) {

                            sockets.push(that.byUserId[message.userId][k]);

                            ex.push(that.byUserId[message.userId][k].id);
                        }
                    }
                }
                if (that.byTid[message.tid]) {

                    for (var k in that.byTid[message.tid]) {

                        if (ex.indexOf(that.byTid[message.tid][k].id) === -1) {

                            sockets.push(that.byTid[message.tid][k]);

                            ex.push(that.byTid[message.tid][k].id);
                        }
                    }
                }

                if (!sockets.length) {
                    return next({code: 0, message: 'No active socket found'});
                }

                return next();
            }
        ], function(error) {

            if (sockets.length) {

                for (var k in sockets) sockets[k].emit('messageSent', {
                    error: error,
                    result: message
                });
            }

            return callback(error, message);
        });
    };

    this.messageRead = function(messageId, callback) {

        var message = null;

        return async.series([
            function(next) {

                var MessageService = require('./message');

                var messageService = new MessageService();

                return messageService.readMessage({
                    messageId: messageId
                }, next);
            }
        ], function(error) {

            if (callback) {
                return callback(error, message);
            }
        });
    };

    this.messagesSend = function(param, callback) {

        var messages = null;

        return async.series([
            function(next) {

                var MessageService = require('./message');

                var messageService = new MessageService();

                return messageService.getMessagesUnread(param, function(error, _messages) {

                    if (error) {
                        return next(error);
                    }

                    messages = _messages;

                    return next();
                });
            },
            function(next) {

                if (!messages || !messages.length) {
                    return next();
                }

                return async.eachSeries(messages, function(message, _next) {

                    return that.messageSent(message.id, function() {
                        return _next();
                    });
                }, function(error) {
                    return next(error);
                });
            }
        ], function(error) {

            if (callback) {
                return callback(error, messages);
            }
        });
    };
};

module.exports = SocketService;