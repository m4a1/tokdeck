'use strict';

var config = require('../config');
var validator = require('validator');
var _ = require('lodash');
var User = require('../models').User;
var Account = require('../models').Account;
var AccountLocal = require('../models').AccountLocal;
var LangService = require('./lang');
var ImageService = require('./image');
var MailService = require('./mail');
var credential = require('credential');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.user'
});

var UserService = function() {};

UserService.prototype.getUser = function(param, callback) {

    var id = param.id;
    var email = param.email;
    var remind = param.remind;
    var transaction = param.transaction;
    var user = null;

    return async.series([
        function(next) {

            var w = null;

            if (id) {
                w = {id: id};
            } else if (email) {
                w = {email: email};
            } else if (remind) {
                w = remind.remindHash
                    ? ['MD5(CONCAT(`User`.`id`, ";", `User`.`salt`, ";", `User`.`remindPin`)) = ?', remind.remindHash]
                    : (remind.email && remind.pin ? {email: remind.email, remindPin: remind.pin} : null);
            } else {
                return next({code: 2, message: 'No user found'});
            }

            return User.find({
                where: w
            }, {
                transaction: transaction
            }).then(function(_user) {

                if (!_user) {
                    return next({code: 2, message: 'No user found'}) || null;
                }

                user = _user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(error) {
        return callback(error, user);
    });
};

UserService.prototype.createUser = function(param, callback) {

    var that = this;
    var langService = new LangService();

    var provider = param.provider;
    var profile = param.profile;
    var langId = profile.langId;
    var transaction = param.transaction;
    var username = profile.username || '';
    var password = profile.password || '';
    var email = profile.email || '';
    var name = profile.name || '';
    var imageUrl = profile.imageUrl;
    var imageId = null;
    var user = null;

    return async.series([
        function(next) {

            if (langId) {
                return next();
            }

            return langService.getLang({
                isDefault: true
            }, function(error, lang) {

                if (error) {
                    return next(error);
                }

                langId = lang.id;

                return next();
            });
        },
        function (next) {

            if (provider === 'local' && !username) {
                return next({code: 0, message: 'Username not set'});
            }

            return next();
        },
        function (next) {

            if (provider === 'local' && !password) {
                return next({code: 0, message: 'Password not set'});
            }

            return next();
        },
        function (next) {

            if (provider === 'local' && !email) {
                return next({code: 0, message: 'Email not set'});
            }

            return that.validateEmail(email, {}, function(err) {

                if (err && (provider === 'local' || err.code !== 6)) {
                    return next(err);
                }

                return next();
            });
        },
        function (next) {

            if (!email) {
                return next();
            }

            return that.fixEmail({
                email: email,
                provider: provider
            }, function(err, _email) {

                if (err) {
                    return next(err);
                }

                email = _email;

                return next();
            });
        },
        function(next) {

            return that.validateImage(imageUrl, function(error, _imageId) {

                if (_imageId) {
                    imageId = _imageId;
                }

                return next();
            });
        },
        function(next) {

            return User.create({
                imageId: imageId,
                name: name,
                email: email,
                langId: langId
            }, {
                transaction: transaction
            }).then(function(_user) {

                user = _user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (!user.email) {
                return next();
            }

            that.sendConfirmationEmail({
                user: user,
                transaction: transaction
            });

            return next();
        }
    ], function(error) {
        return callback(error, user);
    });
};

UserService.prototype.fixEmail = function(param, callback) {

    logger.info('UserService', 'fixEmail', 'start');

    var that = this;
    var provider = param.provider;
    var providerOriginal = null;
    var email = param.email;
    var parts = email.split('@');
    var suffix = '';
    var start = parts[0];
    var end = parts[1];
    var partsStart = start.split('+');

    if (partsStart.length) {

        provider = providerOriginal = partsStart.pop();

        if (partsStart.length > 1) {
            start = partsStart.join('+');
        }
    }

    return async.series([
        function(next) {

            var exist = true;
            var num = 0;

            return async.doWhilst(
                function (_next) {

                    suffix = num > 0 || providerOriginal
                        ? '+' + (provider === 'local' ? '' : provider) + (num > 0 ? String(num) : '')
                        : '';

                    return that.getUser({
                        email: start + suffix + '@' + end
                    }, function(err) {

                        if (err && err.code === 2) {

                            exist = false;

                            return _next();
                        }

                        num++;

                        return _next();
                    });
                },
                function () {
                    return exist;
                },
                next
            );
        }
    ], function(err) {

        logger.info('UserService', 'fixEmail', 'finish');

        if (err) {

            logger.error('UserService', 'fixEmail', err);

            return callback(err);
        }

        return callback(null, start + suffix + '@' + end);
    });
};

UserService.prototype.validateImage = function(imageUrl, callback) {

    var imageService = new ImageService();

    if (!imageUrl) {
        return callback();
    }

    return imageService.add(imageUrl, config.image.validator.user, function(error, image) {

        if (error || !image) {
            return callback();
        }

        return callback(null, image.id);
    });
};

UserService.prototype.updateUser = function(param, callback) {

    var that = this;
    var userId = param.userId;
    var profile = param.profile;
    var AccountService = require('./account');
    var accountService = new AccountService();
    var noCheckCurrentPassword = param.noCheckCurrentPassword;
    var user = null, userOld = null;
    var localAccount = null;
    var emailOld = null;

    return async.series([
        function(next) {

            return that.getUser({
                id: userId
            }, function(error, _user) {

                if (error) {
                    return next(error);
                }

                userOld = _user;
                emailOld = userOld.email;

                return next();
            });
        },
        function (next) {
            return that.validateEmail(profile.email, userOld, next);
        },
        function(next) {

            if (!profile.password) {
                return next();
            }

            return that.getUserLocalAccount({
                userId: userOld.id,
                noLogError: true
            }, function(err, _localAccount) {

                if (err || !_localAccount) {
                    return next();
                }

                localAccount = _localAccount;

                return next();
            });
        },
        function (next) {

            if (!profile.password || !localAccount || noCheckCurrentPassword) {
                return next();
            }

            return credential.verify(localAccount.password, profile.passwordCurrent, function (error, isValid) {

                if (!isValid) {
                    return next({code: 5, message: 'Current password not match'});
                }

                delete profile.passwordCurrent;

                return next();
            });
        },
        function (next) {

            if (!profile.password && !profile.name) {
                return next();
            }

            return accountService.setupAccount({
                provider: 'local',
                userId: userOld.id,
                profile: profile
            }, next);
        },
        function(next) {

            return that.validateImage(profile.imageUrl, function(error, _imageId) {

                profile.imageId = _imageId ? _imageId : null;

                return next();
            });
        },
        function(next) {

            var field = [];

            for (var k in profile) {

                if (typeof userOld[k] !== 'undefined') {
                    field.push(k);
                }
            }

            return userOld.update(profile, field).then(function(_user) {

                user = _user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (!user.email || emailOld === user.email) {
                return next();
            }

            that.sendConfirmationEmail({
                user: user,
                changed: emailOld ? true : false
            });

            return next();
        }
    ], function(error) {
        return callback(error, user);
    });
};

UserService.prototype.formatUser = function(user, callback) {

    var that = this;
    var imageService = new ImageService();
    var result = null;
    var imageUrl = null;
    var localAccount = null;

    return async.series([
        function(next) {

            if (!user.imageId) {
                return next();
            }

            return imageService.getUrl(user.imageId, 'big', function(error, _imageUrl) {

                if (!error && _imageUrl) {
                    imageUrl = _imageUrl;
                }

                return next();
            });
        },
        function(next) {

            return that.getUserLocalAccount({
                userId: user.id,
                noLogError: true
            }, function(err, _localAccount) {

                if (!err && _localAccount) {
                    localAccount = _localAccount;
                }

                return next();
            });
        },
        function(next) {

            result = {
                id: user.id,
                langId: user.langId,
                email: user.email,
                name: user.name,
                username: localAccount ? localAccount.guid : null,
                imageUrl: imageUrl,
                hasPassword: !!localAccount,
                emailConfirmed: user.confirmationPin ? 0 : 1,
                onboardingPassed: localAccount && user.email && user.name
            };
            return next();
        }
    ], function(error) {

        return callback(error, result);
    });
};

UserService.prototype.getUserLocalAccount = function(param, callback) {

    logger.info('UserService', 'getUserLocalAccount', 'start');

    var userId = param.userId;
    var noLogError = param.noError;
    var result = null;

    return async.series([
        function (next) {

            return AccountLocal.find({
                include: [
                    {
                        model: Account,
                        attributes: [],
                        required: true,
                        where: {
                            userId: userId
                        }
                    }
                ]
            }).then(function(_result) {

                if (!_result) {
                    return next({code: 0, message: 'Local Account Not found'}) || null;
                }

                result = _result || null;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('UserService', 'getUserLocalAccount', 'finish');

        if (err) {

            if (!noLogError) {
                logger.error('UserService', 'getUserLocalAccount', err);
            }

            return callback(err);
        }

        return callback(null, result);
    });
};

UserService.prototype.sendConfirmationEmail = function(param, callback) {

    var that = this;
    var user = param.user;
    var changed = param.changed;
    var transaction = param.transaction;
    var mailService = new MailService();
    var result = false;
    var confirmationHash = null;
    var pin = '';

    return async.series([
        function (next) {

            if (!user.email) {
                return next();
            }

            return that.generatePin({}, function(error, _pin) {

                pin = _pin;

                confirmationHash = require('crypto').createHash('md5').update([user.id, user.salt, pin].join(';')).digest("hex");

                return next();
            });
        },
        function (next) {

            if (!user.email) {
                return next();
            }

            return user.update({
                confirmationPin: pin
            }, ['confirmationPin'], {
                transaction: transaction
            }).then(function(_user) {

                result = true;

                user = _user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function (next) {

            if (!user.email) {
                return next();
            }

            mailService.mailUserConfirmation({
                user: user,
                changed: changed,
                confirmationPin: pin,
                confirmationLink: config.siteUrl + '/dashboard/userconfirmation?confirmationHash=' + confirmationHash
            });

            return next();
        }
    ], function(error) {

        if (callback) {
            return callback(error, result);
        }
    });
};

UserService.prototype.generatePin = function(param, callback) {

    var length = param.length || 4;
    var result = [];

    return async.series([
        function (next) {

            for (var k = 0; k < length; k++) {
                result.push(Math.floor(Math.random() * 9));
            }

            return next();
        }
    ], function() {

        if (callback) {
            return callback(null, result.join(''));
        }
    });
};

UserService.prototype.remindUser = function(param, callback) {

    var that = this;
    var email = param.email;
    var mailService = new MailService();
    var user = null;
    var remindHash = null;
    var pin = '';

    return async.series([
        function(next) {

            if (!email) {
                return next({code: 0, message: 'No E-mail provided'});
            }

            return that.getUser({
                email: email
            }, function(error, _user) {

                if (error) {
                    return next(error);
                }

                user = _user;

                return next();
            });
        },
        function (next) {

            return that.generatePin({}, function(error, _pin) {

                pin = _pin;

                remindHash = require('crypto').createHash('md5').update([user.id, user.salt, pin].join(';')).digest("hex");

                return next();
            });
        },
        function (next) {

            return user.update({
                remindPin: pin
            }, ['remindPin']).then(function(_user) {

                user = _user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function (next) {

            if (!user.email) {
                return next();
            }

            mailService.mailUserRemind({
                user: user,
                remindPin: pin,
                remindLink: config.siteUrl + '/dashboard/remind/continue?remindHash=' + remindHash + '&email=' + email
            });

            return next();
        }
    ], function(error) {

        if (callback) {
            return callback(error, user);
        }
    });
};

UserService.prototype.remindcontinueUser = function(param, callback) {

    var that = this;
    var email = param.email;
    var password = param.password;
    var pin = param.pin;
    var remindHash = param.remindHash;
    var user = null;

    return async.series([
        function(next) {

            if (!email && !remindHash) {
                return next({code: 0, message: 'No E-mail provided or Link expired'});
            }

            return that.getUser({
                remind: {
                    email: email,
                    remindHash: remindHash,
                    pin: pin
                }
            }, function(error, _user) {

                if (error) {
                    return next(error);
                }

                user = _user;

                return next();
            });
        },
        function (next) {

            return that.updateUser({
                userId: user.id,
                noCheckCurrentPassword: true,
                profile: {
                    remindPin: '',
                    password: password
                }
            }, function(error, _user) {

                if (error) {
                    return next(error);
                }

                user = _user;

                return next();
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, user);
        }
    });
};

UserService.prototype.validateUsername = function(username, accountLocalExisting, callback) {

    if (!username || accountLocalExisting.guid === username) {
        return callback();
    }

    return AccountLocal.find({
        where: {
            guid: username
        }
    }).then(function(accountLocal) {

        if (accountLocal && accountLocal.id !== accountLocalExisting.id) {
            return callback({code: 7, message: 'We already have a User with this username in our system. Please enter another username.'});
        }

        return callback() || null;
    }).catch(function(err) {

        return callback({code: 0, message: 'DB error', original: err}) || null;
    });
};

UserService.prototype.validatePassword = function(password, callback) {

    if (!password) {
        return callback();
    }

    if (String(password).length < 4) {
        return callback({code: 3, message: 'Password should be more than 4 characters length'});
    }

    if (String(password).length > 200) {
        return callback({code: 4, message: 'Password should be less than 200 characters length'});
    }

    return callback();
};

UserService.prototype.validateEmail = function(email, user, callback) {

    if (!email || user.email === email) {
        return callback();
    }

    if (!validator.isEmail(email)) {
        return callback({code: 0, message: 'Email is invalid'});
    }

    return this.getUser({
        email: email
    }, function(error, _user) {

        if (!error && _user && _user.id !== user.id) {
            return callback({code: 6, message: 'We already have a User with this email in our system. Please enter another email address.'});
        }

        return callback();
    });
};

module.exports = UserService;