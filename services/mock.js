'use strict';

/* global sequelize */

var async = require('async');

var config = require('../config');
var Lang = require('../models').Lang;
var Locale = require('../models').Locale;
var Page = require('../models').Page;
var logger = require('../lib/logger')({
    prefix: 'service.mock'
});

var MockService = function () {};

MockService.prototype.init = function(params, callback) {

    var langs = [
        {
            stitle: 'en',
            title: 'English',
            isDefault: true
        }
    ];

    var locales = [
        {
            stitle: 'page_signin_title',
            value: 'Sign In',
            langId: 1
        },
        {
            stitle: 'page_signin_title_1',
            value: 'Sign In with:',
            langId: 1
        },
        {
            stitle: 'page_signin_title_2',
            value: 'or TokDeck:',
            langId: 1
        },
        {
            stitle: 'username',
            value: 'Username',
            langId: 1
        },
        {
            stitle: 'password',
            value: 'Password',
            langId: 1
        },
        {
            stitle: 'signin',
            value: 'Sign In',
            langId: 1
        },
        {
            stitle: 'page_signup_title_1',
            value: 'Sign Up with:',
            langId: 1
        },
        {
            stitle: 'page_signup_title_2',
            value: 'or TokDeck:',
            langId: 1
        },
        {
            stitle: 'signup',
            value: 'Signup',
            langId: 1
        },
        {
            stitle: 'password_repeat',
            value: 'Repeat password',
            langId: 1
        },
        {
            stitle: 'email',
            value: 'E-mail',
            langId: 1
        },
        {
            stitle: 'error_unknown',
            value: 'Application error',
            langId: 1
        },
        {
            stitle: 'copyright',
            value: 'TokDeck © 2015',
            langId: 1
        },
        {
            stitle: 'site_name',
            value: 'TokDeck',
            langId: 1
        },
        {
            stitle: 'signout',
            value: 'Sign Out',
            langId: 1
        },
        {
            stitle: 'page_dashboard_title',
            value: 'Dashboard',
            langId: 1
        },
        {
            stitle: 'page_home_title',
            value: 'Home',
            langId: 1
        },
        {
            stitle: 'page_about_title',
            value: 'About',
            langId: 1
        },
        {
            stitle: 'page_about_message',
            value: '<h3>Message</h3>',
            langId: 1
        },
        {
            stitle: 'page_contacts_title',
            value: 'Contacts',
            langId: 1
        },
        {
            stitle: 'page_contacts_message',
            value: '<h3>Message</h3>',
            langId: 1
        },
        {
            stitle: 'profile',
            value: 'Profile',
            langId: 1
        },
        {
            stitle: 'page_profile_title',
            value: 'Profile',
            langId: 1
        },
        {
            stitle: 'page_profile_profile_title',
            value: 'Personal',
            langId: 1
        },
        {
            stitle: 'page_profile_password_title',
            value: 'Change password',
            langId: 1
        },
        {
            stitle: 'username_placeholder',
            value: 'johnny',
            langId: 1
        },
        {
            stitle: 'name_placeholder',
            value: 'John',
            langId: 1
        },
        {
            stitle: 'email_placeholder',
            value: 'hello@tokdeck.com',
            langId: 1
        },
        {
            stitle: 'save',
            value: 'Save',
            langId: 1
        },
        {
            stitle: 'cancel',
            value: 'Cancel',
            langId: 1
        },
        {
            stitle: 'save_profile',
            value: 'Update profile',
            langId: 1
        },
        {
            stitle: 'password_current',
            value: 'Current password',
            langId: 1
        },
        {
            stitle: 'create_account',
            value: 'Create account',
            langId: 1
        },
        {
            stitle: 'have_account',
            value: 'Do not have an account?',
            langId: 1
        },
        {
            stitle: 'forgot_password',
            value: 'Forgot Password?',
            langId: 1
        },
        {
            stitle: 'notifications',
            value: 'Notifications',
            langId: 1
        },
        {
            stitle: 'onboarding_save_profile',
            value: 'Save',
            langId: 1
        },
        {
            stitle: 'onboarding_panel_title',
            value: 'Welcome!',
            langId: 1
        },
        {
            stitle: 'onboarding_panel_help_title',
            value: 'Please, fill in some more details',
            langId: 1
        },
        {
            stitle: 'onboarding_panel_help_message',
            value: 'Text',
            langId: 1
        },
        {
            stitle: 'save_password',
            value: 'Update password',
            langId: 1
        },
        {
            stitle: 'password_new',
            value: 'New password',
            langId: 1
        },
        {
            stitle: 'page_auction_title',
            value: 'Auctions',
            langId: 1
        },
        {
            stitle: 'page_personal_title',
            value: 'Personal data',
            langId: 1
        },
        {
            stitle: 'name',
            value: 'Name',
            langId: 1
        },
        {
            stitle: 'lang',
            value: 'Language',
            langId: 1
        },
        {
            stitle: 'remind',
            value: 'Recover password',
            langId: 1
        },
        {
            stitle: 'remind_intro',
            value: 'Email with password recover instructions will be sent to you.',
            langId: 1
        },
        {
            stitle: 'remindcontinue_intro',
            value: 'Set new password.',
            langId: 1
        },
        {
            stitle: 'pin',
            value: 'Password reset PIN',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_subject_changed',
            value: 'E-mail confirmation',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_subject',
            value: 'E-mail confirmation',
            langId: 1
        },
        {
            stitle: 'mail_userremind_subject',
            value: 'Recover password',
            langId: 1
        },
        {
            stitle: 'mail_greeting',
            value: 'Hello,',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_greeting_changed',
            value: 'You E-mail has been changed.',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_greeting',
            value: 'You are trying to link this E-mail address to TokDeck account.',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_text',
            value: 'Please, follow link below to confirm E-mail.',
            langId: 1
        },
        {
            stitle: 'mail_userconfirmation_action_text',
            value: 'Link:',
            langId: 1
        },
        {
            stitle: 'mail_userremind_greeting',
            value: 'You are trying to reset password for TokDeck account.',
            langId: 1
        },
        {
            stitle: 'mail_userremind_text',
            value: 'Please, follow link below:',
            langId: 1
        },
        {
            stitle: 'mail_userrecover_pin_action_text',
            value: 'Password reset PIN:',
            langId: 1
        },
        {
            stitle: 'mail_userrecover_link_action_text',
            value: 'Password reset link:',
            langId: 1
        },
        {
            stitle: 'msg_profile_updated',
            value: 'Profile updated',
            langId: 1
        },
        {
            stitle: 'msg_user_reminded',
            value: 'Password reset instructions have been sent',
            langId: 1
        },
        {
            stitle: 'msg_user_reminded_finish',
            value: 'New password has been set',
            langId: 1
        },
        {
            stitle: 'msg_user_signed_in',
            value: 'Welcome!',
            langId: 1
        },
        {
            stitle: 'msg_auction_created',
            value: 'Auction created',
            langId: 1
        },
        {
            stitle: 'msg_auction_updated',
            value: 'Auction updated',
            langId: 1
        },
        {
            stitle: 'msg_auction_removed',
            value: 'Auction removed',
            langId: 1
        },
        {
            stitle: 'msg_product_created',
            value: 'Product created',
            langId: 1
        },
        {
            stitle: 'msg_product_updated',
            value: 'Product updated',
            langId: 1
        },
        {
            stitle: 'msg_product_removed',
            value: 'Product removed',
            langId: 1
        },
        {
            stitle: 'msg_tok_created',
            value: 'Tok created',
            langId: 1
        },
        {
            stitle: 'msg_tok_updated',
            value: 'Tok updated',
            langId: 1
        },
        {
            stitle: 'msg_tok_removed',
            value: 'Tok removed',
            langId: 1
        },
        {
            stitle: 'add',
            value: 'Add',
            langId: 1
        },
        {
            stitle: 'edit',
            value: 'Edit',
            langId: 1
        },
        {
            stitle: 'delete',
            value: 'Delete',
            langId: 1
        },
        {
            stitle: 'auction_list_title',
            value: 'Auctions',
            langId: 1
        },
        {
            stitle: 'auction_list',
            value: 'List',
            langId: 1
        },
        {
            stitle: 'auction_add_title',
            value: 'New auction',
            langId: 1
        },
        {
            stitle: 'auction_add',
            value: 'Fill in auction dtails',
            langId: 1
        },
        {
            stitle: 'auction_title',
            value: 'Title',
            langId: 1
        },
        {
            stitle: 'auction_title_placeholder',
            value: 'Auction-1',
            langId: 1
        },
        {
            stitle: 'auction_list_none',
            value: 'You do not have auctions. Press Add button to create some.',
            langId: 1
        },
        {
            stitle: 'auction_edit_title',
            value: 'Editing auction',
            langId: 1
        },
        {
            stitle: 'auction_edit',
            value: 'Change auction settings',
            langId: 1
        },
        {
            stitle: 'auction_delete_title',
            value: 'Deleting auction',
            langId: 1
        },
        {
            stitle: 'delete',
            value: 'Delete',
            langId: 1
        },
        {
            stitle: 'delete_confirmation',
            value: 'Delete confirmation',
            langId: 1
        },
        {
            stitle: 'auction_delete_intro',
            value: 'You are going to delete auction. This action can not bew reverted. Continue?',
            langId: 1
        },
        {
            stitle: 'auction_product_title',
            value: 'Auction products',
            langId: 1
        },
        {
            stitle: 'auction_product',
            value: 'List of products available for this auction',
            langId: 1
        },
        {
            stitle: 'product_oid',
            value: 'Product ID',
            langId: 1
        },
        {
            stitle: 'product_list_none',
            value: 'You have no products attached to auction. Add some.',
            langId: 1
        },
        {
            stitle: 'product_add_title',
            value: 'New product',
            langId: 1
        },
        {
            stitle: 'product_edit_title',
            value: 'Editing product',
            langId: 1
        },
        {
            stitle: 'product_delete_title',
            value: 'Deleting product',
            langId: 1
        },
        {
            stitle: 'product_add',
            value: 'Fill in product dtails',
            langId: 1
        },
        {
            stitle: 'product_delete_intro',
            value: 'You are going to delete product. This action can not bew reverted. Continue?',
            langId: 1
        },
        {
            stitle: 'auction_tok_title',
            value: 'Auction toks',
            langId: 1
        },
        {
            stitle: 'auction_tok',
            value: 'List of toks available for this auction',
            langId: 1
        },
        {
            stitle: 'tok_oid',
            value: 'Tok ID',
            langId: 1
        },
        {
            stitle: 'tok_list_none',
            value: 'You have no toks attached to auction. Add some.',
            langId: 1
        },
        {
            stitle: 'tok_add_title',
            value: 'New tok',
            langId: 1
        },
        {
            stitle: 'tok_edit_title',
            value: 'Editing tok',
            langId: 1
        },
        {
            stitle: 'tok_delete_title',
            value: 'Deleting tok',
            langId: 1
        },
        {
            stitle: 'tok_add',
            value: 'Fill in tok dtails',
            langId: 1
        },
        {
            stitle: 'tok_delete_intro',
            value: 'You are going to delete tok. This action can not bew reverted. Continue?',
            langId: 1
        },
        {
            stitle: 'msg_auction_status_changed',
            value: 'Auction status has been changed',
            langId: 1
        },
        {
            stitle: 'msg_auction_active_changed',
            value: 'Auction activity state has been changed',
            langId: 1
        },
        {
            stitle: 'msg_product_active_changed',
            value: 'Product activity state has been changed',
            langId: 1
        },
        {
            stitle: 'msg_tok_active_changed',
            value: 'Tok activity state has been changed',
            langId: 1
        },
        {
            stitle: 'active',
            value: 'Active',
            langId: 1
        },
        {
            stitle: 'yes',
            value: 'Yes',
            langId: 1
        },
        {
            stitle: 'no',
            value: 'No',
            langId: 1
        },
        {
            stitle: 'status',
            value: 'Status',
            langId: 1
        },
        {
            stitle: 'auction_status_running',
            value: 'Running',
            langId: 1
        },
        {
            stitle: 'auction_status_stopped',
            value: 'Stopped',
            langId: 1
        },
        {
            stitle: 'product_price',
            value: 'Price',
            langId: 1
        },
        {
            stitle: 'auction_field_lotMax_title',
            value: 'Maximum quantity of lots',
            langId: 1
        },
        {
            stitle: 'auction_field_rateMin_title',
            value: 'Minimal bid',
            langId: 1
        },
        {
            stitle: 'auction_field_rateMax_title',
            value: 'Maximal bid',
            langId: 1
        },
        {
            stitle: 'auction_field_periodMin_title',
            value: 'Minimal period for Lot',
            langId: 1
        },
        {
            stitle: 'auction_field_periodMax_title',
            value: 'Maximal period for Lot',
            langId: 1
        },
        {
            stitle: 'auction_field_periodStartMin_title',
            value: 'Minimal period for before Lot start',
            langId: 1
        },
        {
            stitle: 'auction_field_periodStartMax_title',
            value: 'Maximal period for before Lot start',
            langId: 1
        },
        {
            stitle: 'auction_field_timeAdd_title',
            value: 'Time added after bid',
            langId: 1
        },
        {
            stitle: 'auction_field_rateMultiple_title',
            value: 'Bid multiplicator',
            langId: 1
        },
        {
            stitle: 'auction_field_rateDefault_title',
            value: 'Bid default value',
            langId: 1
        },
        {
            stitle: 'auction_field_botActive_title',
            value: 'Toks active',
            langId: 1
        },
        {
            stitle: 'auction_field_botPricePeopleMin_title',
            value: 'Toks minimal price with People',
            langId: 1
        },
        {
            stitle: 'auction_field_botPricePeopleMax_title',
            value: 'Toks maximal price with People',
            langId: 1
        },
        {
            stitle: 'auction_field_botPriceBotMin_title',
            value: 'Toks minimal price without People',
            langId: 1
        },
        {
            stitle: 'auction_field_botPriceBotMax_title',
            value: 'Toks maximal price without People',
            langId: 1
        },
        {
            stitle: 'auction_field_priceStartMin_title',
            value: 'Lot minimal start price',
            langId: 1
        },
        {
            stitle: 'auction_field_priceStartMax_title',
            value: 'Lot maximal price',
            langId: 1
        },
        {
            stitle: 'auction_field_rateDefaultModeQuant_title',
            value: 'Quantity of default bids before lot close below which Toks will do only default bids',
            langId: 1
        },
        {
            stitle: 'auction_field_rateRvMorePercent_title',
            value: 'Percent of price accuracy when selecting template',
            langId: 1
        },
        {
            stitle: 'auction_field_rateGapMin_title',
            value: 'Minimal time gap between rates',
            langId: 1
        },
        {
            stitle: 'auction_field_rateTimeRate1_title',
            value: 'Time below which Toks will do 1 bid',
            langId: 1
        },
        {
            stitle: 'auction_field_rateTimeRatePlenty_title',
            value: 'Time higher which Toks will do plenty of rates',
            langId: 1
        },
        {
            stitle: 'auction_field_yellowPeriod_title',
            value: 'Bid yellow flashing period',
            langId: 1
        },
        {
            stitle: 'auction_field_redPeriod_title',
            value: 'Bid red flashing period',
            langId: 1
        }
    ];

    var pages = [
        {
            stitle: 'dashboard',
            title: 'Dashboard',
            langId: 1,
            message: [
                '<%- include(\'partials/header\') %>',
                '<body class="page-header-fixed compact-menu page-horizontal-bar">',
                    '<div class="overlay"></div>',
                    '<main class="page-content content-wrap">',
                            '<%- include(\'partials/navbar\') %>',
                            '<div class="page-sidebar sidebar horizontal-bar">',
                                    '<div class="page-sidebar-inner">',
                                        '<%- include(\'partials/adaptiveMenu\') %>',
                                        '<%- include(\'partials/dashboardMenu.ejs\') %>',
                                    '</div>',
                            '</div>',
                            '<div class="page-inner">',
                                '<div id="main-wrapper" class="container">',
                                    '<div ui-view></div>',
                                '</div>',
                                '<%- include(\'partials/footerStripe\') %>',
                            '</div>',
                    '</main>',
                '</body>',
                '<%- include(\'partials/footer\') %>'
            ].join('')
        },
        {
            stitle: 'home',
            title: 'Home',
            langId: 1,
            message: [
                '<%- include(\'partials/header\') %>',
                '<body data-spy="scroll" data-target="#header">',
                '<nav id="header" class="navbar navbar-fixed-top">',
                    '<div class="container">',
                        '<div class="navbar-header">',
                            '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">',
                                '<span class="sr-only">Toggle navigation</span>',
                                '<span class="fa fa-bars"></span>',
                            '</button>',
                            '<a class="navbar-brand" href="#"><%= locale.site_name %></a>',
                        '</div>',
                        '<div id="navbar" class="navbar-collapse collapse navbar-right">',
                            '<ul class="nav navbar-nav">',
                                '<li><a href="#home">Home</a></li>',
                                '<li><a href="#features">Features</a></li>',
                                '<li><a href="#pricing">Pricing</a></li>',
                                '<li><a href="#about">About</a></li>',
                                '<li class="au-authbar"><a class="btn btn-danger" href="/dashboard"><%= locale.signin %></a></li>',
                                '<li class="au-authbar"><a class="btn btn-info" href="/signup"><%= locale.signup %></a></li>',
                            '</ul>',
                        '</div>',
                    '</div>',
                '</nav>',
                '<div class="home" id="home">',
                    '<div class="overlay"></div>',
                    '<div class="container">',
                        '<div class="row">',
                            '<div class="home-text col-md-8">',
                                '<h1 class="wow fadeInDown" data-wow-delay="1.5s" data-wow-duration="1.5s" data-wow-offset="10">Responsive Admin Dashboard Template.</h1>',
                                '<p class="lead wow fadeInDown" data-wow-delay="2s" data-wow-duration="1.5s" data-wow-offset="10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<br>Aenean commodo ligula eget dolor.</p>',
                                '<a href="/dashboard" class="btn btn-default btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">Dashboard</a>',
                                '<a href="/signup" class="btn btn-success btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">Free Trial</a>',
                            '</div>',
                            '<div class="scroller">',
                                '<div class="mouse"><div class="wheel"></div></div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>',
                '<div class="container" id="features">',
                    '<div class="row features-list">',
                        '<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">',
                            '<div class="feature-icon">',
                                    '<i class="fa fa-laptop"></i>',
                            '</div>',
                            '<h2>Fully Responsive</h2>',
                            '<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies</p>',
                            '<p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>',
                        '</div>',
                        '<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">',
                            '<div class="feature-icon">',
                                    '<i class="fa fa-lightbulb-o"></i>',
                            '</div>',
                            '<h2>Creative Design</h2>',
                            '<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies</p>',
                            '<p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>',
                        '</div>',
                        '<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">',
                            '<div class="feature-icon">',
                                    '<i class="fa fa-support"></i>',
                            '</div>',
                            '<h2>Free Support</h2>',
                            '<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies</p>',
                            '<p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>',
                        '</div>',
                    '</div>',
                '</div>',
                '<section id="section-1">',
                    '<div class="container">',
                        '<div class="row">',
                            '<div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">',
                                '<img src="assets/images/iphone.png" class="iphone-img" alt="">',
                            '</div>',
                            '<div class="col-sm-8 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">',
                                '<h1>The Power You Need</h1>',
                                '<p>Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero.</p>',
                                '<ul class="list-unstyled features-list-2">',
                                    '<li><i class="fa fa-diamond icon-state-success m-r-xs icon-md"></i>Unique design</li>',
                                    '<li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Everything you need</li>',
                                    '<li><i class="fa fa-cogs icon-state-success m-r-xs icon-md"></i>Tons of features</li>',
                                    '<li><i class="fa fa-cloud icon-state-success m-r-xs icon-md"></i>Easy to use &amp; customize</li>',
                                '</ul>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</section>',
                '<section id="section-2">',
                    '<div class="container">',
                        '<div class="row">',
                            '<div class="col-sm-8 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">',
                                '<section>',
                                    '<div class="tabs tabs-style-linebox">',
                                        '<nav>',
                                            '<ul>',
                                                '<li class="tab-current"><a href=""><span>Responsive</span></a></li>',
                                                '<li class=""><a href=""><span>Browsers</span></a></li>',
                                                '<li class=""><a href=""><span>Bootstrap</span></a></li>',
                                                '<li class=""><a href=""><span>Icons</span></a></li>',
                                                '<li class=""><a href=""><span>Documentation</span></a></li>',
                                            '</ul>',
                                        '</nav>',
                                        '<div class="content-wrap">',
                                            '<section class="content-current">',
                                                '<h1>Responsive Design</h1>',
                                                '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.<br>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>',
                                            '</section>',
                                            '<section><p>',
                                                '<h1>Cross-browser Compatible</h1>',
                                                '<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus.<br>Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>',
                                            '</section>',
                                            '<section><p>',
                                                '<h1>Built With Bootstrap 3.3.4</h1>',
                                                '<p>Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.</p>',
                                            '</section>',
                                            '<section><p>',
                                                '<h1>+1100 Icons Included</h1>',
                                                '<p>Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis.<br>Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam.</p>',
                                            '</section>',
                                            '<section><p>',
                                                '<h1>Well Documented</h1>',
                                                '<p>Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh.</p>',
                                            '</section>',
                                        '</div>',
                                    '</div>',
                                '</section>',
                            '</div>',
                            '<div class="col-sm-4 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">',
                                '<img src="assets/images/iphone2.png" class="iphone-img" alt="">',
                            '</div>',
                        '</div>',
                    '</div>',
                '</section>',
                '<section id="section-3">',
                    '<div class="overlay"></div>',
                    '<div class="container">',
                        '<div class="row">',
                            '<div class="col-sm-12 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">',
                                '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">',
                                    '<ol class="carousel-indicators">',
                                        '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>',
                                        '<li data-target="#carousel-example-generic" data-slide-to="1"></li>',
                                        '<li data-target="#carousel-example-generic" data-slide-to="2"></li>',
                                    '</ol>',
                                    '<div class="carousel-inner" role="listbox">',
                                        '<div class="item active">',
                                            '<div class="row">',
                                                '<div class="col-sm-6 col-sm-offset-3">',
                                                    '<p class="text-white">“Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero.”</p>',
                                                    '<span>- David, App Manager</span>',
                                                '</div>',
                                            '</div>',
                                        '</div>',
                                        '<div class="item">',
                                            '<div class="row">',
                                                '<div class="col-sm-6 col-sm-offset-3">',
                                                    '<p class="text-white">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.”</p>',
                                                    '<span>- Sandra, Director</span>',
                                                '</div>',
                                            '</div>',
                                        '</div>',
                                        '<div class="item">',
                                            '<div class="row">',
                                                '<div class="col-sm-6 col-sm-offset-3">',
                                                    '<p>“Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit.”</p>',
                                                    '<span>- Amily, UI Designer</span>',
                                                '</div>',
                                            '</div>',
                                        '</div>',
                                    '</div>',
                                '</div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</section>',
                '<div class="container" id="pricing">',
                    '<div class="row">',
                        '<div class="cd-pricing-container">',
                            '<div class="cd-pricing-switcher">',
                                '<p class="fieldset">',
                                    '<input type="radio" class="no-uniform" name="duration-1" value="monthly" id="monthly-1" checked>',
                                    '<label for="monthly-1">Monthly</label>',
                                    '<input type="radio" class="no-uniform" name="duration-1" value="yearly" id="yearly-1">',
                                    '<label for="yearly-1">Yearly</label>',
                                    '<span class="cd-switch"></span>',
                                '</p>',
                            '</div>',
                            '<ul class="cd-pricing-list cd-bounce-invert">',
                                '<li>',
                                    '<ul class="cd-pricing-wrapper">',
                                        '<li data-type="monthly" class="is-visible">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Basic</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">30</span>',
                                                    '<span class="cd-duration">mo</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>256MB</em> Memory</li>',
                                                    '<li><em>1</em> User</li>',
                                                    '<li><em>1</em> Website</li>',
                                                    '<li><em>1</em> Domain</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                        '<li data-type="yearly" class="is-hidden">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Basic</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">320</span>',
                                                    '<span class="cd-duration">yr</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>256MB</em> Memory</li>',
                                                    '<li><em>1</em> User</li>',
                                                    '<li><em>1</em> Website</li>',
                                                    '<li><em>1</em> Domain</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                    '</ul>',
                                '</li>',
                                '<li class="cd-popular">',
                                    '<ul class="cd-pricing-wrapper">',
                                        '<li data-type="monthly" class="is-visible">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Popular</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">60</span>',
                                                    '<span class="cd-duration">mo</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>512MB</em> Memory</li>',
                                                    '<li><em>3</em> Users</li>',
                                                    '<li><em>5</em> Websites</li>',
                                                    '<li><em>7</em> Domains</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                        '<li data-type="yearly" class="is-hidden">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Popular</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">630</span>',
                                                    '<span class="cd-duration">yr</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>512MB</em> Memory</li>',
                                                    '<li><em>3</em> Users</li>',
                                                    '<li><em>5</em> Websites</li>',
                                                    '<li><em>7</em> Domains</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                    '</ul>',
                                '</li>',
                                '<li>',
                                    '<ul class="cd-pricing-wrapper">',
                                        '<li data-type="monthly" class="is-visible">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Premier</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">90</span>',
                                                    '<span class="cd-duration">mo</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>1024MB</em> Memory</li>',
                                                    '<li><em>5</em> Users</li>',
                                                    '<li><em>10</em> Websites</li>',
                                                    '<li><em>10</em> Domains</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                        '<li data-type="yearly" class="is-hidden">',
                                            '<header class="cd-pricing-header">',
                                                '<h2>Premier</h2>',
                                                '<div class="cd-price">',
                                                    '<span class="cd-currency">$</span>',
                                                    '<span class="cd-value">950</span>',
                                                    '<span class="cd-duration">yr</span>',
                                                '</div>',
                                            '</header>',
                                            '<div class="cd-pricing-body">',
                                                '<ul class="cd-pricing-features">',
                                                    '<li><em>1024MB</em> Memory</li>',
                                                    '<li><em>5</em> Users</li>',
                                                    '<li><em>10</em> Websites</li>',
                                                    '<li><em>10</em> Domains</li>',
                                                    '<li><em>Unlimited</em> Bandwidth</li>',
                                                    '<li><em>24/7</em> Support</li>',
                                                '</ul>',
                                            '</div>',
                                            '<footer class="cd-pricing-footer">',
                                                '<a class="cd-select" href="#">Select</a>',
                                            '</footer>',
                                        '</li>',
                                    '</ul>',
                                '</li>',
                            '</ul>',
                        '</div>',
                    '</div>',
                '</div>',
                '<div id="about">',
                    '<div class="container">',
                        '<div class="row">',
                            '<div class="col-xs-12 col-sm-6 col-sm-offset-3">',
                                '<div class="content-wrap">',
                                    '<section class="content-current">',
                                        '<h3><%= pages.about.title %></h3>',
                                        '<p>This is text about us</p>',
                                    '</section>',
                                '</div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>',
                '<footer>',
                    '<div class="container">',
                        '<p class="text-center no-s"><%= locale.copyright %></p>',
                    '</div>',
                '</footer>',
                '</body>',
                '<%- include(\'partials/footer\') %>'
            ].join('')
            },
            {
                stitle: 'about',
                title: 'About',
                langId: 1,
                message: [
                    '<%- include(\'partials/header\') %>',
                    '<body class="page-header-fixed compact-menu page-horizontal-bar">',
                        '<div class="overlay"></div>',
                        '<main class="page-content content-wrap">',
                            '<%- include(\'partials/navbar\') %>',
                            '<div class="page-sidebar sidebar horizontal-bar">',
                                '<div class="page-sidebar-inner">',
                                    '<%- include(\'partials/adaptiveMenu\') %>',
                                '</div>',
                            '</div>',
                            '<div class="page-inner">',
                                '<div id="main-wrapper" class="container">',
                                    'About',
                                '</div>',
                                '<%- include(\'partials/footerStripe\') %>',
                            '</div>',
                        '</main>',
                    '</body>',
                    '<%- include(\'partials/footer\') %>'
                ].join('')
        },
        {
            stitle: 'contacts',
            title: 'Contacts',
            langId: 1,
            message: [
                '<%- include(\'partials/header\') %>',
                '<body class="page-header-fixed compact-menu page-horizontal-bar">',
                    '<div class="overlay"></div>',
                    '<main class="page-content content-wrap">',
                            '<%- include(\'partials/navbar\') %>',
                            '<div class="page-sidebar sidebar horizontal-bar">',
                                    '<div class="page-sidebar-inner">',
                                        '<%- include(\'partials/adaptiveMenu\') %>',
                                    '</div>',
                            '</div>',
                            '<div class="page-inner">',
                                '<div id="main-wrapper" class="container">',
                                    'Contacts',
                                '</div>',
                                '<%- include(\'partials/footerStripe\') %>',
                            '</div>',
                    '</main>',
                '</body>',
                '<%- include(\'partials/footer\') %>'
            ].join('')
        }
    ];

    logger.info('MockService', 'init', 'start');

    params = params || {};

    if (typeof params.active !== 'undefined' && !params.active) {
        return callback ? callback() : null;
    }

    var noPage = params.noPage || false;

    var logging = sequelize.sequelize.options.logging;

    sequelize.sequelize.options.logging = false;

    return async.series([
        function(next) {

            return sequelize.sequelize.sync({
                force: true
            }).then(function() {

                return next() || null;
            }).catch(next);
        }/*,
        function(next) {

            if (noPage) {
                return next();
            }

            return sequelize.sequelize.query('TRUNCATE Pages').spread(function() {

                return next() || null;
            }).catch(next);
        },
        function(next) {

            return sequelize.sequelize.query('TRUNCATE Locales').spread(function() {

                return next() || null;
            }).catch(next);
        }*/,
        function(next) {

            return async.eachSeries(langs, function(lang, _next) {

                var stitle = lang.stitle;

                delete lang.stitle;

                return Lang.findOrCreate({
                    where: {
                        stitle: stitle
                    },
                    defaults: lang
                }).spread(function() {

                    return _next() || null;
                }).catch(next);
            }, next);
        },
        function(next) {

            return async.eachSeries(locales, function(locale, _next) {

                var value = locale.value;

                delete locale.value;

                return Locale.findOrCreate({
                    where: locale,
                    defaults: {
                        value: value
                    }
                }).spread(function() {

                    return _next() || null;
                }).catch(_next);
            }, next);
        },
        function(next) {

            if (noPage) {
                return next();
            }

            return async.eachSeries(pages, function(page, _next) {

                var value = page.value;

                delete page.value;

                return Page.findOrCreate({
                    where: page,
                    defaults: {
                        value: value
                    }
                }).spread(function() {

                    return _next() || null;
                }).catch(_next);
            }, next);
        }
    ], function(err) {

        logger.info('MockService', 'init', 'finish');

        sequelize.sequelize.options.logging = logging;

        if (err) {
            logger.error('MockService', 'init', err);
        }

        if (callback) {
            return callback(err);
        }
    });
};

module.exports = MockService;