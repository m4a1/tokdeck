'use strict';

var config = require('../config');
var async = require('async');
var Lang = require('../models').Lang;
var Page = require('../models').Page;
var logger = require('../lib/logger')({
    prefix: 'service.page'
});

var PageService = function(){

    var that = this;

    this.getById = function(id, callback) {

        return Page.find({
            where: {
                id: id
            }
        }).then(function(_mail) {

            return callback(null, _mail) || null;
        }).catch(function(err) {

            return callback({code: 0, message: 'DB error', original: err}) || null;
        });
    };

    this.getPage = function(param, callback) {

        var langId = param.langId;
        var stitle = param.stitle;
        var langDefault = null;
        var list = {};

        async.series([
            function(next) {

                return Lang.find({
                    where: {
                        isDefault: true
                    }
                }).then(function(_lang) {

                    if (!_lang) {
                        return next({code: 0, message: 'Lang default not found'}) || null;
                    }

                    langDefault = _lang;

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                var w = {
                    langId: langDefault.id
                };

                if (stitle) {
                    w.stitle = stitle;
                }

                return Page.findAll({
                    where: w
                }).then(function(pages) {

                    if (!pages) {
                        return next() || null;
                    }

                    pages.forEach(function(page) {
                        list[page.stitle] = page.dataValues;
                    });

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            },
            function(next) {

                if (langDefault.id === langId || !langId) {
                    return next();
                }

                var w = {
                    langId: langId
                };

                if (stitle) {
                    w.stitle = stitle;
                }

                return Page.findAll({
                    where: w
                }).then(function(pages) {

                    if (!pages) {
                        return next() || null;
                    }

                    pages.forEach(function(page) {
                        list[page.stitle] = page.dataValues;
                    });

                    return next() || null;
                }).catch(function(err) {

                    return next({code: 0, message: 'DB error', original: err}) || null;
                });
            }
        ], function(error) {
            return callback(error, list);
        });
    };
};

module.exports = PageService;