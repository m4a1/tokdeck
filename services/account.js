'use strict';

/* global sequelize */

var config = require('../config');
var Account = require('../models').Account;
var User = require('../models').User;
var accountStrategyService = require('./accounts');
var UserService = require('./user');

var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.account'
});

var AccountService = function() {};

AccountService.prototype.getAccount = function(param, callback) {

    logger.info('AccountService', 'getAccount', 'start');

    var that = this;
    var id = param.id;
    var account = null;

    return async.series([
        function(next) {

            return that.getAccounts({
                id: id
            }, function(error, accounts) {

                if (error) {
                    return next(error);
                }

                if (!accounts || !accounts.length) {
                    return next({code: 0, message: 'No account found'});
                }

                account = accounts[0];

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountService', 'getAccount', 'finish');

        if (err) {

            logger.error('AccountService', 'getAccount', err);

            return callback(err);
        }

        return callback(null, account);
    });
};

AccountService.prototype.createAccount = function(param, callback) {

    logger.info('AccountService', 'createAccount', 'start');

    var userService = new UserService();
    var profile = param.profile;
    var provider = param.provider;
    var userId = param.userId;
    var account = null;
    var user = null;
    var accountStrategy = null;
    var accountSpecific = null;
    var profileFormatted = null;
    var isOuterTransaction = !!param.transaction;
    var transaction = param.transaction || null;

    return async.series([
        function(next) {

            if (transaction) {
                return next();
            }

            return sequelize.sequelize.transaction().then(function(_transaction) {

                transaction = _transaction;

                return next() || null;
            });
        },
        function(next) {

            if (!userId) {
                return next();
            }

            return userService.getUser({
                id: userId,
                transaction: transaction
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            var AccountStrategy = accountStrategyService.use(provider);

            if (!AccountStrategy) {
                return next({code: 0, message: 'No AccountStrategy found', provider: provider});
            }

            accountStrategy = new AccountStrategy();

            return next();
        },
        function(next) {

            return Account.findOrCreate({
                where: {
                    userId: userId,
                    type: provider
                },
                defaults: {
                    userId: userId,
                    type: provider
                },
                transaction: transaction
            }).spread(function(_account) {

                account = _account;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return accountStrategy.createAccount({
                userId: userId,
                profile: profile,
                transaction: transaction
            }, function(err, _accountSpecific, _profileFormatted) {

                if (err) {
                    return next(err);
                }

                accountSpecific = _accountSpecific;
                profileFormatted = _profileFormatted;

                return next();
            });
        },
        function(next) {

            if (user) {
                return next();
            }

            return userService.createUser({
                provider: provider,
                profile: profileFormatted,
                transaction: transaction
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            return account.setUser(user, {
                transaction: transaction
            }).then(function() {

                account.User = user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return accountSpecific.setAccount(account, {
                transaction: transaction
            }).then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (isOuterTransaction) {
                return next();
            }

            return transaction.commit().done(function () {

                transaction = null;

                return next() || null;
            });
        }
    ], function(err) {

        logger.info('AccountService', 'createAccount', 'finish');

        if (err) {

            if (transaction && !isOuterTransaction) {
                transaction.rollback();
            }

            logger.error('AccountService', 'createAccount', err);

            return callback(err);
        }

        return callback(null, account);
    });
};

AccountService.prototype.updateAccount = function(param, callback) {

    logger.info('AccountService', 'updateAccount', 'start');

    var profile = param.profile;
    var transaction = param.transaction;
    var accountSpecific = param.accountSpecific;
    var account = null;
    var accountStrategy = null;

    return async.series([
        function(next) {

            return Account.find({
                where: {
                    id: accountSpecific.accountId
                },
                include: [
                    {
                        model: User,
                        required: true
                    }
                ]
            }, {
                transaction: transaction
            }).then(function(_account) {

                account = _account;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            var AccountStrategy = accountStrategyService.use(account.type);

            if (!AccountStrategy) {
                return next({code: 0, message: 'No AccountStrategy found', provider: account.type});
            }

            accountStrategy = new AccountStrategy();

            return next();
        },
        function(next) {

            return accountStrategy.updateAccount({
                accountSpecific: accountSpecific,
                profile: profile,
                transaction: transaction
            }, function(err, _accountSpecific) {

                if (err) {
                    return next(err);
                }

                accountSpecific = _accountSpecific;

                return next();
            });

            return next();
        }
    ], function(err) {

        logger.info('AccountService', 'updateAccount', 'finish');

        if (err) {

            logger.error('AccountService', 'updateAccount', err);

            return callback(err);
        }

        return callback(null, account);
    });
};

AccountService.prototype.formatAccount = function(account, callback) {

    logger.info('AccountService', 'formatAccount', 'start');

    var result = null;

    return async.series([
        function(next) {

            if (!account) {
                return next({code: 0, message: 'No specific'});
            }

            var type = null;
            var specific = null;

            for (var k in account) {

                if (!account[k]) {
                    continue;
                }

                var match = k.match(/^account(\w+)$/gi);

                if (!match) {
                    continue;
                }

                specific = {
                    name: account[k].name,
                    guid: account[k].guid,
                    url: account[k].url
                };

                type = match[0].replace('account', '').toLowerCase();
            }

            if (!type || !specific) {
                return next({code: 0, message: 'No type or specific'});
            }

            result = {
                id: account.id,
                type: type,
                specific: specific
            };

            return next();
        }
    ], function(err) {

        logger.info('AccountService', 'formatAccount', 'finish');

        if (err) {

            logger.error('AccountService', 'formatAccount', err);

            return callback(err);
        }

        return callback(null, result);
    });
};

AccountService.prototype.setupAccount = function(param, callback) {

    logger.info('AccountService', 'setupAccount', 'start');

    var that = this;

    var transaction = param.transaction;
    var provider = param.provider;
    var profile = param.profile;
    var userId = param.userId;
    var accountStrategy = null;
    var accountSpecific = null;
    var user = null;

    return async.series([
        function(next) {

            var AccountStrategy = accountStrategyService.use(provider);

            if (!AccountStrategy) {
                return next({code: 0, message: 'No AccountStrategy found', provider: provider});
            }

            accountStrategy = new AccountStrategy();

            return next();
        },
        function(next) {

            if (!profile) {
                return next({code: 0, message: 'Authorization Failed'});
            }

            return accountStrategy.getAccountSpecific({
                userId: userId,
                guid: profile.id,
                transaction: transaction
            }, function(err, _accountSpecific) {

                if (err && err.code !== 4) {
                    return next(err);
                }

                accountSpecific = _accountSpecific;

                return next();
            });
        },
        function(next) {

            if (!accountSpecific) {
                return next();
            }

            return that.updateAccount({
                accountSpecific: accountSpecific,
                profile: profile,
                transaction: transaction
            }, function(err, account) {

                if (err) {
                    return next(err);
                }

                user = account.User;

                return next();
            });
        },
        function(next) {

            if (accountSpecific) {
                return next();
            }

            return that.createAccount({
                provider: provider,
                profile: profile,
                userId: userId,
                transaction: transaction
            }, function(err, account) {

                if (err) {
                    return next(err);
                }

                user = account.User;

                return next();
            });
        }
    ], function(err) {

        logger.info('AccountService', 'setupAccount', 'finish');

        if (err) {

            logger.error('AccountService', 'setupAccount', err);

            return callback(err);
        }

        return callback(null, user);
    });
};

module.exports = AccountService;