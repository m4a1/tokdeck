'use strict';

var config = require('../config');
var logger = require('../lib/logger')({
    prefix: 'service.cache'
});

var CacheService = function () {

    var that    = this;

    this.set = function(param, callback) {

        if (!config.cache) {
            return callback();
        }

        var type = param.type, data = param.data;

        if (!process.cache) {
            process.cache = {};
        }

        process.cache[type] = data;

        return callback();
    };

    this.get = function(param, callback) {

        if (!config.cache) {
            return callback();
        }

        var type = param.type, out = null;

        if (process.cache && process.cache[type]) {
            out = process.cache[type];
        }

        return callback(null, out);
    };
};

module.exports = CacheService;