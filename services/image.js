'use strict';

var config = require('../config');
var Image = require('../models').Image;
var UtilService = require('./util');
var fs = require('fs-extra');
var fsreal = require('fs');
var imagesize = require('image-size');
var imagemagick = require('imagemagick-native');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'service.image'
});

var ImageService = function () {};

ImageService.prototype.add = function(url, validators, callback) {

    var that = this;
    var utilService = new UtilService();
    var image = null, imagePath = null, ext = 'jpg';
    var minWidth = validators && validators.minWidth ? validators.minWidth : 0;
    var minHeight = validators && validators.minHeight ? validators.minHeight : 0;
    var aspechWhMax = validators && validators.aspechWhMax ? validators.aspechWhMax : 0;
    var storeOriginal = validators && validators.storeOriginal;
    var dir = '';

    async.series([
        function(next) {

            if (!url) {
                return next({code: 0, message: 'url not set'});
            }

            dir = storeOriginal ? 'original' : that.getDir(url);

            var parts = url.split('.');

            var _ext = String(parts[parts.length - 1]).toLowerCase();

            if (_ext === 'gif' || _ext === 'png' || _ext === 'jpg' || _ext === 'jpeg') {
                ext = _ext;
            }

            return next();
        },
        function(next) {

            return Image.find({
                where: {
                    url: url
                }
            }).then(function(_image) {

                if (!_image) {
                    return next() || null;
                }

                image = _image;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'db get error', original: err}) || null;
            });
        },
        function(next) {

            if (!image || !storeOriginal) {
                return next();
            }

            if (storeOriginal) {
                return callback ? callback(null, image) : null;
            }

            return fsreal.exists(config.image.storage + '/' + dir + '/' + image.id + '.' + ext, function(exists) {

                if (exists) {
                    return callback ? callback(null, image) : null;
                }

                return next();
            });
        },
        function(next) {

            if (storeOriginal) {
                return next();
            }

            return fsreal.exists(url, function(exists) {

                if (exists) {

                    imagePath = url;

                    return next();
                } else {

                    return utilService.downloadImg({
                        url: url
                    }, function(error, result) {

                        if (error) {
                            return next(error);
                        }

                        if (!result) {
                            return next({code: 0, message: 'image saving error'});
                        }

                        imagePath = result;

                        return next();
                    });
                }
            });
        },
        function(next) {

            if ((minWidth === 0 && minHeight === 0 && aspechWhMax === 0) || storeOriginal) {
                return next();
            }

            return imagesize(imagePath, function(error, dimensions) {

                if (error) {
                    return next({code: 0, message: 'validation error', original: error});
                }

                // minWidth
                if (minWidth !== 0 && dimensions.width < minWidth) {
                    return next({code: 0, message: 'validation error minWidth', minWidth: minWidth, width: dimensions.width});
                }

                // minHeight
                if (minHeight !== 0 && dimensions.height < minHeight) {
                    return next({code: 0, message: 'validation error minHeight', minHeight: minHeight, height: dimensions.height});
                }

                // aspect wh
                if (aspechWhMax !== 0 && dimensions.width / dimensions.height > aspechWhMax) {
                    return next({code: 0, message: 'validation error aspechWhMax', aspechWhMax: aspechWhMax, aspechWh: dimensions.width / dimensions.height});
                }

                return next();
            });
        },
        function(next) {

            if (image) {
                return next();
            }

            return Image.create({
                url: url,
                ext: ext,
                dir: dir
            }).then(function(_image) {

                image = _image;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'db save error', original: err}) || null;
            });
        },
        function(next) {

            if (!image || !storeOriginal) {
                return next();
            }

            if (callback) {
                return callback(null, image);
            }
        },
        function(next) {

            return fs.ensureDir(config.image.storage + '/' + dir, function(error) {

                if (error) {
                    return next({code: 0, message: 'directory operation error', original: error});
                }

                return next();
            });
        },
        function(next) {

            return fs.copy(imagePath, config.image.storage + '/' + dir + '/' + image.id + '.' + ext, function(error) {

                if (error) {
                    return next({code: 0, message: 'file copy error', original: error});
                }

                return next();
            });
        },
        function(next) {

            return fsreal.chmod(config.image.storage + '/' + dir + '/' + image.id + '.' + ext, 755, function(error) {

                if (error) {
                    return next({code: 0, message: 'file chmod error', original: error});
                }

                return next();
            });
        },
        function(next) {

            var imagesPreviews = [];

            for (var k in config.image.preview) {
                imagesPreviews.push(config.image.preview[k]);
            }

            return async.eachSeries(imagesPreviews, function(preview, _next) {

                return that.touchPreview(image, preview, function(error) {
                    return _next();
                });
            }, function() {
                return next();
            });
        }
    ], function(error) {

        return async.series([
            function(next) {

                if (!imagePath) {
                    return next();
                }

                return fsreal.unlink(imagePath, function() {
                    return next();
                });
            },
            function(next) {

                if (!error || !image) {
                    return next();
                }

                return fsreal.unlink(config.image.storage + '/' + dir + '/' + image.id + '.' + ext, function() {
                    return next();
                });
            },
            function(next) {

                if (!error || !image) {
                    return next();
                }

                return image.destroy().then(function() {

                    image = null;

                    return next() || null;
                }).catch(function() {

                    image = null;

                    return next() || null;
                });
            }
        ], function() {

            if (callback) {
                return callback(error, image);
            }
        });
    });
};

ImageService.prototype.getPreviewFn = function(image, preview) {

    preview = typeof preview === 'string' ? config.image.preview[preview] : preview;

    if (image.ext === 'gif') {
        preview = null;
    }

    var fnArray = [];

    if (preview) {

        if (preview.width) {
            fnArray.push(preview.width);
        }

        if (preview.height) {
            fnArray.push(preview.height);
        }

        if (preview.resizeStyle) {
            fnArray.push(preview.resizeStyle);
        }

        if (preview.gravity) {
            fnArray.push(preview.gravity);
        }
    }

    var fn = fnArray.join('_');

    fn += (fn.length ? '_' : '') + image.id + '.' + image.ext;

    return {
        preview: preview,
        fn: fn,
        path: (preview ? config.image.cache : config.image.storage) + '/' + image.dir + '/' + fn
    };
};

ImageService.prototype.touchPreview = function(image, preview, callback) {

    var that = this;
    var ret = that.getPreviewFn(image, preview);
    var exists = false;
    var fn = ret.fn;
    var path = ret.path;

    preview = ret.preview;

    async.series([
        function(next) {

            if (!preview) {
                return next();
            }

            return fsreal.exists(config.image.storage + '/' + image.dir + '/' + fn, function(_exists) {

                exists = _exists;

                return next();
            });
        },
        function(next) {
            if (!preview || exists) return next();
            fs.ensureDir(config.image.cache + '/' + image.dir, function() {
                return next();
            });
        },
        function(next) {

            if (!preview || exists) {
                return next();
            }

            preview.srcData = fsreal.readFileSync(config.image.storage + '/' + image.dir + '/' + image.id + '.' + image.ext);
            preview.ignoreWarnings = true;

            return imagemagick.convert(preview, function(error, data) {

                if (error) {
                    return next({code: 0, message: 'Conver error', original: error});
                }

                fsreal.writeFileSync(config.image.cache + '/' + image.dir + '/' + fn, data);

                return next();
            });
        },
        function(next) {

            if (!preview || exists) {
                return next();
            }

            return fsreal.chmod(config.image.cache + '/' + image.dir + '/' + fn, 755, function(error) {

                if (error) {
                    return next({code: 0, message: 'file chmod error', original: error});
                }

                return next();
            });
        }
    ], function(error) {

        if (error) {
            return callback(error, null);
        }

        return callback(null, {
            fn: fn,
            preview: preview,
            path: path
        });
    });
};

ImageService.prototype.getUrlFromImage = function(image, preview, callback) {

    var that = this;
    if (image && image.dir === 'original') {
        return callback(null, image.url);
    }

    var ret = that.getPreviewFn(image, preview);
    var parts = image.dir.split('/');

    if (!parts || !parts[0]) {
        parts = [0];
    }

    return callback(null, config.image.static.replace('*', parts[0]) + '/' + (ret.preview ? 'cache' : 'images') + '/' + image.dir + '/' + ret.fn);
};

ImageService.prototype.getUrl = function(id, preview, callback) {

    var that = this;

    return this.get(id, function(error, image) {

        if (error) {
            return callback(error);
        }

        return that.getUrlFromImage(image, preview, function(error, url) {
            return callback(error, url);
        });
    });
};

ImageService.prototype.getPath = function(id, preview, callback) {

    var that = this;

    return this.get(id, function(error, image) {

        if (error) {
            return callback(error);
        }

        var ret = that.getPreviewFn(image, preview);

        return callback(null, ret.path);
    });
};

ImageService.prototype.get = function(id, callback) {

    var image = null;

    return async.series([
        function(next) {

            return Image.find({
                where: {
                    id: id
                }
            }).then(function(_image) {

                if (!_image) {
                    return next({code: 0, message: 'image not found'}) || null;
                }

                image = _image;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'image get error', original: err}) || null;
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, image);
        }
    });
};

ImageService.prototype.delete = function(id, callback) {

    var image = null;

    return async.series([
        function(next) {

            return Image.find({
                where: {
                    id: id
                }
            }).then(function(_image) {

                if (!_image) {
                    return next({code: 0, message: 'image not found'}) || null;
                }

                image = _image;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'image get error', original: err}) || null;
            });
        },
        function(next) {

            return image.destroy().then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'image deletion error', original: err}) || null;
            });
        },
        function(next) {

            return fs.remove(config.image.storage + '/' + image.dir + '/' + image.id + '.' + image.ext, function() {
                return next();
            });
        }
    ], function(error) {

        if (callback) {
            return callback(error, image);
        }
    });
};

ImageService.prototype.getDir = function(url) {

    var md5 = require('crypto').createHash('md5').update(url).digest("hex");

    return md5.charAt(0) + '/' + md5.charAt(1) + '/' + md5.charAt(2);
};

module.exports = ImageService;
