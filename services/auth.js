'use strict';

var config = require('../config');
var _ = require('lodash');
var async = require('async');
var jwt = require('jsonwebtoken');
var credential = require('credential');
var moment = require('moment');
var expressJwt = require('express-jwt');
var AccessToken = require('../models').AccessToken;
var User = require('../models').User;
var UtilService = require('./util');
var UserService = require('./user');
var logger = require('../lib/logger')({
    prefix: 'service.auth'
});

var AuthService = function() {};

AuthService.prototype.oauthAuthorize = function(params, callback) {

    logger.info('AuthService', 'oauthAuthorize', 'start');

    var validateJwt = expressJwt({
        secret: config.jwtSecret
    });
    var token = 'error';
    var tokenRemote = null;
    var scopeRemote = params.scope;
    var req = params.req;
    var scope = [];
    var user = null;

    if (!req.headers.authorization && req.query && req.query.token) {
        req.headers.authorization = 'Bearer ' + req.query.token;
    }

    return async.series([
        function(next) {

            if ((!scopeRemote || !scopeRemote.length) && !req.headers.authorization) {
                return next(true);
            }

            if (!req.headers.authorization) {
                return next({code: 0, message: 'No authorization header'});
            }

            token = req.headers.authorization.replace(/^Bearer\ /, '');

            if (!token) {
                return next({code: 0, message: 'No token found in header'});
            }

            return validateJwt(req, req.res, function() {

                if (!req.user) {
                    return next({code: 0, message: 'No token found'});
                }

                tokenRemote = req.user;

                return next();
            });
        },
        function(next) {

            return AccessToken.find({
                where: {
                    token: token
                }
            }).then(function(accessToken) {

                if (!accessToken) {
                    return next({code: 0, message: 'No token found'}) || null;
                }

                if (accessToken.isRevoked) {
                    return next({code: 0, message: 'Token revoked'}) || null;
                }

                if ((new moment(accessToken.expiredAt)).isBefore(new moment())) {
                    return next({code: 0, message: 'Token expired'}) || null;
                }

                if (!tokenRemote.userId  || tokenRemote.userId !== accessToken.userId) {
                    return next({code: 0, message: 'Token broken'}) || null;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            return User.find({
                where: {
                    id: tokenRemote.userId
                }
            }).then(function(_user) {

                if (!_user) {
                    return next() || null;
                }

                user = _user;

                scope = config.oauth.scope.user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (!user) {
                return next({code: 0, message: 'User not found'});
            }

            var diff = _.difference(scopeRemote, scope);

            if (diff.length) {
                return next({code: 0, message: 'Scope mismatch: ' + diff.join(', ')});
            }

            return next();
        }
    ], function(err) {

        logger.info('AuthService', 'oauthAuthorize', 'finish');

        if (err && err !== true) {

            logger.error('AuthService', 'oauthAuthorize', err);

            return callback(err);
        }

        return callback(null, user.id);
    });
};

AuthService.prototype.oauthCreateToken = function(params, callback) {

    logger.info('AuthService', 'oauthCreateToken', 'start');

    var token = 'error';
    var user = null;
    var scopeRemote = [];
    var scopeLocal = [];
    var scope = params.scope;
    var state = params.state;
    var realm = params.realm;
    var userId = params.userId;
    var redirect_uri = params.redirect_uri;
    var accessTokenExpire = params.accessTokenExpire || config.oauth.accessTokenExpire;
    var utilService = new UtilService();
    var userService = new UserService();

    return async.series([
        function(next) {

            if (!scope) {
                return next({code: 0, message: 'Scope not set'});
            }

            if (!redirect_uri) {
                return next({code: 0, message: 'Redirect Uri not set'});
            }

            if (state !== config.oauth.state) {
                return next({code: 0, message: 'Incorrect state'});
            }

            if (realm !== config.oauth.realm) {
                return next({code: 0, message: 'Incorrect realm'});
            }

            if (!userId) {
                return next({code: 0, message: 'User ID not set'});
            }

            scopeRemote = scope.split(',');

            return next();
        },
        function(next) {

            return User.find({
                where: {
                    id: userId
                }
            }).then(function(_user) {

                if (!_user) {
                    return next({code: 0, message: 'User not found'}) || null;
                }

                user = _user;

                scopeLocal = config.oauth.scope.user;

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (scope === 'all') {
                scopeRemote = scopeLocal;
            }

            var diff = _.difference(scopeRemote, scopeLocal);

            if (diff.length) {
                return next({code: 0, message: 'Scope mismatch: ' + diff.join(', ')});
            }

            return next();
        },
        function(next) {

            return AccessToken.find({
                where: {
                    userId: user.id,
                    isRevoked: false,
                    scope: scope,
                    expiredAt: {
                        $lt: (new moment()).add(1, 'days').toDate()
                    }
                }
            }).then(function(accessToken) {

                if (accessToken) {
                    token = accessToken.token;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (token !== 'error') {
                return next();
            }

            token = jwt.sign({
                userId: user.id,
                string: utilService.randomString(20)
            }, config.jwtSecret, {
                expiresIn: accessTokenExpire
            });

            return AccessToken.create({
                token: token,
                userId: user.id,
                scope: scope,
                expiredAt: (new moment()).add(accessTokenExpire, 'seconds')
            }).then(function() {

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        }
    ], function(err) {

        logger.info('AuthService', 'oauthCreateToken', 'finish', token);

        if (err && err !== true) {

            logger.error('AuthService', 'oauthCreateToken', err);

            return callback(err);
        }

        return callback(null, {
            token: token,
            redirect_uri: redirect_uri,
            state: state
        });
    });
};

module.exports = AuthService;