'use strict';

var winston = require('winston');
var moment = require('moment');

module.exports = function(params) {

    params = params || {};
    var prefix = params.prefix || 'default';

    function ts() {
        return (new moment()).format('YYYY-MM-DD hh:mm:ss');
    }

    var d = {
        transports: [
            new (winston.transports.Console)({
                colorize: true,
                timestamp: ts
            }),
            new winston.transports.File({
                filename: 'logs/' + prefix + '.error.log',
                level: 'error',
                maxsize: 100000,
                maxFiles: 10,
                timestamp: ts
            })
        ]
    };

    if (!process.exceptionHandlersInited) {

        d.exceptionHandlers = [
            new (winston.transports.Console)({
                colorize: true,
                timestamp: ts
            }),
            new winston.transports.File({
                filename: 'logs/exceptions.log',
                level: 'error',
                maxsize: 1000000,
                maxFiles: 10,
                timestamp: ts
            })
        ];

        process.exceptionHandlersInited = true;
    }

    return new winston.Logger(d);
};