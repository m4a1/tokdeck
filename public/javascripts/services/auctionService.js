tokdeck.factory('auctionService', function($rootScope, apiService) {

    var add = function(data, callback) {
        return apiService.run('auction', 'save', data, callback);
    };

    var load = function(callback) {
        return apiService.run('auction', 'get', {}, callback);
    };

    var get = function(param, callback) {
        return apiService.run('auction_card', 'get', param, callback);
    };

    var update = function(param, callback) {
        return apiService.run('auction_card', 'put', param, callback);
    };

    var remove = function(param, callback) {
        return apiService.run('auction_card', 'delete', param, callback);
    };

    return {
        add: add,
        load: load,
        get: get,
        update: update,
        remove: remove
    };
});