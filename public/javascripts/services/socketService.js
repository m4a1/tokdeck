tokdeck.factory('socketService', function ($rootScope, configService) {
    var setUser =  function(user) {

        if (typeof io === 'undefined') {
            return;
        }

        $rootScope.socket.emit('setUser', {
            id: user && user.id ? user.id : 0,
            tid: sessionStorage.uuid
        });
    };

    var runMessageHook = function(message) {

        if (!message.stitle) {
            return;
        }

        switch (message.stitle) {
            default:
                break;
        }
    };

    var messageRead = function(id) {

        if (typeof io === 'undefined' || !id) {
            return;
        }

        $rootScope.socket.emit('messageRead', {
            id: id
        });
    };

    var init =  function(user) {

        if (typeof io === 'undefined') {
            return;
        }

        $rootScope.socket = io(configService.frontendUrl, {secure: configService.frontendUrl.indexOf('https:') !== -1});

        setUser(user);

        $rootScope.socket.on('messageSent', function (data) {

            if (!data || data.error || !data.result) {
                return;
            }

            var message = data.result;

            runMessageHook(message);

            return $rootScope.$broadcast('messageSent', {
                message: message
            });
        });

        $rootScope.socket.on('reconnect', function () {
            return setUser($rootScope.user);
        });
    };

    return {
        init: init,
        setUser: setUser,
        messageRead: messageRead
    };
});