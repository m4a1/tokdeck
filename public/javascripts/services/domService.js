tokdeck.factory('domService', function($cookies, $rootScope, $timeout, localeService, userService, socketService) {

    var flash = function(param) {

        param.timeout = typeof param.timeout === 'undefined' ? 4000: param.timeout;

        if (!param.style) {
            param.style = 'success';
        }

        if (!param.id) {
            param.id = String((new Date()).getTime() + Math.random());
        }

        var message = _.extend({}, param);

        return toastr[param.style](message.message, message.title);
    };

    var processError = function(error, callback) {
        var message = null;
        var stopMessage = false;

        if (error) {

            $rootScope.offline = typeof error.status !== 'undefined' && (!error.status || error.status > 500);

            if ($rootScope.offline) {
                stopMessage = true;
            }

            if (error.login) {

                if ($rootScope.token) {
                    userService.signout();
                }

                $rootScope.checkLogin();

                stopMessage = true;
            }

            if (!message) {
                message = localeService.data['error_' + error.code];
            }

            if (!message) {
                message = error.message;
            }

            if (!message) {
                message = localeService.data.error_unknown;
            }
        } else {
            stopMessage = true;
        }

        if (!stopMessage) {
            flash({
                style: 'error',
                message: message
            });
        }

        if (callback) {
            return callback();
        }
    };

    var messageRead = function(id) {
        socketService.messageRead(id);
    };

    var init = function() {

        $rootScope.$on('messageSent', function (event, args) {

            var message = args.message;

            if (!message) {
                return;
            }

            if (message.view === 'flash') {
                if (!message.persistent) {
                    messageRead(message.id);
                }

                flash({
                    message: typeof localeService.data[message.message] === 'undefined' ? message.message : localeService.data[message.message],
                    title: message.title,
                    style: message.style,
                    timeout: message.timeout
                });
            }
        });
    };

    return {
        processError: processError,
        flash: flash,
        init: init,
        messageRead: messageRead
    };
});