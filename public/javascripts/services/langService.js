tokdeck.factory('langService', function(apiService) {

    var list = function(callback) {
        return apiService.run('lang', 'get', {}, callback);
    };

    return {
        list: list
    };
});