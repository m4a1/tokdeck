tokdeck.factory('productService', function($rootScope, apiService) {
    
    var add = function(data, callback) {
        return apiService.run('product', 'save', data, callback);
    };

    var load = function(param, callback) {
        return apiService.run('product', 'get', param, callback);
    };

    var get = function(param, callback) {
        return apiService.run('product_card', 'get', param, callback);
    };

    var update = function(param, callback) {
        return apiService.run('product_card', 'put', param, callback);
    };

    var remove = function(param, callback) {
        return apiService.run('product_card', 'delete', param, callback);
    };

    return {
        add: add,
        load: load,
        get: get,
        update: update,
        remove: remove
    };
});