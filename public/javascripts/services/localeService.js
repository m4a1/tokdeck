tokdeck.factory('localeService', function(apiService, configService) {

    var data = {
        error_unknown: 'Application error',
        offline: 'API is offline. Reload page'
    };

    var init = function(param, callback) {

        return load(param, function(error, result) {
            callback(error, result);
        });
    };

    var load = function(param, callback) {

        var langId = param.langId || 0;

        return apiService.run('locale', 'get', {
            langId: langId
        }, function(error, result) {

            if (result) {
                _.extend(data, result);
            }

            return callback(error, data);
        });
    };

    var get = function(key) {
        return data[key];
    };

    return {
        data: data,
        init: init,
        load: load,
        get: get
    };
});