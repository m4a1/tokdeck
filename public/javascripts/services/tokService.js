tokdeck.factory('tokService', function($rootScope, apiService) {
    
    var add = function(data, callback) {
        return apiService.run('tok', 'save', data, callback);
    };

    var load = function(param, callback) {
        return apiService.run('tok', 'get', param, callback);
    };

    var get = function(param, callback) {
        return apiService.run('tok_card', 'get', param, callback);
    };

    var update = function(param, callback) {
        return apiService.run('tok_card', 'put', param, callback);
    };

    var remove = function(param, callback) {
        return apiService.run('tok_card', 'delete', param, callback);
    };

    return {
        add: add,
        load: load,
        get: get,
        update: update,
        remove: remove
    };
});