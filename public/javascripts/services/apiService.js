tokdeck.factory('apiService', function($resource, configService, $rootScope) {

    var resources = {
        locale: $resource(configService.apiUrl + '/locale/:langId', {localeId: '@langId'}),
        account: $resource(configService.apiUrl + '/account', {token: '@token'}),
        user_profile: $resource(configService.apiUrl + '/user/me', {token: '@token', silent: '@silent'}, {put: {method: 'PUT'}}),
        auction: $resource(configService.apiUrl + '/auction', {token: '@token'}),
        auction_card: $resource(configService.apiUrl + '/auction/:auctionId', {token: '@token', auctionId: '@auctionId'}, {put: {method: 'PUT'}}),
        product: $resource(configService.apiUrl + '/product', {token: '@token', auctionId: '@auctionId'}),
        product_card: $resource(configService.apiUrl + '/product/:productId', {token: '@token', productId: '@productId'}, {put: {method: 'PUT'}}),
        tok: $resource(configService.apiUrl + '/tok', {token: '@token', auctionId: '@auctionId'}),
        tok_card: $resource(configService.apiUrl + '/tok/:tokId', {token: '@token', tokId: '@tokId'}, {put: {method: 'PUT'}}),
        user_remind: $resource(configService.apiUrl + '/user/remind', {token: '@token', tid: '@tid'}, {put: {method: 'PUT'}}),
        user_signin: $resource(configService.apiUrl + '/user/signin/local', {tid: '@tid'}),
        lang: $resource(configService.apiUrl + '/lang')
    };

    var run = function(point, method, param, callback) {

        if (!param.token && $rootScope.token) {
            param.token = $rootScope.token;
        }

        return resources[point][method](param, function(result) {

            if (typeof result.error !== 'undefined' || typeof result.result !== 'undefined') {
                return callback(result.error, result.result);
            }

            if (typeof result.code !== 'undefined' || typeof result.message !== 'undefined') {
                return callback(result, null);
            }

            return callback(null, result);
        }, callback);
    };

    return {
        resources: resources,
        run: run
    };
});