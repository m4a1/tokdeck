tokdeck.factory('startService', function($rootScope, $state, domService, userService) {

    var onboarding = function(param, callback) {

        $rootScope.onboarding = true;

        $state.go('onboarding');

        if (callback) {
            return callback();
        }
    };

    var continueAfterOnboarding = function(param, callback) {

      return async.series([
            function(next) {

                return userService.init({
                    reinit: true
                }, next);
            }
      ], function(error) {

            domService.processError(error);

            $rootScope.onboarding = false;

            $state.go($rootScope.continueState.state, $rootScope.continueState.param);

            $rootScope.continueState = {
                state: 'home',
                param: {}
            };

            if (callback) {
                return callback();
            }
      });
    };

    var init = function(param, callback) {

        if ($rootScope.user && !$rootScope.user.onboardingPassed) {
            onboarding();
        } else {
            continueAfterOnboarding();
        }

        return callback();
    };

    return {
        init: init,
        onboarding: onboarding,
        continueAfterOnboarding: continueAfterOnboarding
    };
});