tokdeck.factory('userService', function($rootScope, apiService, $cookies, configService, $location, utilService) {

    $rootScope.user = null;

    var user = function() {
        return $rootScope.user;
    };

    var signin = function(data, callback) {

        data.tid = sessionStorage.uuid;

        return apiService.run('user_signin', 'save', data, callback);
    };

    var signup = function(data, callback) {

        data.tid = sessionStorage.uuid;

        return apiService.run('user_signin', 'save', data, callback);
    };

    var remind = function(data, callback) {

        data.token = $rootScope.token;
        data.tid = sessionStorage.uuid;

        return apiService.run('user_remind', 'save', data, callback);
    };

    var remindcontinue = function(data, callback) {

        data.token = $rootScope.token;
        data.tid = sessionStorage.uuid;

        return apiService.run('user_remind', 'put', data, callback);
    };

    var update = function(data, callback) {

        data.token = $rootScope.token;

        return apiService.run('user_profile', 'put', data, callback);
    };

    var load = function(callback) {
        return apiService.run('user_profile', 'get', {}, callback);
    };

    var init = function(param, callback) {

        if (!param.reinit) {
            $rootScope.user = null;
        }

        if ($location.search().auth) {

            try {

                var user = JSON.parse(utilService.base64Decode($location.search().auth));

                $rootScope.token = user.token;

                $cookies.put(user.cookie.name, user.token, user.cookie);

                return window.location = configService.frontendUrl + '/dashboard';
            } catch (e) {}
        }

        if (!$rootScope.token) {
            return callback();
        }

        return load(function(error, result) {

            if (error) {
                error.login = true;
            }

            if (!error && !result) {
                error = {login: true};
            }

            if (result) {
                $rootScope.user = result;
            }

            return callback(error, result);
        });
    };

    var signout = function() {

        var uuid = sessionStorage.uuid;

        sessionStorage.clear();
        sessionStorage.uuid = uuid;

        $rootScope.user = null;
        $rootScope.token = null;

        delete $rootScope.onboarding;

        return $cookies.remove('token', {
            domain: configService.cookie.domain
        });
    };

    return {
        signup: signup,
        signin: signin,
        signout: signout,
        user: user,
        init: init,
        load: load,
        update: update,
        remind: remind,
        remindcontinue: remindcontinue
    };
});