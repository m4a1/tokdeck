tokdeck.

directive('textheight', function ($rootScope, $timeout) {
    return {
        link: function (scope, element, attr) {

            $timeout(function() {

                var height = element.height();

                element.addClass('td-home-item-text-expanded');

                var heightNew = element.height();

                element.removeClass('td-home-item-text-expanded');

                if (heightNew === height) {
                    eval('scope.' + attr.textheight + ' = false');
                }
                else {
                    eval('scope.' + attr.textheight + ' = true');
                }
            }, 200);
        }
    };
});