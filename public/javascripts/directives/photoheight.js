tokdeck.

directive('photoheight', function ($rootScope, $timeout) {
    return {
        link: function (scope, element, attr) {
            return $timeout(function() {

                var height = element.height();

                element.addClass('td-home-item-photo-expanded');

                var heightNew = element.height();

                element.removeClass('td-home-item-photo-expanded');

                if (heightNew === height) {
                    eval('scope.' + attr.photoheight + ' = false');
                } else {

                    eval('scope.' + attr.photoheight + ' = true');

                    element.find('img:first').css({
                        marginTop: Math.floor((heightNew - height) / -2) + 'px'
                    });
                }

            }, 200);
        }
    };
});