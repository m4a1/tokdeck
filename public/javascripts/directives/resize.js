tokdeck.

directive('resize', function ($window, $rootScope) {
    return {
        link: function () {

            var w = angular.element($window);

            var onResize = function() {

                var width = w.width();

                $rootScope.screenWide = width > 865;

                if (width >= 1200) {
                    $rootScope.screenSize = 'lg';
                }
                else if (width >= 992) {
                    $rootScope.screenSize = 'md';
                }
                else if (width >= 768) {
                    $rootScope.screenSize = 'sm';
                }
                else {
                    $rootScope.screenSize = 'xs';
                }
            };

            w.bind('resize', onResize);

            onResize();
        }
    };
});