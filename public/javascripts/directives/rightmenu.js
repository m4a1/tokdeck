tokdeck.

directive('rightmenu', function ($window, $rootScope, $ionicSideMenuDelegate) {
    return {
        link: function (scope, element) {

            var w = angular.element($window);

            var c = angular.element('.td-menu-content');

            var onResize = function() {

                $ionicSideMenuDelegate.toggleLeft(false);
                $ionicSideMenuDelegate.toggleRight(false);

                var width = w.width();

                if ($rootScope.screenWide) {
                    c.addClass('td-menu-content-width');
                }
                else {
                    c.removeClass('td-menu-content-width');
                }

                var rightWidth = $rootScope.screenWide ? width - c.width() : width * 0.9;

                if (width - rightWidth < 50) {
                    rightWidth = width - 50;
                }

                element.width(rightWidth);

                $ionicSideMenuDelegate._instances[0].right.width = rightWidth;
            };

            w.bind('resize', onResize);
            
            onResize();
        }
    };
});