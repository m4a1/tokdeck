tokdeck.

filter('facebookhashtags', function() {
    return function(text) {

        var found = text.match(/\#([^\ $\n\t]*)/gi);

        if (found) {

            for (var k in found) {
                text = text.replace(found[k], '<a href="https://www.facebook.com/hashtag/' + found[k].replace('#', '') + '" target="_blank">' + found[k] + '</a>');
            }
        }
        
        return text;
    };
});