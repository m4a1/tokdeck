tokdeck.

filter('findlinks', function() {
    return function(text) {

        var found = text.match(/[^\"]http(s|)\:\/\/([^\ $\n\t]*)/gi);

        if (found) {

            for (var k in found) {

                var first = found[k].slice(0, 1);

                var href = found[k].slice(1);

                text = text.replace(found[k], first + '<a href="' + href + '" target="_blank">' + href + '</a>');
            }
        }
        
        return text;
    };
});