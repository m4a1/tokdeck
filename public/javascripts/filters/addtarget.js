tokdeck.

filter('addtarget', function() {
    return function(text) {

        var found = null;

        var r = /\<a([^\>]*)href\=\"(.+?)\"([^\>]*)\>/gi;

        do {

            found = r.exec(text);

            if (found && text.indexOf('target=') === -1) {
                text = text.replace(found[0], '<a target="_blank"' + found[1] + 'href="' + found[2] + '"' + found[3] + '>');
            }
        }
        while(found);
    };
});