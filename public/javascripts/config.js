window.config = {
    envs: {
        development: {
            cookie: {
                domain: '.tokdeck.com'
            },
            frontendUrl: 'https://tokdeck.com',
            apiUrl: 'https://api.tokdeck.com/v1'
        }
    },
    auction: {
        def: '%dev_auction_config%'
    }
};

