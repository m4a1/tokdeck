tokdeck.

controller('socialController', ['$rootScope', '$scope', 'configService', '$window', '$timeout', function($rootScope, $scope, configService, $window, $timeout) {

    $scope.go = function(type) {

        $rootScope.loading({
            delay: 0
        });

        return $timeout(function() {

            return $window.location = configService.apiUrl + '/user/signin/' + type + (sessionStorage.uuid ? '?tid=' + sessionStorage.uuid : '');
        }, 400);
    };
}]);
