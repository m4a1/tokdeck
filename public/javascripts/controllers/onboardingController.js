tokdeck.

controller('onboardingController', ['$rootScope', '$state', '$scope', 'userService', 'domService', 'startService', function($rootScope, $state, $scope, userService, domService, startService) {

    if (!$rootScope.onboarding) {
        return $state.go('home');
    }

    if ($rootScope.user.onboardingPassed) {

        startService.continueAfterOnboarding();

        return $state.go('home');
    }

    $scope.clear = function() {

        $scope.onboarding = {
            username: '',
            name: '',
            email: '',
            password: '',
            password_repeat: ''
        };

        if (!$rootScope.user.username && $rootScope.user.name) {
            $scope.onboarding.username = $rootScope.user.name;
        }

        if ($scope.form_onboarding) {
            return $scope.form_onboarding.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                var data = {
                    silent: true
                };

                for (var k in $scope.onboarding) {

                    if (!$rootScope.user[k] && k !== 'password_repeat') {
                        data[k] = $scope.onboarding[k];
                    }
                }

                return userService.update(data, next);
            },
            function(next) {

                return $rootScope.initCoreData({
                    reinit: true
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {

                $scope.clear();

                startService.continueAfterOnboarding();

                return $state.go('home');
            }
        });
    };
}]);
