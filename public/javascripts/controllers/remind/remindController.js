tokdeck.

controller('remindController', ['$rootScope', '$scope', 'domService', 'userService', '$state', function($rootScope, $scope, domService, userService, $state) {

    $scope.clear = function() {

        $scope.remind = {
            email: ''
        };

        if ($scope.form_remind) {
            $scope.form_remind.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        var email = $scope.remind.email;

        $rootScope.loading(true);

        return async.series([
            function(next) {
                return userService.remind($scope.remind, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {

                $scope.clear();

                return $state.go('remindcontinue', {
                    email: email
                });
            }
        });
    };
}]);
