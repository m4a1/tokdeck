tokdeck.

controller('remindcontinueController', ['$rootScope', '$scope', 'domService', 'userService', '$state', '$stateParams', function($rootScope, $scope, domService, userService, $state, $stateParams) {

    if (!$stateParams.remindHash && !$stateParams.email) {
        return $state.go('remind');
    }

    $scope.clear = function() {

        $scope.password = {
            remindHash: $stateParams.remindHash,
            email: $stateParams.email,
            pin: '',
            password_new: '',
            password_repeat: ''
        };

        if ($scope.form_password) {
            $scope.form_password.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                var data = {
                    email: $scope.password.email,
                    remindHash: $scope.password.remindHash,
                    pin: $scope.password.pin,
                    password: $scope.password.password_new
                };

                return userService.remindcontinue(data, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {

                $scope.clear();

                return $state.go('signin');
            }
        });
    };
}]);
