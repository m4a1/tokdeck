tokdeck.

controller('tokaddController', ['$rootScope', '$scope', 'tokService', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, tokService, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.auction = {
        title: ''
    };

    $scope.tok = {
        oid: ''
    };

    $rootScope.loading({
        element: '#form_tok'
    });

    auctionService.get({
        auctionId: auctionId
    }, function(err, auction) {

        $rootScope.loading({
            show: false,
            element: '#form_tok'
        });

        if (err) {
            return domService.processError(err);
        }

        _.extend($scope.auction, auction);
    });

    $scope.submit = function() {

        var d = _.extend({
            auctionId: $scope.auction.id
        }, $scope.tok);

        $rootScope.loading({
            element: '#form_tok'
        });

        return async.series([
            function(next) {
                return tokService.add(d, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_tok'
            });

            if (!error) {

                $scope.form_tok.$setPristine();

                return $state.go('auction.tok.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
