tokdeck.

controller('tokeditController', ['$rootScope', '$scope', 'tokService', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, tokService, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var tokId = $stateParams.tokId;

    $scope.auction = {
        title: ''
    };

    $scope.tok = {
        oidOld: '',
        oid: ''
    };

    $rootScope.loading({
        element: '#form_tok'
    });

    async.series([
        function(next) {

            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return tokService.get({
                auctionId: auctionId,
                tokId: tokId
            }, function(err, tok) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.tok, tok);

                $scope.tok.oidOld = $scope.tok.oid;
                $scope.tok.active = $scope.tok.active ? '1' : '0';

                return next();
            });
        }
    ], function(err) {

        $rootScope.loading({
            show: false,
            element: '#form_tok'
        });

        if (err) {
            return domService.processError(err);
        }
    });

    $scope.submit = function() {

        var d = _.extend({
            auctionId: $scope.auction.id,
            tokId: $scope.tok.id
        }, $scope.tok);

        delete d.titleOld;
        delete d.id;

        $rootScope.loading({
            element: '#form_tok'
        });

        return async.series([
            function(next) {
                return tokService.update(d, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_tok'
            });

            if (!error) {

                $scope.form_tok.$setPristine();

                return $state.go('auction.tok.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
