tokdeck.

controller('toklistController', ['$rootScope', '$scope', 'tokService', 'auctionService', 'domService', '$stateParams', function($rootScope, $scope, tokService, auctionService, domService, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.auction = {
        title: ''
    };

    $scope.list = {
        inited: false,
        data: []
    };

    $rootScope.loading({
        element: '#form_tok'
    });

    async.series([
        function(next) {

            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return tokService.load({
                auctionId: auctionId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                $scope.list.data = list;

                return next();
            });
        }
    ], function(err) {

        $scope.list.inited = true;

        $rootScope.loading({
            show: false,
            element: '#form_tok'
        });
        
        if (err) {
            return domService.processError(err);
        }
    });
}]);
