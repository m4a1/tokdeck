tokdeck.

controller('tokactiveController', ['$rootScope', '$scope', 'auctionService', 'tokService', 'domService', '$state', '$stateParams', function($rootScope, $scope, auctionService, tokService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var tokId = $stateParams.tokId;
    var active = Number($stateParams.active) ? 1 : 0;

    async.series([
        function(next) {

            return tokService.update({
                auctionId: auctionId,
                tokId: tokId,
                active: active,
                action: 'active'
            }, next);
        }
    ], function(error) {

        domService.processError(error);

        return $state.go('auction.tok.index', {
            auctionId: auctionId
        });
    });
}]);
