tokdeck.

controller('tokdeleteController', ['$rootScope', '$scope', 'tokService', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, tokService, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var tokId = $stateParams.tokId;

    $scope.auction = {
        title: ''
    };

    $scope.tok = {
        oidOld: '',
        oid: ''
    };

    $rootScope.loading({
        element: '#form_tok'
    });

    async.series([
        function(next) {

            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return tokService.get({
                auctionId: auctionId,
                tokId: tokId
            }, function(err, tok) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.tok, tok);

                $scope.tok.oidOld = $scope.tok.oid;

                return next();
            });
        }
    ], function(err) {

        $rootScope.loading({
            show: false,
            element: '#form_tok'
        });

        if (err) {
            return domService.processError(err);
        }
    });

    $scope.submit = function() {

        $rootScope.loading({
            element: '#form_tok'
        });

        return async.series([
            function(next) {

                return tokService.remove({
                    auctionId: $scope.auction.id,
                    tokId: $scope.tok.id
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_tok'
            });

            if (!error) {
                return $state.go('auction.tok.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
