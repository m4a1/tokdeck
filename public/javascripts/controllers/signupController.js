tokdeck.

controller('signupController', ['$rootScope', '$scope', 'userService', '$cookies', 'domService', '$state', function($rootScope, $scope, userService, $cookies, domService, $state) {

    $scope.clear = function() {

        $scope.signup = {
            username: '',
            name: '',
            email: '',
            password: '',
            password_repeat: ''
        };

        if ($scope.form_signup) {
            return $scope.form_signup.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                var data = _.extend({}, $scope.signup);

                delete data.password_repeat;

                return userService.signup(data, function(error, user) {

                    if (error) {
                        return next(error);
                    }

                    $rootScope.token = user.token;

                    $cookies.put(user.cookie.name, user.token, user.cookie);

                    return next();
                });
            },
            function(next) {

                return $rootScope.initBootstrap({
                    reinit: true
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {

                $scope.clear();

                return $state.go('home');
            }
        });
    };
}]);
