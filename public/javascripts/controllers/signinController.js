tokdeck.

controller('signinController', ['$rootScope', '$scope', 'domService', 'userService', '$state', '$cookies', '$location', function($rootScope, $scope, domService, userService, $state, $cookies, $location) {

    $scope.clear = function() {

        $scope.signin = {
            username: '',
            password: ''
        };

        if ($scope.form_signin) {
            return $scope.form_signin.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                var data = _.extend({}, $scope.signin);

                return userService.signin(data, function(error, user) {

                    if (error) {
                        return next(error);
                    }

                    $rootScope.token = user.token;

                    $cookies.put(user.cookie.name, user.token, user.cookie);

                    return next();
                });
            },
            function(next) {

                return $rootScope.initBootstrap({
                    reinit: true
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {

                $scope.clear();

                return $state.go('home');
            }
        });
    };
}]);
