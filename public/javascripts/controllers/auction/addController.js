tokdeck.

controller('auctionaddController', ['$rootScope', '$scope', 'auctionService', 'domService', '$state', function($rootScope, $scope, auctionService, domService, $state) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    $scope.auction = {
        title: ''
    };

    $scope.submit = function() {

        $rootScope.loading({
            element: '#form_auction'
        });

        return async.series([
            function(next) {
                return auctionService.add($scope.auction, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_auction'
            });

            if (!error) {

                $scope.form_auction.$setPristine();

                return $state.go('auction.index');
            }
        });
    };
}]);
