tokdeck.

controller('auctiondeleteController', ['$rootScope', '$scope', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.auction = {
        title: ''
    };

    $rootScope.loading({
        element: '#form_auction'
    });

    auctionService.get({
        auctionId: auctionId
    }, function(err, auction) {

        $rootScope.loading({
            show: false,
            element: '#form_auction'
        });

        if (err) {
            return domService.processError(err);
        }

        _.extend($scope.auction, auction);
    });

    $scope.submit = function() {

        $rootScope.loading({
            element: '#form_auction'
        });

        return async.series([
            function(next) {

                return auctionService.remove({
                    auctionId: $scope.auction.id
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_auction'
            });

            if (!error) {
                return $state.go('auction.index');
            }
        });
    };
}]);
