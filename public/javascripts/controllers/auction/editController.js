tokdeck.

controller('auctioneditController', ['$rootScope', '$scope', 'auctionService', 'domService', '$state', '$stateParams', 'configService', function($rootScope, $scope, auctionService, domService, $state, $stateParams, configService) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.fields = configService.auction.def;

    $scope.auction = {
        titleOld: '',
        title: '',
        active: false
    };

    for (var k in $scope.fields) {
        $scope.auction[k] = '';
    }

    $rootScope.loading({
        element: '#form_auction'
    });

    auctionService.get({
        auctionId: auctionId
    }, function(err, auction) {

        $rootScope.loading({
            show: false,
            element: '#form_auction'
        });

        if (err) {
            return domService.processError(err);
        }

        _.extend($scope.auction, auction);

        $scope.auction.titleOld = $scope.auction.title;
        $scope.auction.active = $scope.auction.active ? '1' : '0';
    });

    $scope.submit = function() {

        var d = _.extend({
            auctionId: $scope.auction.id
        }, $scope.auction);

        delete d.titleOld;
        delete d.id;

        $rootScope.loading({
            element: '#form_auction'
        });

        return async.series([
            function(next) {
                return auctionService.update(d, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_auction'
            });

            if (!error) {

                $scope.form_auction.$setPristine();

                return $state.go('auction.index');
            }
        });
    };
}]);
