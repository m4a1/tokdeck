tokdeck.

controller('auctionstatusController', ['$rootScope', '$scope', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var status = $stateParams.status;

    async.series([
        function(next) {

            return auctionService.update({
                auctionId: auctionId,
                status: status,
                action: 'status'
            }, next);
        }
    ], function(error) {

        domService.processError(error);

        return $state.go('auction.index');
    });
}]);
