tokdeck.

controller('auctionactiveController', ['$rootScope', '$scope', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var active = Number($stateParams.active) ? 1 : 0;

    async.series([
        function(next) {

            return auctionService.update({
                auctionId: auctionId,
                active: !!active,
                action: 'active'
            }, next);
        }
    ], function(error) {

        domService.processError(error);

        return $state.go('auction.index');
    });
}]);
