tokdeck.

controller('auctionlistController', ['$rootScope', '$scope', 'auctionService', 'domService', function($rootScope, $scope, auctionService, domService) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    $scope.list = {
        inited: false,
        data: []
    };

    auctionService.load(function(err, list) {

        $scope.list.inited = true;

        if (err) {
            return domService.processError(err);
        }
        
        $scope.list.data = list;
    });
}]);
