tokdeck.

controller('productdeleteController', ['$rootScope', '$scope', 'productService', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, productService, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var productId = $stateParams.productId;

    $scope.auction = {
        title: ''
    };

    $scope.product = {
        oidOld: '',
        oid: ''
    };

    $rootScope.loading({
        element: '#form_product'
    });

    async.series([
        function(next) {

            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return productService.get({
                auctionId: auctionId,
                productId: productId
            }, function(err, product) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.product, product);

                $scope.product.oidOld = $scope.product.oid;

                return next();
            });
        }
    ], function(err) {

        $rootScope.loading({
            show: false,
            element: '#form_product'
        });

        if (err) {
            return domService.processError(err);
        }
    });

    $scope.submit = function() {

        $rootScope.loading({
            element: '#form_product'
        });

        return async.series([
            function(next) {

                return productService.remove({
                    auctionId: $scope.auction.id,
                    productId: $scope.product.id
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_product'
            });

            if (!error) {
                return $state.go('auction.product.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
