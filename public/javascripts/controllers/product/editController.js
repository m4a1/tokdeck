tokdeck.

controller('producteditController', ['$rootScope', '$scope', 'productService', 'auctionService', 'domService', '$state', '$stateParams', 'configService', function($rootScope, $scope, productService, auctionService, domService, $state, $stateParams, configService) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var productId = $stateParams.productId;

    $scope.fields = configService.auction.def;

    delete $scope.fields.lotMax;

    $scope.auction = {
        title: ''
    };

    $scope.product = {
        oidOld: '',
        oid: '',
        price: 0.00
    };

    for (var k in $scope.fields) {
        $scope.product[k] = '';
    }

    $rootScope.loading({
        element: '#form_product'
    });

    async.series([
        function(next) {
            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return productService.get({
                auctionId: auctionId,
                productId: productId
            }, function(err, product) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.product, product);

                $scope.product.oidOld = $scope.product.oid;
                $scope.product.active = $scope.product.active ? '1' : '0';

                return next();
            });
        }
    ], function(err) {

        $rootScope.loading({
            show: false,
            element: '#form_product'
        });

        if (err) {
            return domService.processError(err);
        }
    });

    $scope.submit = function() {

        var d = _.extend({
            auctionId: $scope.auction.id,
            productId: $scope.product.id
        }, $scope.product);

        delete d.titleOld;
        delete d.id;

        $rootScope.loading({
            element: '#form_product'
        });

        return async.series([
            function(next) {
                return productService.update(d, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_product'
            });

            if (!error) {

                $scope.form_product.$setPristine();

                return $state.go('auction.product.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
