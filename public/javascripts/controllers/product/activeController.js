tokdeck.

controller('productactiveController', ['$rootScope', '$scope', 'auctionService', 'productService', 'domService', '$state', '$stateParams', function($rootScope, $scope, auctionService, productService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;
    var productId = $stateParams.productId;
    var active = Number($stateParams.active) ? 1 : 0;

    async.series([
        function(next) {

            return productService.update({
                auctionId: auctionId,
                productId: productId,
                active: active,
                action: 'active'
            }, next);
        }
    ], function(error) {

        domService.processError(error);

        return $state.go('auction.product.index', {
            auctionId: auctionId
        });
    });
}]);
