tokdeck.

controller('productaddController', ['$rootScope', '$scope', 'productService', 'auctionService', 'domService', '$state', '$stateParams', function($rootScope, $scope, productService, auctionService, domService, $state, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.auction = {
        title: ''
    };

    $scope.product = {
        oid: '',
        price: 0.00
    };

    $rootScope.loading({
        element: '#form_product'
    });

    auctionService.get({
        auctionId: auctionId
    }, function(err, auction) {

        $rootScope.loading({
            show: false,
            element: '#form_product'
        });

        if (err) {
            return domService.processError(err);
        }

        _.extend($scope.auction, auction);
    });

    $scope.submit = function() {

        var d = _.extend({
            auctionId: $scope.auction.id
        }, $scope.product);

        $rootScope.loading({
            element: '#form_product'
        });

        return async.series([
            function(next) {
                return productService.add(d, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading({
                show: false,
                element: '#form_product'
            });

            if (!error) {

                $scope.form_product.$setPristine();

                return $state.go('auction.product.index', {
                    auctionId: $scope.auction.id
                });
            }
        });
    };
}]);
