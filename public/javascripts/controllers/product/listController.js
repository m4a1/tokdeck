tokdeck.

controller('productlistController', ['$rootScope', '$scope', 'productService', 'auctionService', 'domService', '$stateParams', function($rootScope, $scope, productService, auctionService, domService, $stateParams) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    var auctionId = $stateParams.auctionId;

    $scope.auction = {
        title: ''
    };

    $scope.list = {
        inited: false,
        data: []
    };

    $rootScope.loading({
        element: '#form_product'
    });

    async.series([
        function(next) {

            return auctionService.get({
                auctionId: auctionId
            }, function(err, auction) {

                if (err) {
                    return next(err);
                }

                _.extend($scope.auction, auction);

                return next();
            });
        },
        function(next) {

            return productService.load({
                auctionId: auctionId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                $scope.list.data = list;

                return next();
            });
        }
    ], function(err) {

        $scope.list.inited = true;

        $rootScope.loading({
            show: false,
            element: '#form_product'
        });
        
        if (err) {
            return domService.processError(err);
        }
    });
}]);
