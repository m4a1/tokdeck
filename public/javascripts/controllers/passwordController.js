tokdeck.

controller('passwordController', ['$rootScope', '$scope', 'domService', 'userService', function($rootScope, $scope, domService, userService) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    $scope.clear = function() {

        $scope.password = {
            password: '',
            password_new: '',
            password_repeat: ''
        };

        if ($scope.form_password) {
            return $scope.form_password.$setPristine();
        }
    };

    $scope.clear();

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                var data = {
                    password: $scope.password.password_new
                };

                if ($rootScope.user.hasPassword) {
                    data.passwordCurrent = $scope.password.password;
                }

                return userService.update(data, next);
            },
            function(next) {

                return $rootScope.initCoreData({
                    reinit: true
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {
                return $scope.clear();
            }
        });
    };
}]);
