tokdeck.

controller('profileController', ['$rootScope', '$scope', 'langService', 'userService', 'domService', function($rootScope, $scope, langService, userService, domService) {

    if (!$rootScope.checkLogin()) {
        return;
    }

    $scope.profile = _.extend({}, $rootScope.user);

    langService.list(function(error, result) {
        $scope.langs = result;
    });

    $scope.submit = function() {

        $rootScope.loading(true);

        return async.series([
            function(next) {

                return userService.update($scope.profile, next);
            },
            function(next) {

                return $rootScope.initCoreData({
                    reinit: true
                }, next);
            }
        ], function(error) {

            domService.processError(error);

            $rootScope.loading(false);

            if (!error) {
                return $scope.form_profile.$setPristine();
            }
        });
    };
}]);
