var tokdeck = angular.module('tokdeck', [
    'ui.router',
    'ngResource',
    'ngCookies',
    'ngMessages',
    'template',
    'validation.match',
    'angularMoment'
]).config(function ($stateProvider) {

    tokdeck.env = '%env%';

    $stateProvider.state('home', {
        url: '/dashboard',
        controller: 'homeController',
        templateUrl: 'public/views/home.html'
    });

    $stateProvider.state('remind', {
        url: '/dashboard/remind',
        controller: 'remindController',
        templateUrl: 'public/views/remind/remind.html'
    });

    $stateProvider.state('remindcontinue', {
        url: '/dashboard/remind/continue?remindHash&email',
        controller: 'remindcontinueController',
        templateUrl: 'public/views/remind/continue.html'
    });

    $stateProvider.state('profile', {
        url: '/dashboard/profile',
        templateUrl: 'public/views/profile.html'
    });

    $stateProvider.state('onboarding', {
        url: '/dashboard/onboarding',
        controller: 'onboardingController',
        templateUrl: 'public/views/onboarding.html'
    });

    $stateProvider.state('auction', {
        abstract: true,
        url: '/dashboard/auction',
        templateUrl: 'public/views/auction/index.html'
    });

    $stateProvider.state('auction.add', {
        url: '/add',
        controller: 'auctionaddController',
        templateUrl: 'public/views/auction/add.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.index', {
        url: '',
        controller: 'auctionlistController',
        templateUrl: 'public/views/auction/list.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.edit', {
        url: '/edit/:auctionId',
        controller: 'auctioneditController',
        templateUrl: 'public/views/auction/edit.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.status', {
        url: '/status/:auctionId/:status',
        controller: 'auctionstatusController',
        parent: 'auction'
    });

    $stateProvider.state('auction.active', {
        url: '/active/:auctionId/:active',
        controller: 'auctionactiveController',
        parent: 'auction'
    });

    $stateProvider.state('auction.delete', {
        url: '/delete/:auctionId',
        controller: 'auctiondeleteController',
        templateUrl: 'public/views/auction/delete.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.product', {
        abstract: true,
        url: '/product/:auctionId',
        controller: 'productController',
        templateUrl: 'public/views/product/index.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.product.index', {
        url: '',
        controller: 'productlistController',
        templateUrl: 'public/views/product/list.html',
        parent: 'auction.product'
    });

    $stateProvider.state('auction.product.add', {
        url: '/add',
        controller: 'productaddController',
        templateUrl: 'public/views/product/add.html',
        parent: 'auction.product'
    });

    $stateProvider.state('auction.product.edit', {
        url: '/edit/:productId',
        controller: 'producteditController',
        templateUrl: 'public/views/product/edit.html',
        parent: 'auction.product'
    });

    $stateProvider.state('auction.product.active', {
        url: '/active/:productId/:active',
        controller: 'productactiveController',
        parent: 'auction.product'
    });

    $stateProvider.state('auction.product.delete', {
        url: '/delete/:productId',
        controller: 'productdeleteController',
        templateUrl: 'public/views/product/delete.html',
        parent: 'auction.product'
    });

    $stateProvider.state('auction.tok', {
        abstract: true,
        url: '/tok/:auctionId',
        controller: 'tokController',
        templateUrl: 'public/views/tok/index.html',
        parent: 'auction'
    });

    $stateProvider.state('auction.tok.index', {
        url: '',
        controller: 'toklistController',
        templateUrl: 'public/views/tok/list.html',
        parent: 'auction.tok'
    });

    $stateProvider.state('auction.tok.add', {
        url: '/add',
        controller: 'tokaddController',
        templateUrl: 'public/views/tok/add.html',
        parent: 'auction.tok'
    });

    $stateProvider.state('auction.tok.edit', {
        url: '/edit/:tokId',
        controller: 'tokeditController',
        templateUrl: 'public/views/tok/edit.html',
        parent: 'auction.tok'
    });

    $stateProvider.state('auction.tok.active', {
        url: '/active/:tokId/:active',
        controller: 'tokactiveController',
        parent: 'auction.tok'
    });

    $stateProvider.state('auction.tok.delete', {
        url: '/delete/:tokId',
        controller: 'tokdeleteController',
        templateUrl: 'public/views/tok/delete.html',
        parent: 'auction.tok'
    });

    $stateProvider.state('signout', {
        url: '/dashboard/signout',
        controller: function(userService) {
            userService.signout();
            window.location = '/';
        }
    });

    $stateProvider.state('signin', {
        url: '/dashboard/signin',
        controller: 'signinController',
        templateUrl: 'public/views/signin.html'
    });

    $stateProvider.state('signup', {
        url: '/dashboard/signup',
        controller: 'signupController',
        templateUrl: 'public/views/signup.html'
    });
}).constant('angularMomentConfig', {
    timezone: 'Europe/London'
}).config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
}).run(function (amMoment, $window, $cookies, $rootScope, userService, localeService, configService, domService, socketService, domService, startService, $location, $state) {

    $rootScope.token = $cookies.get('token');

    if (!sessionStorage.uuid) {
        sessionStorage.uuid = uuid.v4();
    }

    $rootScope.continueState = {
        state: 'home',
        param: $location.search()
    };

    $rootScope.onboarding = false;

    $rootScope.$on('$stateChangeStart', function(evt, toState, param) {

        if (!$rootScope.loaded || $rootScope.offline) {

            evt.preventDefault();

            if (toState.name !== 'onboarding') {

                $rootScope.continueState.state = toState.name;

                if (param) {
                    _.extend($rootScope.continueState.param, param);
                }
            }
        } else if ($rootScope.onboarding && toState.name !== 'onboarding' && toState.name !== 'signout') {
            evt.preventDefault();
        }

        $rootScope.state = toState.name;
    });

    amMoment.changeLocale('en');

    $rootScope.initLoading = function(val) {
        $rootScope.loaded = !val;
    };

    $rootScope.loading = function(param) {

        if (typeof param !== 'object') {
            param = {
                show: false
            };
        } else if (typeof param.show === 'undefined') {
            param.show = true;
        }

        if (param.show) {

            blockUI(param.element ? param.element : 'body');

            //$(document.body).removeClass('pace-done');

            //$('.pace').removeClass('pace-inactive');

            //Pace.start();
        } else {

            unblockUI(param.element ? param.element : 'body');

            //Pace.stop();
        }
    };
    $rootScope.checkLogin = function() {

        if (!$rootScope.user) {

            $state.go('signin');

            return false;
        }

        return true;
    };

    window.r = $rootScope;

    $rootScope.localeService = localeService;
    $rootScope.configService = configService;
    $rootScope.domService = domService;
    $rootScope.offline = false;

    domService.init();

    socketService.init();

    $rootScope.initCoreData = function(param, callback) {

        var reinit = param.reinit;
        var errorUser = null;

        return async.series([
            function(next) {

                return userService.init({
                    reinit: reinit
                }, function(error) {

                    errorUser = error;

                    return next();
                });
            },
            function(next) {

                return localeService.init({
                    langId: $rootScope.user ? $rootScope.user.langId : 0
                }, next);
            },
            function(next) {

                $rootScope.dashboardMenu = [
                    {
                        state: 'home',
                        title: localeService.data.page_home_title
                    },
                    {
                        state: 'auction.index',
                        title: localeService.data.page_auction_title
                    }
                ];

                return next();
            }
        ], function(error) {

            $rootScope.initLoading(false);

            if (errorUser) {
                domService.processError(errorUser);
            }

            if (callback) {
                return callback(error);
            }
        });
    };
    $rootScope.initBootstrap = function(param, callback) {

        return async.series([
            function(next) {
                return $rootScope.initCoreData(param, next);
            },
            function(next) {

                if (!$rootScope.user) {
                    return next();
                }

                socketService.setUser($rootScope.user);

                return next();
            },
            function(next) {

                //if (!$rootScope.user) {
                //    return next();
                //}

                return startService.init(param, next);
            }
        ], function(error) {

            domService.processError(error);

            if (callback) {
                return callback(error);
            }
        });
    };

    $rootScope.initBootstrap({}, function() {
        return angular.element($window).triggerHandler('resize');
    });
}).filter('to_trusted', ['$sce', function($sce){

    return function(text) {

        return $sce.trustAsHtml(text);
    };
}]);