'use strict';

var config = require('../../config');
var async = require('async');
var integrationStrategyService = require('../../services/integrations');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.integration'
});

var IntegrationApi = {};

IntegrationApi.auth = function(req, res) {

    logger.info('IntegrationApi', 'auth', 'start');

    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var provider = params.provider;
    var redirectUrl = null;
    var integrationStrategy = null;

    return async.series([
        function(next) {

            var IntegrationStrategy = integrationStrategyService.use(provider);

            if (!IntegrationStrategy) {
                return next({code: 0, message: 'No IntegrationStrategy found', provider: provider});
            }

            integrationStrategy = new IntegrationStrategy();

            return next();
        },
        function(next) {

            params.req = req;
            params.res = res;

            return integrationStrategy.auth(params, function(err, result) {

                if (err) {
                    return next(err);
                }

                redirectUrl = result.redirectUrl;

                return next();
            });
        }
    ], function(err) {

        logger.info('IntegrationApi', 'auth', 'finish');

        if (err) {
            logger.error('IntegrationApi', 'auth', err);
        }

        return res.redirect(redirectUrl);
    });
};

module.exports = IntegrationApi;