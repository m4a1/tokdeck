'use strict';

var config = require('../../config');
var async = require('async');
var _ = require('lodash');
var AuctionService = require('../../services/auction');
var MessageService = require('../../services/message');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.auction'
});

var AuctionApi = {};

AuctionApi.list = function(req, res) {

    logger.info('AuctionApi', 'list', 'start');

    var auctionService = new AuctionService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return auctionService.getAuctions({
                userId: userId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                result = list;

                return next();
            });
        }
    ], function(err) {

        logger.info('AuctionApi', 'list', 'finish');

        if (err) {
            logger.error('AuctionApi', 'list', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

AuctionApi.show = function(req, res) {

    logger.info('AuctionApi', 'show', 'start');

    var auctionService = new AuctionService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var auctionId = params.auctionId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return auctionService.getAuction({
                userId: userId,
                auctionId: auctionId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        }
    ], function(err) {

        logger.info('AuctionApi', 'show', 'finish');

        if (err) {
            logger.error('AuctionApi', 'show', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

AuctionApi.add = function(req, res) {

    logger.info('AuctionApi', 'add', 'start');

    var auctionService = new AuctionService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return auctionService.addAuction(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_auction_created'
            });

            return next();
        }
    ], function(err) {

        logger.info('AuctionApi', 'add', 'finish');

        if (err) {
            logger.error('AuctionApi', 'add', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

AuctionApi.update = function(req, res) {

    logger.info('AuctionApi', 'update', 'start');

    var auctionService = new AuctionService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var action = params.action;
    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return auctionService.updateAuction(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: action === 'status'
                    ? 'msg_auction_status_changed'
                    : (action === 'active'
                        ? 'msg_auction_active_changed'
                        : 'msg_auction_updated'
                    )
            });

            return next();
        }
    ], function(err) {

        logger.info('AuctionApi', 'update', 'finish');

        if (err) {
            logger.error('AuctionApi', 'update', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

AuctionApi.remove = function(req, res) {

    logger.info('AuctionApi', 'remove', 'start');

    var auctionService = new AuctionService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var auctionId = params.auctionId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return auctionService.removeAuction({
                userId: userId,
                auctionId: auctionId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_auction_removed'
            });

            return next();
        }
    ], function(err) {

        logger.info('AuctionApi', 'remove', 'finish');

        if (err) {
            logger.error('AuctionApi', 'remove', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = AuctionApi;