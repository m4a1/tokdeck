'use strict';

var config = require('../../config');
var async = require('async');
var LangService = require('../../services/lang');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.lang'
});

var LangApi = {};

LangApi.list = function(req, res) {

    logger.info('LangApi', 'list', 'start');

    var langService = new LangService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var result = null;

    return async.series([
        function(next) {

            return langService.getLangs({}, function(err, list) {

                if (err) {
                    return next(err);
                }

                if (list) {
                    result = list;
                }

                return next();
            });
        }
    ], function(err) {

        logger.info('LangApi', 'list', 'finish');

        if (err) {
            logger.error('LangApi', 'list', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = LangApi;