'use strict';

var config = require('../../config');
var async = require('async');
var _ = require('lodash');
var ProductService = require('../../services/product');
var MessageService = require('../../services/message');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.product'
});

var ProductApi = {};

ProductApi.list = function(req, res) {

    logger.info('ProductApi', 'list', 'start');

    var productService = new ProductService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var auctionId = params.auctionId;
    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return productService.getProducts({
                userId: userId,
                auctionId: auctionId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                result = list;

                return next();
            });
        }
    ], function(err) {

        logger.info('ProductApi', 'list', 'finish');

        if (err) {
            logger.error('ProductApi', 'list', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

ProductApi.show = function(req, res) {

    logger.info('ProductApi', 'show', 'start');

    var productService = new ProductService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var productId = params.productId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return productService.getProduct({
                userId: userId,
                productId: productId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        }
    ], function(err) {

        logger.info('ProductApi', 'show', 'finish');

        if (err) {
            logger.error('ProductApi', 'show', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

ProductApi.add = function(req, res) {

    logger.info('ProductApi', 'add', 'start');

    var productService = new ProductService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return productService.addProduct(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_product_created'
            });

            return next();
        }
    ], function(err) {

        logger.info('ProductApi', 'add', 'finish');

        if (err) {
            logger.error('ProductApi', 'add', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

ProductApi.update = function(req, res) {

    logger.info('ProductApi', 'update', 'start');

    var productService = new ProductService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var action = params.action;
    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return productService.updateProduct(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: action === 'active'
                    ? 'msg_tok_active_changed'
                    : 'msg_tok_updated'
            });

            return next();
        }
    ], function(err) {

        logger.info('ProductApi', 'update', 'finish');

        if (err) {
            logger.error('ProductApi', 'update', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

ProductApi.remove = function(req, res) {

    logger.info('ProductApi', 'remove', 'start');

    var productService = new ProductService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var productId = params.productId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return productService.removeProduct({
                userId: userId,
                productId: productId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_product_removed'
            });

            return next();
        }
    ], function(err) {

        logger.info('ProductApi', 'remove', 'finish', params);

        if (err) {
            logger.error('ProductApi', 'remove', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = ProductApi;