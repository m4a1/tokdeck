'use strict';

var config = require('../../config');
var async = require('async');
var moment = require('moment');
var UserService = require('../../services/user');
var UtilService = require('../../services/util');
var AuthService = require('../../services/auth');
var AccountService = require('../../services/account');
var MessageService = require('../../services/message');
var accountStrategyService = require('../../services/accounts');
var logger = require('../../lib/logger')({
    prefix: 'api.user'
});

var UserApi = {};

UserApi.add = function(req, res) {

    logger.info('UserApi', 'add', 'start');

    var userService = new UserService();
    var utilService = new UtilService();
    var authService = new AuthService();
    var accountService = new AccountService();
    var params = utilService.formatParams(req.swagger.params);

    var username = params.username;
    var name = params.name;
    var email = params.email;
    var password = params.password;
    var user = null;
    var result = null;
    var token = null;

    return async.series([
        function(next) {

            return accountService.setupAccount({
                provider: 'local',
                profile: {
                    username: username,
                    name: name,
                    email: email,
                    password: password
                }
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function (next) {

            if (!user) {
                return next();
            }

            return authService.oauthCreateToken({
                scope: 'all',
                redirect_uri: true,
                state: config.oauth.state,
                realm: config.oauth.realm,
                userId: user.id,
                accessTokenExpire: config.oauth.accessTokenExpire
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                token = result.token;

                return next();
            });
        },
        function(next) {

            if (!user) {
                return next({code: 0, message: 'User not found'});
            }

            var expires = new moment();

            return userService.formatUser(user, function(error, _result) {

                if (error) {
                    return next(error);
                }

                result = _result;

                result.token = token;

                result.cookie = {
                    name: 'token',
                    domain: config.user.cookieDomain,
                    expires: expires.add(config.user.cookieExpire / 1000, 's').toDate().toUTCString()
                };

                return next();
            });
        }
    ], function(err) {

        logger.info('UserApi', 'add', 'finish');

        if (err) {
            logger.error('UserApi', 'add', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

UserApi.signin = function(req, res) {

    var userService = new UserService();
    var utilService = new UtilService();
    var authService = new AuthService();
    var accountService = new AccountService();
    var messageService = new MessageService();
    var params = utilService.formatParams(req.swagger.params);

    logger.info('UserApi', 'signin', 'start');

    var provider = params.provider;
    var tid = req.query.tid;

    var accountStrategy = null;
    var user = null;
    var profile = null;
    var result = null;
    var token = null;

    return async.series([
        function(next) {

            var AccountStrategy = accountStrategyService.use(provider);

            if (!AccountStrategy) {
                return next({code: 0, message: 'No AccountStrategy found', provider: provider});
            }

            accountStrategy = new AccountStrategy();

            return next();
        },
        function(next) {

            params.req = req;
            params.res = res;

            return accountStrategy.signin(params, function(err, _profile) {

                if (err) {
                    return next(err);
                }

                profile = _profile;

                return next();
            });
        },
        function(next) {

            if (!profile) {
                return next({code: 0, message: 'Authorization Failed'});
            }

            return accountService.setupAccount({
                provider: provider,
                profile: profile
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function (next) {

            return authService.oauthCreateToken({
                scope: 'all',
                redirect_uri: true,
                state: config.oauth.state,
                realm: config.oauth.realm,
                userId: user.id,
                accessTokenExpire: config.oauth.accessTokenExpire
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                token = _result.token;

                return next();
            });
        },
        function(next) {

            var expires = new moment();

            return userService.formatUser(user, function(error, _result) {

                if (error) {
                    return next(error);
                }

                result = _result;

                result.token = token;

                result.cookie = {
                    name: 'token',
                    domain: config.user.cookieDomain,
                    expires: expires.add(config.user.cookieExpire / 1000, 's').toDate().toUTCString()
                };

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: user.id,
                tid: tid,
                message: 'msg_user_signed_in'
            });

            return next();
        }
    ], function(err) {

        logger.info('UserApi', 'signin', 'finish');

        if (err) {
            logger.error('UserApi', 'signin', err);
        }

        if (req.callback) {
            return req.callback(err, result);
        }

        if (provider === 'local') {

            return res.send({
                error: err,
                result: result
            });
        }

        return res.redirect(config.siteUrl + '/dashboard/signin?auth=' + encodeURIComponent((new Buffer(JSON.stringify(result))).toString('base64')));
    });
};

UserApi.remind = function(req, res) {

    var userService = new UserService();
    var utilService = new UtilService();
    var messageService = new MessageService();
    var params = utilService.formatParams(req.swagger.params);

    logger.info('UserApi', 'remind', 'start');

    var email = params.email;
    var tid = req.query.tid;
    var result = null;


    return async.series([
        function(next) {

            return userService.remindUser({
                email: email
            }, function(err) {

                if (err) {
                    return next(err);
                }

                result = true;

                return next();
            });
        },
        function(next) {

            if (!tid) {
                return next();
            }

            messageService.addMessage({
                tid: tid,
                message: 'msg_user_reminded'
            });

            return next();
        }
    ], function(err) {

        logger.info('UserApi', 'remind', 'finish');

        if (err) {
            logger.error('UserApi', 'remind', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

UserApi.remindcontinue = function(req, res) {

    var userService = new UserService();
    var utilService = new UtilService();
    var messageService = new MessageService();
    var params = utilService.formatParams(req.swagger.params);

    logger.info('UserApi', 'remindcontinue', 'start');

    var email = params.email;
    var pin = params.pin;
    var password = params.password;
    var remindHash = params.remindHash;
    var tid = req.query.tid;
    var result = null;

    return async.series([
        function(next) {

            return userService.remindcontinueUser({
                email: email,
                pin: pin,
                remindHash: remindHash,
                password: password
            }, function(err) {

                if (err) {
                    return next(err);
                }

                result = true;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                tid: tid,
                message: 'msg_user_reminded_finish'
            });

            return next();
        }
    ], function(err) {

        logger.info('UserApi', 'remindcontinue', 'finish');

        if (err) {
            logger.error('UserApi', 'remindcontinue', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

UserApi.show = function(req, res) {

    var userService = new UserService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    logger.info('UserApi', 'show', 'start');

    var userId = null;
    var user = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return userService.getUser({
                id: userId
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            if (!user) {
                return next({code: 0, message: 'User not found'});
            }

            return userService.formatUser(user, function(error, _result) {

                if (error) {
                    return next(error);
                }

                result = _result;

                return next();
            });
        }
    ], function(err) {

        logger.info('UserApi', 'show', 'finish');

        if (err) {
            logger.error('UserApi', 'show', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

UserApi.update = function(req, res) {

    var userService = new UserService();
    var utilService = new UtilService();
    var messageService = new MessageService();

    var params = utilService.formatParams(req.swagger.params);

    logger.info('UserApi', 'update', 'start');

    var silent = params.silent;
    var userId = null;
    var user = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var data = {};

            for (var k in params) {

                if (typeof params[k] !== 'undefined') {
                    data[k] = params[k];
                }
            }

            delete data.userId;

            if (!Object.keys(data).length) {
                return next(true);
            }

            return userService.updateUser({
                userId: userId,
                profile: data
            }, function(err, _user) {

                if (err) {
                    return next(err);
                }

                user = _user;

                return next();
            });
        },
        function(next) {

            return userService.formatUser(user, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            if (silent) {
                return next();
            }

            messageService.addMessage({
                userId: user.id,
                message: 'msg_profile_updated'
            });

            return next();
        }
    ], function(err) {

        logger.info('UserApi', 'update', 'finish');

        if (err) {
            logger.error('UserApi', 'update', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = UserApi;