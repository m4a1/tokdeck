'use strict';

var config = require('../../config');
var async = require('async');
var _ = require('lodash');
var TokService = require('../../services/tok');
var MessageService = require('../../services/message');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.tok'
});

var TokApi = {};

TokApi.list = function(req, res) {

    logger.info('TokApi', 'list', 'start');

    var tokService = new TokService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var auctionId = params.auctionId;
    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return tokService.getToks({
                userId: userId,
                auctionId: auctionId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                result = list;

                return next();
            });
        }
    ], function(err) {

        logger.info('TokApi', 'list', 'finish');

        if (err) {
            logger.error('TokApi', 'list', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

TokApi.show = function(req, res) {

    logger.info('TokApi', 'show', 'start');

    var tokService = new TokService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var tokId = params.tokId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return tokService.getTok({
                userId: userId,
                tokId: tokId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        }
    ], function(err) {

        logger.info('TokApi', 'show', 'finish');

        if (err) {
            logger.error('TokApi', 'show', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

TokApi.add = function(req, res) {

    logger.info('TokApi', 'add', 'start');

    var tokService = new TokService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return tokService.addTok(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_tok_created'
            });

            return next();
        }
    ], function(err) {

        logger.info('TokApi', 'add', 'finish');

        if (err) {
            logger.error('TokApi', 'add', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

TokApi.update = function(req, res) {

    logger.info('TokApi', 'update', 'start');

    var tokService = new TokService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var action = params.action;
    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            var d = _.extend({
                userId: userId
            }, params);

            return tokService.updateTok(d, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: action === 'active'
                    ? 'msg_tok_active_changed'
                    : 'msg_tok_updated'
            });

            return next();
        }
    ], function(err) {

        logger.info('TokApi', 'update', 'finish');

        if (err) {
            logger.error('TokApi', 'update', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

TokApi.remove = function(req, res) {

    logger.info('TokApi', 'remove', 'start');

    var tokService = new TokService();
    var messageService = new MessageService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);
    var tokId = params.tokId;

    var userId = null;
    var result = null;

    return async.series([
        function(next) {

            return utilService.checkUser(req, function(err, _params) {

                if (err) {
                    return next(err);
                }

                userId = _params.userId;

                return next();
            });
        },
        function(next) {

            return tokService.removeTok({
                userId: userId,
                tokId: tokId
            }, function(err, _result) {

                if (err) {
                    return next(err);
                }

                result = _result;

                return next();
            });
        },
        function(next) {

            messageService.addMessage({
                userId: userId,
                message: 'msg_tok_removed'
            });

            return next();
        }
    ], function(err) {

        logger.info('TokApi', 'remove', 'finish');

        if (err) {
            logger.error('TokApi', 'remove', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = TokApi;