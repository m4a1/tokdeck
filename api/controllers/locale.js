'use strict';

var config = require('../../config');
var async = require('async');
var LocaleService = require('../../services/locale');
var UtilService = require('../../services/util');
var logger = require('../../lib/logger')({
    prefix: 'api.locale'
});

var LocaleApi = {};

LocaleApi.show = function(req, res) {

    logger.info('LocaleApi', 'show', 'start');

    var localeService = new LocaleService();
    var utilService = new UtilService();

    var params = utilService.formatParams(req.swagger.params);

    var langId = params.langId;
    var result = null;

    return async.series([
        function(next) {

            return localeService.getLocale({
                langId: langId
            }, function(err, list) {

                if (err) {
                    return next(err);
                }

                if (list) {
                    result = list;
                }

                return next();
            });
        }
    ], function(err) {

        logger.info('LocaleApi', 'show', 'finish');

        if (err) {
            logger.error('LocaleApi', 'show', err);
        }

        return res.send({
            error: err,
            result: result
        });
    });
};

module.exports = LocaleApi;