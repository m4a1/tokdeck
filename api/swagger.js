'use strict';

var config = require('../config');
var urlParser = require('url');

var Swagger = function() {};

Swagger.prototype.getJSON = function() {

    return {
        swagger: '2.0',
        info: {
            version: '1.0.0',
            title: 'Auction API'
        },
        host: urlParser.parse(config.apiUrl).host,
        basePath: '/v1',
        schemes: [
            'http',
            'https'
        ],
        consumes: [
            'application/json',
            'application/x-www-form-urlencoded'
        ],
        produces: [
            'application/json'
        ],
        securityDefinitions: {
            auction_auth: {
                type: 'oauth2',
                authorizationUrl: config.apiUrl + '/v1/oauth',
                flow: 'implicit',
                scopes: {
                    'user:read': 'Read users',
                    'user:write': 'Write users',
                    'auction:read': 'Read auctions',
                    'auction:write': 'Write auctions',
                    'product:read': 'Read products',
                    'product:write': 'Write products',
                    'tok:read': 'Read toks',
                    'tok:write': 'Write toks'
                }
            }
        },
        tags: [
            {
                name: 'locale'
            },
            {
                name: 'lang'
            },
            {
                name: 'user'
            },
            {
                name: 'auction'
            },
            {
                name: 'product'
            },
            {
                name: 'tok'
            },
            {
                name: 'integration'
            }
        ],
        paths: {
            '/user': {
                'x-swagger-router-controller': 'user',
                post: {
                    tags: ['user'],
                    description: 'Creates user account',
                    operationId: 'add',
                    parameters: [
                        {
                            name: 'username',
                            'in': 'formData',
                            description: 'User nickname',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'name',
                            'in': 'formData',
                            description: 'User name',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'email',
                            'in': 'formData',
                            description: 'User email',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'password',
                            'in': 'formData',
                            description: 'User password',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['user:write']
                        }
                    ]
                }
            },
            '/user/signin/{provider}': {
                'x-swagger-router-controller': 'user',
                post: {
                    tags: ['user'],
                    description: 'Sign user in',
                    operationId: 'signin',
                    parameters: [
                        {
                            name: 'provider',
                            'in': 'path',
                            description: 'Provider type',
                            enum: [ 'local' ],
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'username',
                            'in': 'formData',
                            description: 'User nickname',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'password',
                            'in': 'formData',
                            description: 'User password',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'name',
                            'in': 'formData',
                            description: 'User name',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'email',
                            'in': 'formData',
                            description: 'User email',
                            type: 'string',
                            required: false
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                },
                get: {
                    tags: ['user'],
                    description: 'Sign user in',
                    operationId: 'signin',
                    parameters: [
                        {
                            name: 'provider',
                            'in': 'path',
                            description: 'Provider type',
                            enum: [ 'twitter', 'facebook', 'google' ],
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/user/signin/{provider}/{action}': {
                'x-swagger-router-controller': 'user',
                get: {
                    tags: ['user'],
                    description: 'Sign user in',
                    operationId: 'signin',
                    parameters: [
                        {
                            name: 'provider',
                            'in': 'path',
                            description: 'Provider type',
                            enum: [ 'twitter', 'facebook', 'google' ],
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'action',
                            'in': 'path',
                            description: 'Auth action',
                            enum: [ 'callback', 'success', 'failure' ],
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/user/remind': {
                'x-swagger-router-controller': 'user',
                post: {
                    tags: ['user'],
                    description: 'Send remind email',
                    operationId: 'remind',
                    parameters: [
                        {
                            name: 'email',
                            'in': 'formData',
                            description: 'User email',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                },
                put: {
                    tags: ['user'],
                    description: 'Remind - finish',
                    operationId: 'remindcontinue',
                    parameters: [
                        {
                            name: 'remindHash',
                            'in': 'formData',
                            description: 'Remind hash',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'pin',
                            'in': 'formData',
                            description: 'PIN',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'email',
                            'in': 'formData',
                            description: 'User email',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'password',
                            'in': 'formData',
                            description: 'New password',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/user/{userId}': {
                'x-swagger-router-controller': 'user',
                get: {
                    tags: ['user'],
                    description: 'Return user profile',
                    operationId: 'show',
                    parameters: [
                        {
                            name: 'userId',
                            'in': 'path',
                            description: 'User ID or me',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['user:read']
                        }
                    ]
                },
                put: {
                    tags: ['user'],
                    description: 'Update user profile',
                    operationId: 'update',
                    parameters: [
                        {
                            name: 'userId',
                            'in': 'path',
                            description: 'User ID or me',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'username',
                            'in': 'formData',
                            description: 'User nickname',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'name',
                            'in': 'formData',
                            description: 'User name',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'email',
                            'in': 'formData',
                            description: 'User email',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'password',
                            'in': 'formData',
                            description: 'User password',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'passwordCurrent',
                            'in': 'formData',
                            description: 'User current password',
                            type: 'string',
                            required: false
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['user:write']
                        }
                    ]
                }
            },
            '/locale/{langId}': {
                'x-swagger-router-controller': 'locale',
                get: {
                    tags: ['locale'],
                    description: 'Return user profile',
                    operationId: 'show',
                    parameters: [
                        {
                            name: 'langId',
                            'in': 'path',
                            description: 'Lang ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/ListResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/lang': {
                'x-swagger-router-controller': 'lang',
                get: {
                    tags: ['lang'],
                    description: 'Return langs',
                    operationId: 'list',
                    parameters: [],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/ListResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/auction': {
                'x-swagger-router-controller': 'auction',
                get: {
                    tags: ['auction'],
                    description: 'Return auctions',
                    operationId: 'list',
                    parameters: [],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/ListResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['auction:read']
                        }
                    ]
                },
                post: {
                    tags: ['auction'],
                    description: 'Creates auction',
                    operationId: 'add',
                    parameters: [
                        {
                            name: 'title',
                            'in': 'formData',
                            description: 'Auction title',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['auction:write']
                        }
                    ]
                }
            },
            '/auction/{auctionId}': {
                'x-swagger-router-controller': 'auction',
                get: {
                    tags: ['auction'],
                    description: 'Returns auction',
                    operationId: 'show',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'path',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['auction:read']
                        }
                    ]
                },
                put: {
                    tags: ['auction'],
                    description: 'Updates auction',
                    operationId: 'update',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'path',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        },
                        {
                            name: 'title',
                            'in': 'formData',
                            description: 'Auction title',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'status',
                            'in': 'formData',
                            description: 'Auction status',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'active',
                            'in': 'formData',
                            description: 'Auction active',
                            type: 'boolean',
                            required: false
                        },
                        {
                            name: 'action',
                            'in': 'formData',
                            description: 'Auction action',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'lotMax',
                            'in': 'formData',
                            description: 'Auction lotMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMin',
                            'in': 'formData',
                            description: 'Auction rateMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMax',
                            'in': 'formData',
                            description: 'Auction rateMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodMin',
                            'in': 'formData',
                            description: 'Auction periodMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodMax',
                            'in': 'formData',
                            description: 'Auction periodMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodStartMin',
                            'in': 'formData',
                            description: 'Auction periodStartMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodStartMax',
                            'in': 'formData',
                            description: 'Auction periodStartMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'timeAdd',
                            'in': 'formData',
                            description: 'Auction timeAdd',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMultiple',
                            'in': 'formData',
                            description: 'Auction rateMultiple',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateDefault',
                            'in': 'formData',
                            description: 'Auction rateDefault',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botActive',
                            'in': 'formData',
                            description: 'Auction botActive',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPricePeopleMin',
                            'in': 'formData',
                            description: 'Auction botPricePeopleMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPricePeopleMax',
                            'in': 'formData',
                            description: 'Auction botPricePeopleMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPriceBotMin',
                            'in': 'formData',
                            description: 'Auction botPriceBotMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPriceBotMax',
                            'in': 'formData',
                            description: 'Auction botPriceBotMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'priceStartMin',
                            'in': 'formData',
                            description: 'Auction priceStartMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'priceStartMax',
                            'in': 'formData',
                            description: 'Auction priceStartMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateDefaultModeQuant',
                            'in': 'formData',
                            description: 'Auction rateDefaultModeQuant',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateRvMorePercent',
                            'in': 'formData',
                            description: 'Auction rateRvMorePercent',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateGapMin',
                            'in': 'formData',
                            description: 'Auction rateGapMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateTimeRate1',
                            'in': 'formData',
                            description: 'Auction rateTimeRate1',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateTimeRatePlenty',
                            'in': 'formData',
                            description: 'Auction rateTimeRatePlenty',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'yellowPeriod',
                            'in': 'formData',
                            description: 'Auction yellowPeriod',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'redPeriod',
                            'in': 'formData',
                            description: 'Auction redPeriod',
                            type: 'string',
                            required: false
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['auction:write']
                        }
                    ]
                },
                delete: {
                    tags: ['auction'],
                    description: 'Removes auction',
                    operationId: 'remove',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'path',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['auction:write']
                        }
                    ]
                }
            },
            '/product': {
                'x-swagger-router-controller': 'product',
                get: {
                    tags: ['product'],
                    description: 'Return products',
                    operationId: 'list',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'query',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/ListResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['product:read']
                        }
                    ]
                },
                post: {
                    tags: ['product'],
                    description: 'Creates product',
                    operationId: 'add',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'formData',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        },
                        {
                            name: 'oid',
                            'in': 'formData',
                            description: 'Product oid',
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'price',
                            'in': 'formData',
                            description: 'Product price',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['product:write']
                        }
                    ]
                }
            },
            '/product/{productId}': {
                'x-swagger-router-controller': 'product',
                get: {
                    tags: ['product'],
                    description: 'Returns product',
                    operationId: 'show',
                    parameters: [
                        {
                            name: 'productId',
                            'in': 'path',
                            description: 'Product ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['product:read']
                        }
                    ]
                },
                put: {
                    tags: ['product'],
                    description: 'Updates product',
                    operationId: 'update',
                    parameters: [
                        {
                            name: 'productId',
                            'in': 'path',
                            description: 'Product ID',
                            type: 'integer',
                            required: true
                        },
                        {
                            name: 'oid',
                            'in': 'formData',
                            description: 'Product oid',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'price',
                            'in': 'formData',
                            description: 'Product price',
                            type: 'integer',
                            required: false
                        },
                        {
                            name: 'active',
                            'in': 'formData',
                            description: 'Product active',
                            type: 'boolean',
                            required: false
                        },
                        {
                            name: 'action',
                            'in': 'formData',
                            description: 'Product action',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMin',
                            'in': 'formData',
                            description: 'Product rateMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMax',
                            'in': 'formData',
                            description: 'Product rateMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodMin',
                            'in': 'formData',
                            description: 'Product periodMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodMax',
                            'in': 'formData',
                            description: 'Product periodMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodStartMin',
                            'in': 'formData',
                            description: 'Product periodStartMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'periodStartMax',
                            'in': 'formData',
                            description: 'Product periodStartMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'timeAdd',
                            'in': 'formData',
                            description: 'Product timeAdd',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateMultiple',
                            'in': 'formData',
                            description: 'Product rateMultiple',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateDefault',
                            'in': 'formData',
                            description: 'Product rateDefault',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botActive',
                            'in': 'formData',
                            description: 'Product botActive',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPricePeopleMin',
                            'in': 'formData',
                            description: 'Product botPricePeopleMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPricePeopleMax',
                            'in': 'formData',
                            description: 'Product botPricePeopleMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPriceBotMin',
                            'in': 'formData',
                            description: 'Product botPriceBotMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'botPriceBotMax',
                            'in': 'formData',
                            description: 'Product botPriceBotMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'priceStartMin',
                            'in': 'formData',
                            description: 'Product priceStartMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'priceStartMax',
                            'in': 'formData',
                            description: 'Product priceStartMax',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateDefaultModeQuant',
                            'in': 'formData',
                            description: 'Product rateDefaultModeQuant',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateRvMorePercent',
                            'in': 'formData',
                            description: 'Product rateRvMorePercent',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateGapMin',
                            'in': 'formData',
                            description: 'Product rateGapMin',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateTimeRate1',
                            'in': 'formData',
                            description: 'Product rateTimeRate1',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'rateTimeRatePlenty',
                            'in': 'formData',
                            description: 'Product rateTimeRatePlenty',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'yellowPeriod',
                            'in': 'formData',
                            description: 'Product yellowPeriod',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'redPeriod',
                            'in': 'formData',
                            description: 'Product redPeriod',
                            type: 'string',
                            required: false
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['product:write']
                        }
                    ]
                },
                delete: {
                    tags: ['product'],
                    description: 'Removes product',
                    operationId: 'remove',
                    parameters: [
                        {
                            name: 'productId',
                            'in': 'path',
                            description: 'Product ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['product:write']
                        }
                    ]
                }
            },
            '/tok': {
                'x-swagger-router-controller': 'tok',
                get: {
                    tags: ['tok'],
                    description: 'Return toks',
                    operationId: 'list',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'query',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/ListResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['tok:read']
                        }
                    ]
                },
                post: {
                    tags: ['tok'],
                    description: 'Creates tok',
                    operationId: 'add',
                    parameters: [
                        {
                            name: 'auctionId',
                            'in': 'formData',
                            description: 'Auction ID',
                            type: 'integer',
                            required: true
                        },
                        {
                            name: 'oid',
                            'in': 'formData',
                            description: 'Tok oid',
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['tok:write']
                        }
                    ]
                }
            },
            '/tok/{tokId}': {
                'x-swagger-router-controller': 'tok',
                get: {
                    tags: ['tok'],
                    description: 'Returns tok',
                    operationId: 'show',
                    parameters: [
                        {
                            name: 'tokId',
                            'in': 'path',
                            description: 'Tok ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['tok:read']
                        }
                    ]
                },
                put: {
                    tags: ['tok'],
                    description: 'Updates tok',
                    operationId: 'update',
                    parameters: [
                        {
                            name: 'tokId',
                            'in': 'path',
                            description: 'Tok ID',
                            type: 'integer',
                            required: true
                        },
                        {
                            name: 'oid',
                            'in': 'formData',
                            description: 'Tok oid',
                            type: 'string',
                            required: false
                        },
                        {
                            name: 'active',
                            'in': 'formData',
                            description: 'Tok active',
                            type: 'boolean',
                            required: false
                        },
                        {
                            name: 'action',
                            'in': 'formData',
                            description: 'Tok action',
                            type: 'string',
                            required: false
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['tok:write']
                        }
                    ]
                },
                delete: {
                    tags: ['tok'],
                    description: 'Removes tok',
                    operationId: 'remove',
                    parameters: [
                        {
                            name: 'tokId',
                            'in': 'path',
                            description: 'Tok ID',
                            type: 'integer',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    },
                    security: [
                        {
                            auction_auth: ['tok:write']
                        }
                    ]
                }
            },
            '/integration/auth/{provider}': {
                'x-swagger-router-controller': 'integration',
                get: {
                    tags: ['integration'],
                    description: 'Authorizes integration',
                    operationId: 'auth',
                    parameters: [
                        {
                            name: 'provider',
                            'in': 'path',
                            description: 'Provider',
                            enum: [ 'shopify' ],
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            },
            '/integration/auth/{provider}/{action}': {
                'x-swagger-router-controller': 'integration',
                get: {
                    tags: ['integration'],
                    description: 'Authorizes integration',
                    operationId: 'auth',
                    parameters: [
                        {
                            name: 'provider',
                            'in': 'path',
                            description: 'Provider',
                            enum: [ 'shopify' ],
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'action',
                            'in': 'path',
                            description: 'Provider',
                            enum: [ 'callback' ],
                            type: 'string',
                            required: true
                        }
                    ],
                    responses: {
                        200: {
                            description: 'Success',
                            schema: {
                                $ref: '#/definitions/SuccessResponse'
                            }
                        },
                        'default': {
                            description: 'Error',
                            schema: {
                                $ref: '#/definitions/ErrorResponse'
                            }
                        }
                    }
                }
            }
        },
        definitions: {
            ErrorResponse: {
                required: [
                    'error'
                ],
                properties: {
                    error: {
                        type: 'object'
                    }
                }
            },
            SuccessResponse: {
                required: [
                    'result'
                ],
                properties: {
                    result: {
                        type: 'object'
                    }
                }
            },
            ListResponse: {
                required: [
                    'success',
                    'data'
                ],
                properties: {
                    success: {
                        type: 'boolean'
                    },
                    data: {
                        type: 'array',
                        items: {
                            type: 'object'
                        }
                    }
                }
            }
        }
    };
};

module.exports = Swagger;