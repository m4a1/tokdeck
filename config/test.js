var config = {
    server: {
        port: 3701
    },
    socket: {
        port: 3702
    },
    db: {
        database: 'kalaus_tokdeck_t',
        host: 'localhost',
        username: 'kalaus_tokdeck_t',
        password: 'tokdeck',
        dialect: 'mysql',
        port: 3306,
        logging: true
    },
    cache: false,
    viewCache: false,
    mock: false,
    session: {
        secret: 'NT&O*7TN',
        resave: false,
        saveUninitialized: false,
        cookie:{
            maxAge: 60 * 60 * 24 * 30 * 1000
        }
    },
    oauth: {
        accessTokenExpire: 86400,
        state: 'auction_auth',
        realm: 'auction',
        scope: {
            user: ['user:read', 'user:write', 'auction:read', 'auction:write', 'product:read', 'product:write', 'tok:read', 'tok:write']
        }
    },
    jwtSecret: 'BR7Ri7Dni75E65EB',
    apiUrl: 'https://api.tokdeck.com',
    siteUrl: 'https://tokdeck.com',
    wwwPath: '/www',
    integration: {
        shopify: {
            key: '2c27bdec8e0d73136e2cc03181cf83ce',
            secret: 'bbc3db0b4c88874c9a9d1886b908a786',
            scopes: [ 'read_products', 'write_script_tags' ]
        }
    },
    google: {
        clientId: '281697932571-ukarhntoeuouudi5qombeukb96oeipde.apps.googleusercontent.com',
        clientSecret: 'Te45XukG5p73jR-V1VS9xax1'
    },
    twitter: {
        consumerKey: 'r4Mj7lsUu96hG9bOWyRHvQwfd',
        consumerSecret: 'Q8Dg0YFKO5bpVmtOOyrjArn9zoHkErLNIgBPlHle4rmfSVv3b8'
    },
    facebook: {
        clientID: '655451497933073',
        clientSecret: 'f15733f23b511ae0cb5f06e67cda61d2'
    },
    user: {
        expire: 86400,
        cookieExpire: 3600 * 24 * 30 * 1000,
        cookieDomain: '.tokdeck.com'
    },
    sparkpost: {
        apikey: '8760b93772e932e79df4c28e97d2b45608a78713',
        from_email: 'info@tokdeck.com',
        from_name: 'TokDeck'
    },
    image: {
        storage: __dirname + '/../../static/original',
        cache: __dirname + '/../../static/cache',
        static: 'https://i*.tokdeck.com',
        preview: {
            big: {
                width: 800,
                height: 800,
                resizeStyle: 'aspectfit'
            },
            list: {
                width: 300,
                height: 225,
                resizeStyle: 'aspectfill',
                gravity: 'North'
            }
        },
        validator: {
            user: {
                minWidth: 50,
                aspechWhMax: 3
            }
        }
    },
    dnode: {
        secret: 'retDtno*T',
        instances: {
            socket: {
                port: 8886
            }
        }
    },
    auction: {
        def: {
            lotMax: '100%',
			rateMin: 10,
			rateMax: '50%',
			periodMin: 720,
			periodMax: 2880,
			periodStartMin: 0,
			periodStartMax: 30,
			timeAdd: 15,
			rateMultiple: 100,
			rateDefault: '100%',
			botActive: 1,
			botPricePeopleMin: '90%',
			botPricePeopleMax: '110%',
			botPriceBotMin: '75%',
			botPriceBotMax: '110%',
			priceStartMin: '10%',
			priceStartMax: '20%',
			rateDefaultModeQuant: 3,
			rateRvMorePercent: 6,
			rateGapMin: 10,
			rateTimeRate1: 30,
			rateTimeRatePlenty: 1800,
			yellowPeriod: 60,
			redPeriod: 15
        }
    }
};

module.exports = config;