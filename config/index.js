var _ = require('lodash');
var fs = require('fs');

if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
}

module.exports = _.extend({
    env: process.env.NODE_ENV,
    buildId: Number(String(fs.readFileSync(__dirname + '/../.build')).trim()) || 1
}, require('./' + process.env.NODE_ENV));