'use strict';

module.exports = function (sequelize, DataTypes) {
    
    var Model = sequelize.define('Lang', {
        stitle: {
            type: DataTypes.STRING
        },
        title: {
            type: DataTypes.STRING
        },
        isDefault: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    });
    return Model;
};