'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Product', {
        oid: {
            type: DataTypes.STRING(150)
        },
        price: {
            type: DataTypes.DECIMAL(10, 2)
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        rateMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodStartMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodStartMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        timeAdd: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateMultiple: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateDefault: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botActive: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPricePeopleMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPricePeopleMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPriceBotMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPriceBotMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        priceStartMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        priceStartMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateDefaultModeQuant: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateRvMorePercent: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateGapMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateTimeRate1: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateTimeRatePlenty: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        yellowPeriod: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        redPeriod: {
            type: DataTypes.STRING,
            defaultValue: null
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.Auction, {
                        foreignKey: 'auctionId'
                });
            }
        }
    });
    return Model;
};