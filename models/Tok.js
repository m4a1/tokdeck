'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Tok', {
        oid: {
            type: DataTypes.STRING(150)
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.Auction, {
                        foreignKey: 'auctionId'
                });
            }
        }
    });
    return Model;
};