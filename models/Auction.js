'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Auction', {
        title: {
            type: DataTypes.STRING(150)
        },
        status: {
            type: DataTypes.ENUM('running', 'stopped'),
            defaultValue: 'running',
            allowNull: false
        },
        clientUrl: {
            type: DataTypes.STRING
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        lotMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodStartMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        periodStartMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        timeAdd: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateMultiple: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateDefault: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botActive: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPricePeopleMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPricePeopleMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPriceBotMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        botPriceBotMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        priceStartMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        priceStartMax: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateDefaultModeQuant: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateRvMorePercent: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateGapMin: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateTimeRate1: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        rateTimeRatePlenty: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        yellowPeriod: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        redPeriod: {
            type: DataTypes.STRING,
            defaultValue: null
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.User, {
                    foreignKey: 'userId'
                });

                Model.hasMany(models.Product, {
                    foreignKey: 'auctionId'
                });

                Model.hasMany(models.Tok, {
                    foreignKey: 'auctionId'
                });

                Model.hasMany(models.Lot, {
                    foreignKey: 'auctionId'
                });
            }
        }
    });
    return Model;
};