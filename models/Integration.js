'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Integration', {
        type: {
            type: DataTypes.STRING,
            defaultValue: '',
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.User, {
                    foreignKey: 'userId'
                });
            }
        }
    });

    return Model;
};