'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Lot', {
        price: {
            type: DataTypes.DECIMAL(10, 2)
        },
        priceStart: {
            type: DataTypes.DECIMAL(10, 2)
        },
        status: {
            type: DataTypes.ENUM('running', 'stopped'),
            defaultValue: 'stopped',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        timeStart: {
            type: DataTypes.DATE,
            defaultValue: null
        },
        timeEnd: {
            type: DataTypes.DATE,
            defaultValue: null
        },
        rateArchive: {
            type: DataTypes.TEXT,
            defaultValue: null
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.Auction, {
                        foreignKey: 'auctionId'
                });
                
                Model.belongsTo(models.Product, {
                        foreignKey: 'productId'
                });
            }
        }
    });
    return Model;
};