'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Page', {
        title: {
            type: DataTypes.STRING
        },
        metaTitle: {
            type: DataTypes.STRING
        },
        metaDescription: {
            type: DataTypes.STRING
        },
        metaKeywords: {
            type: DataTypes.STRING
        },
        stitle: {
            type: DataTypes.STRING(32),
            unique: true
        },
        message: {
            type: DataTypes.TEXT
        },
        isVisible: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.Lang, {
                        foreignKey: 'langId'
                });
            }
        }
    });
    return Model;
};