'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('IntegrationShopify', {
        guid: {
            type: DataTypes.STRING
        },
        token: {
            type: DataTypes.STRING
        },
        scope: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.Integration, {
                    foreignKey: 'integrationId'
                });
            }
        }
    });

    return Model;
};