'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('AccountLocal', {
        guid: {
            type: DataTypes.STRING
        },
        name: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.Account, {
                    foreignKey: 'accountId'
                });
            }
        }
    });
    return Model;
};