'use strict';

module.exports = function (sequelize, DataTypes) {
    
    var Model = sequelize.define('Mail', {
        email: {
            type: DataTypes.STRING,
            defaultValue: '',
            allowNull: false
        },
        subject: {
            type: DataTypes.STRING,
            defaultValue: '',
            allowNull: false
        },
        message: {
            type: DataTypes.TEXT,
            defaultValue: '',
            allowNull: false
        }
    });
    return Model;
};