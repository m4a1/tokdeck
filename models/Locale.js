'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Locale', {
        stitle: {
            type: DataTypes.STRING
        },
        value: {
            type: DataTypes.TEXT
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.Lang, {
                        foreignKey: 'langId'
                });
            }
        }
    });
    return Model;
};