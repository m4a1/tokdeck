'use strict';

module.exports = function (sequelize, DataTypes) {
    
    var Model = sequelize.define('Image', {
        url: {
            type: DataTypes.STRING(150),
            unique: true
        },
        ext: {
            type: DataTypes.STRING
        },
        dir: {
            type: DataTypes.STRING
        }
    });
    return Model;
};