'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('Message', {
        title: {
            type: DataTypes.STRING
        },
        message: {
            type: DataTypes.TEXT
        },
        type: {
            type: DataTypes.STRING
        },
        tid: {
            type: DataTypes.STRING
        },
        view: {
            type: DataTypes.STRING,
            defaultValue: 'flash',
            allowNull: false
        },
        style: {
            type: DataTypes.STRING,
            defaultValue: 'success',
            allowNull: false
        },
        timeout: {
            type: DataTypes.INTEGER,
            defaultValue: 4000,
            allowNull: false
        },
        persistent: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        isRead: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        stitle: {
            type: DataTypes.STRING
        },
        validUntil: {
            type: DataTypes.DATE
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.User, {
                        foreignKey: 'userId'
                });
            }
        }
    });
    return Model;
};