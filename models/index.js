'use strict';

var async = require('async');

if (typeof global.sequelize === 'undefined') {

    var config = require('../config');
    var Sequelize = require('sequelize');
    var path = require('path');
    var _ = require('lodash');
    var fs = require('fs');

    var s = new Sequelize(config.db.database, config.db.username, config.db.password, {
        dialect: config.db.dialect,
        host: config.db.host,
        port: config.db.port,
        logging: config.db.logging ? console.log : false,
        storage: './tmp/test.sqlite',
        pool: {
            maxConnections: 2000,
            maxIdleTime: 30
        }
    });

    if (config.db.dialect === 'mysql') {
        s.query('set names utf8mb4');
    }

    var db = {};

    fs.readdirSync(__dirname)
        .filter(function (file) {
            return (file.indexOf('.') !== 0) && (file !== 'index.js') && (file.slice(-3) === '.js');
        })
        .forEach(function (file) {

            var model = s.import(path.join(__dirname, file));

            db[model.name] = model;
        });

    Object.keys(db).forEach(function (modelName) {

        if (db[modelName].options.classMethods.hasOwnProperty('associate')) {
            db[modelName].associate(db);
        }
    });

    global.sequelize = _.extend({
        sequelize: s,
        Sequelize: Sequelize
    }, db);
}

module.exports = global.sequelize;
