'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('AccountFacebook', {
        guid: {
            type: DataTypes.STRING
        },
        name: {
            type: DataTypes.STRING
        },
        url: {
            type: DataTypes.STRING
        },
        token: {
            type: DataTypes.STRING
        },
        tokenSecret: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function (models) {
                
                Model.belongsTo(models.Account, {
                        foreignKey: 'accountId'
                });
            }
        }
    });
    return Model;
};