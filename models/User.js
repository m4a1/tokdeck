'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('User', {
        name: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING(150)
        },
        confirmationPin: {
            type: DataTypes.STRING(32),
            unique: true
        },
        remindPin: {
            type: DataTypes.STRING(32),
            unique: true
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.Lang, {
                    foreignKey: 'langId'
                });

                Model.belongsTo(models.Image, {
                    foreignKey: 'imageId'
                });
            }
        }
    });
    return Model;
};