'use strict';

module.exports = function (sequelize, DataTypes) {

    var Model = sequelize.define('AccessToken', {
        token: {
            type: DataTypes.STRING,
            allowNull: false,
            allowEmpty: false
        },
        expiredAt: {
            type: DataTypes.DATE,
            allowNull: false,
            allowEmpty: false
        },
        isRevoked: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            allowEmpty: false,
            defaultValue: false
        },
        scope: {
            type: DataTypes.STRING,
            allowNull: false,
            allowEmpty: false
        }
    }, {
        classMethods: {
            associate: function (models) {

                Model.belongsTo(models.User, {
                    foreignKey: 'userId',
                    constraints: false
                });
            }
        }
    });

    return Model;
};