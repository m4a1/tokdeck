'use strict';

var config = require('../config');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'object.tok'
});

var TokObject = function(param) {

    this.data = {};

    this.tokdeck = param.tokdeck;
    this.auction = param.auction;
    this.model = param.model;

    this.setData(this.model);
};

TokObject.prototype.setData = function(model) {

    this.data = model.toJSON();
};

module.exports = TokObject;