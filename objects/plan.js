'use strict';

var async = require('async');
var _ = require('lodash');
var logger = require('../lib/logger')({
    prefix: 'object.plan'
});

var PlanObject = function(param) {

    this.tokdeck = param.tokdeck;
    this.auction = param.auction;
    this.lot = param.lot;

    this.data = {
        date: param.date,
        rate: param.rate,
        user: param.user
    };
};

PlanObject.prototype.start = function(callback) {

    logger.info('PlanObject', 'start', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            that.tokdeck.queue.add('plan_go_' + that.getId(), that.data.date, function() {

                that.lot.rate({
                    user: that.data.user,
                    rate: that.lot.data.price + that.data.rate
                }, function() {
                    //
                });

                var index = _.findIndex(that.lot.plans, function(plan) {
                    return plan.data.date.getTime() === that.date.data.getTime();
                });

                if (index !== -1) {
                    that.lot.plans.splice(index, 1);
                }
            });

            return next();
        }
    ], function(err) {

        logger.info('PlanObject', 'start', 'finish', that.getId());

        if (err) {
            logger.error('PlanObject', 'start', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

PlanObject.prototype.stop = function(callback) {

    logger.info('PlanObject', 'stop', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            that.tokdeck.queue.remove('plan_go_' + that.getId(), that.data.date);

            return next();
        }
    ], function(err) {

        logger.info('PlanObject', 'stop', 'finish', that.getId());

        if (err) {
            logger.error('PlanObject', 'stop', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

PlanObject.prototype.getId = function() {

    return this.lot.getId() + '_' + this.data.date.getTime();
};

module.exports = PlanObject;