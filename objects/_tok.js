'use strict';

var _ = require('lodash');

var AuctionUser = function(auction, data) {
	this.d = {};
	this.a = auction.a;
	this.auction = auction;
	this.model = data;
	var c_user = this.a.config.bidcloud.user_default;
	var field = _.union(
		['oid'],
		Object.keys(c_user)
	);
	field.forEach(function(k) {
		this.d[k] = data[k];
	}.bind(this));
};

module.exports = AuctionUser;