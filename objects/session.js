'use strict';

var config = require('../config');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'object.session'
});

var SessionObject = function(param) {

    this.data = {};

    this.tokdeck = param.tokdeck;
    this.model = param.model;

    this.setData(this.model);
};

SessionObject.prototype.setData = function(model) {

    this.data = model.toJSON();
};

module.exports = SessionObject;