'use strict';

var mongoose = require('mongoose'),
	common = require('../helpers/common'),
	_ = require('lodash'),
	Auction = require('./auction'),
	ModelAuctionNotify = mongoose.model('AuctionNotify'),
	ModelAuctionLot = mongoose.model('AuctionLot'),
	ModelAuctionGoods = mongoose.model('AuctionGoods'),
	AuctionGoods = require('./auctionGoods'),
	AuctionUser = require('./auctionUser'),
	AuctionLot = require('./auctionLot');

var Auction = function(bidcloud, data) {
	this.d = {};
	this.lot_by_id = {};
	this.user_by_oid = {};
	this.goods_by_oid = {};
	this.goods_by_id = {};
	this.notify_array = [];
	this.free_oid = [];
	this.token = null;
	this.timer_install = null;
	this.timer_notify = null;
	this.a = bidcloud;
	this.model = data;
	var c_auction = this.a.config.bidcloud.auction_default;
	var field = _.union(
		['oid', 'title', 'description', 'active', 'client_url', 'state'],
		Object.keys(c_auction)
	);
	field.forEach(function(k) {
		this.d[k] = typeof c_auction[k] !== 'undefined' && typeof data[k] === 'undefined' || String(data[k]).length === 0 ? c_auction[k] : data[k];
	}.bind(this));
};

Auction.prototype.init = function(callback) {
	require('async').series([
		function(cb) {
			// goods
			require('async').eachSeries(this.model.goods, function(el, cb1) {
				setImmediate(function() {
					this.goodsLoad(el, function(err) {
						if (err) return callback(err);
						cb1();
					}.bind(this));
				}.bind(this));
			}.bind(this), function() {
				cb();
			}.bind(this));
		}.bind(this),
		function(cb) {
			// user
			require('async').eachSeries(this.model.users, function(el, cb1) {
				setImmediate(function() {
					this.userLoad(el, function(err) {
						if (err) return callback(err);
						cb1();
					}.bind(this));
				}.bind(this));
			}.bind(this), function() {
				cb();
			}.bind(this));
		}.bind(this),
		function(cb) {
			// lots
			require('async').eachSeries(this.model.lots, function(el, cb1) {
				setImmediate(function() {
					this.lotLoad(el, function(err) {
						if (err) return callback(err);
						cb1();
					}.bind(this));
				}.bind(this));
			}.bind(this), function() {
				cb();
			}.bind(this));
		}.bind(this),
		function(cb) {
			// notify
			this.notify_array = this.model.notifies;
			cb();
		}.bind(this)
	], function() {
		callback();
	}.bind(this));
};

Auction.prototype.run = function(callback) {
	if (this.d.state === 'running') {
		// install timer
		this.timer_install = setInterval(function() {
			this.lotInstall();
		}.bind(this), this.a.config.bidcloud.general.period_install);
		// starting lots
		require('async').eachSeries(this.lot_by_id, function(card, cb) {
			card.run(function (err) {
				if (err) return this.a.error(err);
				cb();
			}.bind(this));
		}.bind(this), function() {
			callback();
		}.bind(this));
		//starting notify
		this.timer_notify = setInterval(function() {
			var first = this.notify_array.shift();
			if (!first) return;
			var data = {};
			try {
				data = JSON.parse(first.data);
			}
			catch (e) {
				return this.a.error({
					code: 2342
				});
			}
			common.clientRequest(first.point, _.extend(data, {
				url: this.d.client_url,
				token: this.model._token.token,
				token_secret: this.model._token.token_secret
			}), function(err, res) {
				if (err) {
					this.notify_array.push(first);
					return this.a.error(err);
				}
				if (res && res.ok) {
					first.remove(function(err) {
						if (err) return this.a.error(err);
					});
				}
				else {
					this.notify_array.push(first);
					return this.a.error({
						code: 35
					});
				}
			}.bind(this));
		}.bind(this), this.a.config.bidcloud.general.period_notify);
	}
	else callback();
};


Auction.prototype.stop = function(callback) {
	clearInterval(this.timer_install);
	clearInterval(this.timer_notify);
	require('async').eachSeries(this.lot_by_id, function(el, cb1) {
		el.stop(function() {
			cb1();
		}.bind(this));
	}.bind(this), function() {
		callback();
	}.bind(this));
};

Auction.prototype.userLoad = function(card, callback) {
	this.user_by_oid[card.oid] = new AuctionUser(this, card);
	callback();
};

Auction.prototype.goodsFetch = function(id, callback) {
	var goods = this.goods_by_id[id];
	if (goods) callback(null, goods);
	else {
		ModelAuctionGoods.findOne({
			_id: id,
			active: 1,
			show_it: 1
		}, function(err, card) {
			if (err) return callback(err);
			if (card) this.goodsLoad(card, callback);
			else callback();
		}.bind(this));
	}
};

Auction.prototype.lotFetch = function(id, callback) {
	var lot = this.lot_by_id[id];
	if (lot) callback(null, lot);
	else {
		ModelAuctionLot.findOne({
			_id: id,
			show_it: 1
		}, function(err, card) {
			if (err) return callback(err);
			if (card) {
				this.goodsFetch(card._goods, function(err, goods) {
					if (err) return callback(err);
					if (goods) this.lotLoad(card, callback);
					else callback();
				}.bind(this));
			}
			else callback();
		}.bind(this));
	}
};

Auction.prototype.lotLoad = function(card, callback) {
	var lot = new AuctionLot(this, card);
	if (lot.goods) {
		this.lot_by_id[card.id] = lot;
		this.goodsOccupy(this.lot_by_id[card.id].goods.d.oid);
		this.lot_by_id[card.id].init(function() {
			this.lot_by_id[card.id].run(function () {
				callback(null, this.lot_by_id[card.id]);
			}.bind(this));
		}.bind(this));
	}
	else {
		callback({
			code: 333
		});
	}
};

Auction.prototype.lotUnload = function(id, callback) {
	if (this.lot_by_id[id]) this.lot_by_id[id].stop(function() {
		delete this.lot_by_id[id];
		callback();
	}.bind(this));
	else callback();
};

Auction.prototype.goodsLoad = function(card, callback) {
	this.goods_by_id[card._id] = this.goods_by_oid[card.oid] = new AuctionGoods(this, card);
	this.goodsFree(this.goods_by_oid[card.oid].d.oid);
	callback(null, this.goods_by_oid[card.oid]);
};

Auction.prototype.lotInstall = function() {
	if (!this.a.started || this.d.state !== 'running') return;
	var first = this.a.queueFirst();
	if (first && ((new Date()).getTime() - first) / 1000 > 1) return;
	if (!this.free_oid.length) return;
	var oid = this.free_oid.shift();
	this.goodsOccupy(oid);
	this.lotCreate(oid, function(card) {
		this.a.dbSave(ModelAuctionLot, null, card, null, function(err, lot) {
			if (err) return this.a.error(err);
			this.lotLoad(lot, function(err, data) {
				if (err) return this.a.error(err);
				// notify client
				this.queueNotify('lot_add', {
					lot: lot._id,
					auction: this.d.oid,
					goods: data.goods.d.oid
				}, function() {
					//
				}.bind(this));
			}.bind(this));
		}.bind(this));
	}.bind(this));
};

Auction.prototype.lotCreate = function(id, callback) {
	var goods = this.goods_by_oid[id];

	//price start
	var price_start = _.random(
		common.percentParam(goods.d.price_start_min, goods.d.price),
		common.percentParam(goods.d.price_start_max, goods.d.price)
	);
	price_start = Math.floor(price_start / goods.d.rate_multiple) * goods.d.rate_multiple;
	if (price_start < 0) price_start = 0;

	// period
	var period = _.random(
		Number(goods.d.period_min),
		Number(goods.d.period_max)
	);

	// period start
	var period_start = _.random(
		Number(goods.d.period_start_min),
		Number(goods.d.period_start_max)
	);

	// start
	var start = ((new Date()).getTime() / 1000) + (period_start * 60);

	if (period < 5) period = 5;
	var card = {
		_auction: this.model._id,
		_goods: goods.model._id,
		price_start: price_start,
		price: price_start,
		start_time: new Date(start * 1000),
		end_time: new Date(start * 1000 + period * 60000)
	};
	callback(card);
};

Auction.prototype.goodsFree = function(oid) {
	var ind = require('phpjs').array_search(oid, this.free_oid);
	if (ind === false) this.free_oid.push(oid);
};

Auction.prototype.goodsOccupy = function(oid) {
	var ind = require('phpjs').array_search(oid, this.free_oid);
	if (ind !== false) this.free_oid.splice(ind, 1);
};

Auction.prototype.queueNotify = function(point, data, callback) {
	ModelAuctionNotify.create({
		_auction: this.model._id,
		point: point,
		data: JSON.stringify(data)
	}, function(err, notify) {
		if (err) return this.a.error(err);
		this.notify_array.push(notify);
		callback();
	}.bind(this));
};

Auction.prototype.clientRate = function(socket, user, data) {
	if (!data.lot || !data.rate) return this.a.socketError(socket, 41);
	if (!user) return this.a.socketError(socket, 46);
	this.lotFetch(data.lot, function(err, lot) {
		if (err) return this.a.error(err);
		if (!lot) return this.a.socketError(socket, 45);
		lot.rate(socket, user, data.rate);
	}.bind(this));
};

Auction.prototype.clientRefresh = function(socket, user, data) {
	if (!data.lot) return this.a.socketError(socket, 41);
	this.lotFetch(data.lot, function(err, lot) {
		if (err) return this.a.error(err);
		if (!lot) return this.a.socketError(socket, 45);
		lot.refresh([socket]);
	}.bind(this));
};

Auction.prototype.clientTime = function(socket, user, data) {
	socket.emit('time', (new Date()).getTime());
};

module.exports = Auction;