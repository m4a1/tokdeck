'use strict';

var mongoose = require('mongoose'),
	common = require('../helpers/common'),
	_ = require('lodash'),
	ModelAuctionLot = mongoose.model('AuctionLot'),
	AuctionPlan = require('./auctionPlan');

var AuctionLot = function(auction, data) {
	this.d = {};
	this.a = auction.a;
	this.auction = auction;
	this.goods = auction.goods_by_id[data._goods];
	this.model = data;
	this.bot = [];
	this.plan = [];
	this.socket = {};
	var field = ['price', 'price_start', 'price_start', 'active', 'start_time', 'end_time', 'started', 'state', 'rate_archive'];
	var d = {};
	field.forEach(function(k) {
		d[k] = data[k];
	}.bind(this));
	this.setData(d);
};

AuctionLot.prototype.init = function(callback) {
	callback();
};

AuctionLot.prototype.run = function(callback) {
	if (this.d.state === 'running') {
		// plan start
		if (!this.d.started) {
			this.a.addQueue(this.d.start_time, 'lot_start_' + this.model._id, function() {
				this.start();
			}.bind(this));
		}
		// plan end
		this.planEnd();
		this.createPlan();
	}
	callback();
};

AuctionLot.prototype.planEnd = function() {
	if (this.d.active) {
		this.a.addQueue(this.d.end_time, 'lot_end_' + this.model._id, function() {
			this.end();
		}.bind(this));
	}
};

AuctionLot.prototype.start = function() {
	if (this.d.started) return;
	this.d.started = 1;
	this.save(['started'], function() {
		//
	}.bind(this));
};

AuctionLot.prototype.end = function() {
	if (!this.d.active) return;
	this.d.active = 0;
	this.d.state = 'finished';
	this.save(['active', 'state'], function() {
		this.auction.goodsFree(this.goods.d.oid);
	}.bind(this));
	var last = this.d.rate_archive.length === 0 ? null : this.d.rate_archive[this.d.rate_archive.length - 1];
	// notify
	this.auction.queueNotify('lot_remove', {
		lot: this.model._id,
		auction: this.auction.d.oid,
		user: last ? last.user : '',
		price: last ? this.d.price : '',
		goods: this.goods.d.oid
	}, function() {
		//
	}.bind(this));
	this.refresh();
};

AuctionLot.prototype.stop = function(callback) {
	this.planClear(function() {
		callback();
	}.bind(this));
};

AuctionLot.prototype.save = function(field, callback) {
	this.a.dbSave(ModelAuctionLot, this.model._id, this.d, field, function() {
		callback();
	}.bind(this));
};

AuctionLot.prototype.setData = function(data) {
	if (data) for (var k in data) {
		if (k === 'started' || k === 'price_start' || k === 'price' || k === 'active' || k === 'show_it') this.d[k] = Number(data[k]);
		else if (k === 'rate_archive') {
			if (data[k] && String(data[k]).length > 0) {
				this.d[k] = [];
				try {
					this.d[k] = JSON.parse(data[k]);
				}
				catch (e) { }
			}
			else this.d[k] = [];
		}
		else this.d[k] = data[k];
	}
};

AuctionLot.prototype.getData = function(field) {
	var ret = this.d;
	if (!field) return ret;
	var ret_clean = {};
	for (var k in field) {
		if (field[k] === 'rate_archive') ret_clean[field[k]] = JSON.stringify(ret[field[k]]);
		else ret_clean[field[k]] = ret[field[k]];
	}
	return ret_clean;
};

AuctionLot.prototype.addSocket = function(socket) {
	if (typeof this.socket[socket.id] === 'undefined') this.socket[socket.id] = socket;
};

AuctionLot.prototype.rate = function(socket, user, rate) {
	var dirty = false;
	var is_user_bot = typeof this.auction.user_by_oid[user] !== 'undefined';
	if (this.finished) {
		if (socket) socket.emit('user_error', {
			code: 57
		});
		return;
	}
	if (!this.d.started) {
		if (socket) socket.emit('user_error', {
			code: 56
		});
		return;
	}
	if (this.d.rate_archive.length > 0 && this.d.rate_archive[this.d.rate_archive.length - 1].user === user) {
		if (socket) socket.emit('user_error', {
			code: 52
		});
		return;
	}
	var rate_clean = 0;
	var now = new Date();
	if (this.d.price < rate) rate_clean = rate - this.d.price;
	var rate_min = common.percentParam(this.goods.d.rate_min, this.goods.d.price);
	if (rate_clean < rate_min) {
		if (socket) socket.emit('user_error', {
			code: 54,
			rate_min: rate_min
		});
		return;
	}
	var rate_max = common.percentParam(this.goods.d.rate_max, this.goods.d.price);
	if (rate_clean > rate_max) {
		if (socket) socket.emit('user_error', {
			code: 55,
			rate_max: rate_max
		});
		return;
	}
	if (rate_clean / this.goods.d.rate_multiple !== Math.floor(rate_clean / this.goods.d.rate_multiple)) {
		if (socket) socket.emit('user_error', {
			code: 53,
			rate_multiple: this.goods.d.rate_multiple
		});
		return;
	}
	if (!is_user_bot) dirty = true;
	this.a.removeQueue(this.d.end_time, 'lot_end_' + this.model._id);
	this.d.end_time = new Date(this.d.end_time.getTime() + this.goods.d.time_add * 1000);
	this.d.price = this.d.price + rate_clean;

	this.d.rate_archive.push({
		rate: rate_clean,
		user: user,
		date: now
	});
	this.save(['end_time', 'price', 'rate_archive'], function() {
		//
	}.bind(this));
	this.planEnd();
	this.refresh();
	if (dirty) this.createPlan();
	if (socket) {
		socket.emit('rate', {
			id: this.model._id,
			user: user,
			rate: rate,
			rate_clean: rate_clean,
			price: this.d.price
		});
		socket.emit('user_info', {
			code: 51
		});
	}
};

AuctionLot.prototype.refresh = function(socket) {
	if (!socket) {
		socket = [];
		for (var k in this.socket) {
			socket.push(this.socket[k]);
		}
	}
	var user = null;
	if (this.d.rate_archive.length) {
		user = this.d.rate_archive[this.d.rate_archive.length - 1].user;
	}
	var rate_default = common.percentParam(this.goods.d.rate_default, this.goods.d.rate_multiple);
	var data = {
		yellow_period: this.goods.d.yellow_period,
		red_period: this.goods.d.red_period,
		active: this.d.active,
		auction: this.auction.d.oid,
		id: this.model._id,
		price: this.d.price,
		rate_default: rate_default,
		start_time: this.d.start_time.getTime(),
		end_time: this.d.end_time.getTime(),
		user: user
	};
	if (socket.length) socket.forEach(function(el) {
		el.emit('refresh', data);
	});
};

AuctionLot.prototype.createPlan = function() {
	var is_last_bot = this.d.rate_archive.length === 0 ? true : typeof this.auction.user_by_oid[this.d.rate_archive[this.d.rate_archive.length - 1].user] !== 'undefined';
	var rates = [];
	var times = [];
	var users = [];
	var k;
	var i;
	if (this.bot.length === 0) {
		var keys = [];
		for (k in this.auction.user_by_oid) keys.push(k);
		keys = require('phpjs').shuffle(keys);
		var cnt = 0;
		keys.forEach(function(el) {
			if (cnt <= 9) this.bot.push(el);
		}.bind(this));
	}
	var time_end = this.d.end_time.getTime() / 1000;
	var time_start = Math.floor(Math.max((new Date()).getTime(), this.d.start_time.getTime()) / 1000);
	if (time_start > time_end) time_start = time_end;

	// Получаем оптимальную цену с людьми
	var price_optimal_people = _.random(
		common.percentParam(this.goods.d.bot_price_people_min, this.goods.d.price),
		common.percentParam(this.goods.d.bot_price_people_max, this.goods.d.price)
	);

	// Получаем оптимальную цену без людей
	var price_optimal_bot = _.random(
		common.percentParam(this.goods.d.bot_price_bot_min, this.goods.d.price),
		common.percentParam(this.goods.d.bot_price_bot_max, this.goods.d.price)
	);

	var price_optimal = is_last_bot ? price_optimal_bot : price_optimal_people;
	price_optimal = Math.floor(price_optimal / this.goods.d.rate_multiple) * this.goods.d.rate_multiple;

	if (price_optimal > this.d.price || this.d.rate_archive.length === 0) {
		var rate_default = common.percentParam(this.goods.d.rate_default, this.goods.d.rate_multiple);
		time_end = Math.floor(this.d.end_time.getTime() / 1000);

		var time_left = time_end - time_start;
		if (time_left <= 0) time_left = 15;

		var rate_min = common.percentParam(this.goods.d.rate_min, this.goods.d.price);
		rate_min = Math.ceil(rate_min / this.goods.d.rate_multiple) * this.goods.d.rate_multiple;

		var rate_max = common.percentParam(this.goods.d.rate_max, this.goods.d.price);
		rate_max = Math.ceil(rate_max / this.goods.d.rate_multiple) * this.goods.d.rate_multiple;
		if (rate_max < rate_min) rate_max = rate_min;

		var left_price_percent = price_optimal - this.d.price <= rate_default * this.goods.d.rate_default_mode_quant ? 0 : 100 - Math.floor(this.d.price / price_optimal * 100);

		var pattern_price_optimal = this.genPriceOptimal(left_price_percent, is_last_bot);
		if (typeof pattern_price_optimal === 'undefined') pattern_price_optimal = [];

		var pattern_rate_optimal = this.genRateOptimal(pattern_price_optimal, time_left, is_last_bot);
		if (typeof pattern_rate_optimal === 'undefined') pattern_rate_optimal = 'X';
		i = 0;
		var total = this.d.price;
		for (k = 0; k < pattern_rate_optimal.length; k += 1) {
			var rate = rate_default;
			if (pattern_rate_optimal.slice(k, 1) === 'O') {
				rate = Math.ceil(common.percentParam(pattern_price_optimal[i] + '%', price_optimal));
				if (rate !== rate_default) {
					var next = total + rate;

					// Находим порядок будущей цены лота
					var dec_mult;
					if (next < 100) dec_mult = 10;
					else if (next < 1000) dec_mult = _.random(0, 2) === 0 ? 100 : 10;
					else if (next < 10000) dec_mult = _.random(0, 2) === 0 ? 1000 : 100;
					else dec_mult = 1000;

					// Находим "красивую" будущую цену лота, исходя из порядка
					var total_good = Math.ceil(next / dec_mult) * dec_mult;

					// Выравниваем ставку
					rate = total_good - total;

					if (rate > rate_max) rate = rate_max;
					if (rate < rate_min) rate = rate_min;
				}
				i += 1;
			}
			rates.push(rate);
			total += rate;
		}

		// Исключительный случай - ставок не задано, а они нужны
		if (!is_last_bot && rates.length === 0) {
			rates.push(rate_default);
		}

		if (rates.length > 0) {
			i = rates.length - 1;
			while (i >= 0 && total > price_optimal) {
				if (rates[i] !== rate_default) {
					rates[i] -= (total - price_optimal);
					if (rates[i] < rate_min) rates[i] = rate_min;
					total = require('phpjs').array_sum(rates);
				}
				i -= 1;
			}

			var pattern_time_good = [];
			for (var k1 in this.a.config.bidcloud.pattern.time) {
				var rate_quant = String(this.a.config.bidcloud.pattern.time[k1]).match(/X/g);
				rate_quant = rate_quant ? rate_quant.length : 0;
				if (rate_quant === rates.length) {
					pattern_time_good.push(this.a.config.bidcloud.pattern.time[k1]);
				}
			}
			var pattern_time_optimal = pattern_time_good[_.random(0, pattern_time_good.length - 1)];

			var step = (time_end - time_start) / pattern_time_optimal.length;

			var start = time_start;
			for (k in pattern_time_optimal) {
				if (pattern_time_optimal[k] === 'X') {
					times.push(_.random(start, Math.floor(start + step)));
				}
				start += step;
			}

			var pattern_user_good = [];
			for (var k2 in this.a.config.bidcloud.pattern.people) {
				if (this.a.config.bidcloud.pattern.people[k2].length === rates.length) {
					pattern_user_good.push(this.a.config.bidcloud.pattern.people[k2]);
				}
			}
			var pattern_user_optimal = pattern_user_good[_.random(0, pattern_user_good.length - 1)];
			for (var k3 in pattern_user_optimal) {
				users.push(this.bot[Number(pattern_user_optimal[k3]) - 1]);
			}
		}
	}
	// Удаляем старый план
	this.planClear(function() {
		if (rates.length > 0) {
			for (var c in rates) {
				var plan = new AuctionPlan(this, {
					lot: this.model._id,
					rate: rates[c],
					user: users[c],
					date: new Date(times[c] * 1000)
				});
				this.plan.push(plan);
				plan.start();
			}
		}
	}.bind(this));
};

AuctionLot.prototype.genPriceOptimal = function(left_price_percent, is_last_bot) {
	var pattern_price_good = [];
	for (var k in this.a.config.bidcloud.pattern.price) {
		var sum = require('phpjs').array_sum(this.a.config.bidcloud.pattern.price[k]);
		if (
			is_last_bot ? (
				(left_price_percent === 0 && sum === 0) || // Если оставшийся процент цены обнулен и дергающих ставок в плане нет
				(sum >= left_price_percent && sum <= left_price_percent * (1 + this.goods.d.rate_rv_more_percent / 100)) // Если сумма дергающих шаблона превышает оптимальную цену, но не более чем на заданный процент
			) : this.a.config.bidcloud.pattern.price[k].length <= 1
		) {
			pattern_price_good.push(this.a.config.bidcloud.pattern.price[k]);
		}
	}
	return pattern_price_good.length === 0 && !is_last_bot ? this.genPriceOptimal(left_price_percent, true) : pattern_price_good[_.random(0, pattern_price_good.length - 1)];
};

AuctionLot.prototype.genRateOptimal = function(pattern_price_optimal, time_left, is_last_bot) {
	var pattern_rate_good = [];
	for (var k in this.a.config.bidcloud.pattern.rate) {
		var rv_quant = String(this.a.config.bidcloud.pattern.rate[k]).match(/O/g);
		rv_quant = rv_quant ? rv_quant.length : 0;
		if (
			(rv_quant === pattern_price_optimal.length) && // во-первых количество ставок должно быть равно оптимальному
			(
				time_left === 0 ||
				time_left > (this.a.config.bidcloud.pattern.rate[k].length * this.goods.d.rate_gap_min + this.a.config.bidcloud.pattern.rate[k].length)
			) && // во-вторых, времени не осталось, либо его осталось больше чем сумма минимальных пауз между ставками
			(
				is_last_bot ||
				(time_left > this.goods.d.rate_time_rate_plenty ? true : (time_left > this.goods.d.rate_time_rate_1 ? this.a.config.bidcloud.pattern.rate[k].length <= 3 : this.a.config.bidcloud.pattern.rate[k].length <= 1))
			) // в-третьих, последним ставил бот, либо количество ставок в плане более 3х если времени больше чем 15 секунд или не более 1ой
		) {
			pattern_rate_good.push(this.a.config.bidcloud.pattern.rate[k]);
		}
	}

	var ret = '';
	if (pattern_rate_good.length === 0) {
		if (!is_last_bot && time_left !== 0) {
			ret = this.genRateOptimal(pattern_price_optimal, time_left, true);
			if (ret.length === 0) ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, is_last_bot);
		}
		else if (!is_last_bot) {
			ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, true);
		}
		else if (time_left !== 0) {
			ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, is_last_bot);
		}
	}
	if (ret.length === 0 && pattern_rate_good.length > 0) {
		ret = pattern_rate_good[_.random(0, pattern_rate_good.length - 1)];
	}
	return ret;
};

AuctionLot.prototype.planClear = function(callback) {
	require('async').eachSeries(this.plan, function(el, cb1) {
		el.stop(function() {
			cb1();
		}.bind(this));
	}.bind(this), function() {
		this.plan = [];
		if (callback) callback();
	}.bind(this));
};

module.exports = AuctionLot;