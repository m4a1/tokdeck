'use strict';

var config = require('../config');
var _ = require('lodash');
var async = require('async');
var UtilService = require('../services/util');
var PlanObject = require('../objects/plan');
var logger = require('../lib/logger')({
    prefix: 'object.lot'
});

var LotObject = function(param) {

    this.data = {
        rateArchive: []
    };

    this.tokdeck = param.tokdeck;
    this.auction = param.auction;
    this.model = param.model;

    this.id = null;
    this.started = false;
    this.plans = [];
    this.bot = [];

    this.setData(this.model);

    this.product = this.auction.productById[this.data.productId];
    this.ended = this.data.timeEnd.getTime() < (new Date()).getTime();
};

LotObject.prototype.setData = function(model) {

    this.data = model.toJSON();

    this.data.rateArchive = this.data.rateArchive
        ? JSON.parse(this.data.rateArchive)
        : [];
};

LotObject.prototype.init = function(callback) {

    logger.info('LotObject', 'init', 'start', this.getId());

    logger.info('LotObject', 'init', 'finish', this.getId());

	return callback();
};

LotObject.prototype.run = function(callback) {

    logger.info('LotObject', 'run', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            return that.planStart(next);
        },
        function(next) {

            return that.planEnd(next);
        }
    ], function(err) {

        logger.info('LotObject', 'run', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'run', that.getId(), err);
        }

        return callback();
    });
};

LotObject.prototype.planStart = function(callback) {

    logger.info('LotObject', 'planStart', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            if (!that.data.active) {
                return next({code: 0, message: 'Lot not active'});
            }

            if (that.started || that.ended) {
                return next();
            }

            that.tokdeck.queue.add('lot_start_' + that.getId(), that.data.timeStart, function() {

                return that.start();
            });

            return next();
        }
    ], function(err) {

        logger.info('LotObject', 'planStart', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'planStart', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

LotObject.prototype.planEnd = function(callback) {

    logger.info('LotObject', 'planEnd', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            if (!that.data.active) {
                return next({code: 0, message: 'Lot not active'});
            }

            if (that.ended) {
                return next();
            }

            that.tokdeck.queue.add('lot_end_' + that.getId(), that.data.timeEnd, function() {

                return that.end();
            });

            return next();
        }
    ], function(err) {

        logger.info('LotObject', 'planEnd', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'planEnd', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

LotObject.prototype.createPlan = function(callback) {

    logger.info('LotObject', 'createPlan', 'start', this.getId());

    var that = this;
    var utilService = new UtilService();
    var plans = [];

    return async.series([
        function(next) {

            var isLastBot = that.data.rateArchive.length === 0
                ? true
                : typeof that.auction.tokByOid[that.data.rateArchive[that.data.rateArchive.length - 1].user] !== 'undefined';

            var k;
            var i;
            var rates = [];
            var times = [];
            var users = [];

            if (!that.bot.length) {

                var keys = [];

                for (k in that.auction.tokByOid) {
                    keys.push(k);
                }

                keys = _.shuffle(keys);

                var cnt = 0;

                keys.forEach(function(el) {

                    if (cnt <= 9) {
                        that.bot.push(el);
                    }
                });
            }

            var timeEnd = that.data.timeEnd.getTime() / 1000;

            var timeStart = Math.floor(Math.max((new Date()).getTime(), that.data.timeStart.getTime()) / 1000);

            if (timeStart > timeEnd) {
                timeStart = timeEnd;
            }

            // Получаем оптимальную цену с людьми
            var priceOptimalPeople = _.random(
                utilService.percentParam(that.product.data.botPricePeopleMin, that.product.data.price),
                utilService.percentParam(that.product.data.botPricePeopleMax, that.product.data.price)
            );

            // Получаем оптимальную цену без людей
            var priceOptimalBot = _.random(
                utilService.percentParam(that.product.data.botPriceBotMin, that.product.data.price),
                utilService.percentParam(that.product.data.botPriceBotMax, that.product.data.price)
            );

            var priceOptimal = isLastBot ? priceOptimalBot : priceOptimalPeople;

            priceOptimal = Math.floor(priceOptimal / that.product.data.rateMultiple) * that.product.data.rateMultiple;

            if (priceOptimal > that.data.price || that.data.rateArchive.length === 0) {

                var rateDefault = utilService.percentParam(that.product.data.rateDefault, that.product.data.rateMultiple);

                timeEnd = Math.floor(that.data.timeEnd.getTime() / 1000);

                var timeLeft = timeEnd - timeStart;

                if (timeLeft <= 0) {
                    timeLeft = 15;
                }

                var rateMin = utilService.percentParam(that.product.data.rateMin, that.product.data.price);

                rateMin = Math.ceil(rateMin / that.product.data.rateMultiple) * that.product.data.rateMultiple;

                var rateMax = utilService.percentParam(that.product.data.rateMax, that.product.data.price);

                rateMax = Math.ceil(rateMax / that.product.data.rateMultiple) * that.product.data.rateMultiple;

                if (rateMax < rateMin) {
                    rateMax = rateMin;
                }

                var leftPricePercent = priceOptimal - that.data.price <= rateDefault * that.product.data.rateDefaultModeQuant ? 0 : 100 - Math.floor(that.data.price / priceOptimal * 100);

                var patternPriceOptimal = that.genPriceOptimal(leftPricePercent, isLastBot);

                if (typeof patternPriceOptimal === 'undefined') {
                    patternPriceOptimal = [];
                }

                var patternRateOptimal = that.genRateOptimal(patternPriceOptimal, timeLeft, isLastBot);

                if (typeof patternRateOptimal === 'undefined') {
                    patternRateOptimal = 'X';
                }

                i = 0;

                var total = that.data.price;

                for (k = 0; k < patternRateOptimal.length; k++) {

                    var rate = rateDefault;

                    if (patternRateOptimal.slice(k, k + 1) === 'O') {

                        rate = Math.ceil(utilService.percentParam(patternPriceOptimal[i] + '%', priceOptimal));

                        if (rate !== rateDefault) {

                            var nextRate = total + rate;

                            // Находим порядок будущей цены лота
                            var decMult;

                            if (nextRate < 100) {
                                decMult = 10;
                            } else if (nextRate < 1000) {
                                decMult = _.random(0, 2) === 0 ? 100 : 10;
                            } else if (nextRate < 10000) {
                                decMult = _.random(0, 2) === 0 ? 1000 : 100;
                            } else decMult = 1000;

                            // Находим "красивую" будущую цену лота, исходя из порядка
                            var totalGood = Math.ceil(nextRate / decMult) * decMult;

                            // Выравниваем ставку
                            rate = totalGood - total;

                            if (rate > rateMax) rate = rateMax;
                            if (rate < rateMin) rate = rateMin;
                        }

                        i += 1;
                    }

                    rates.push(rate);

                    total += rate;
                }

                // Исключительный случай - ставок не задано, а они нужны
                if (!isLastBot && rates.length === 0) {
                    rates.push(rateDefault);
                }

                if (rates.length > 0) {

                    i = rates.length - 1;

                    while (i >= 0 && total > priceOptimal) {

                        if (rates[i] !== rateDefault) {

                            rates[i] -= (total - priceOptimal);

                            if (rates[i] < rateMin) {
                                rates[i] = rateMin;
                            }

                            total = _.sum(rates);
                        }

                        i -= 1;
                    }

                    var patternTimeGood = [];

                    for (var k1 in config.auction.pattern.time) {

                        var rateQuant = String(config.auction.pattern.time[k1]).match(/X/g);

                        rateQuant = rateQuant ? rateQuant.length : 0;

                        if (rateQuant === rates.length) {
                            patternTimeGood.push(config.auction.pattern.time[k1]);
                        }
                    }

                    var patternTimeOptimal = patternTimeGood[_.random(0, patternTimeGood.length - 1)];

                    var step = (timeEnd - timeStart) / patternTimeOptimal.length;

                    var start = timeStart;

                    for (k in patternTimeOptimal) {

                        if (patternTimeOptimal[k] === 'X') {
                            times.push(_.random(start, Math.floor(start + step)));
                        }

                        start += step;
                    }

                    var patternUserGood = [];

                    for (var k2 in config.auction.pattern.people) {

                        if (config.auction.pattern.people[k2].length === rates.length) {
                            patternUserGood.push(config.auction.pattern.people[k2]);
                        }
                    }

                    var patternUserOptimal = patternUserGood[_.random(0, patternUserGood.length - 1)];

                    for (var k3 in patternUserOptimal) {

                        users.push(that.bot[Number(patternUserOptimal[k3]) - 1]);
                    }
                }
            }

            var info = [];

            for (var c in rates) {

                var date = new Date(times[c] * 1000);

                plans.push({
                    rate: rates[c],
                    user: users[c],
                    date: date
                });

                info.push(rates[c] + " " + users[c] + " " + date);
            }

            logger.info('LotObject', 'createPlan', 'plan:', "\n\n" + info.join("\n") + "\n");

            return next();
        },
        function(next) {

            return that.plansRemove(next);
        },
        function(next) {

            return that.plansCreate(plans, next);
        }
    ], function(err) {

        logger.info('LotObject', 'createPlan', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'createPlan', that.getId(), err);
        }

        return callback();
    });
};

LotObject.prototype.genPriceOptimal = function(left_price_percent, is_last_bot) {

	var pattern_price_good = [];

	for (var k in config.auction.pattern.price) {

		var sum = _.sum(config.auction.pattern.price[k]);

		if (
			is_last_bot ? (
				(left_price_percent === 0 && sum === 0) || // Если оставшийся процент цены обнулен и дергающих ставок в плане нет
				(sum >= left_price_percent && sum <= left_price_percent * (1 + this.product.data.rateRvMorePercent / 100)) // Если сумма дергающих шаблона превышает оптимальную цену, но не более чем на заданный процент
			) : config.auction.pattern.price[k].length <= 1
		) {
			pattern_price_good.push(config.auction.pattern.price[k]);
		}
	}

	return pattern_price_good.length === 0 && !is_last_bot ? this.genPriceOptimal(left_price_percent, true) : pattern_price_good[_.random(0, pattern_price_good.length - 1)];
};

LotObject.prototype.genRateOptimal = function(pattern_price_optimal, time_left, is_last_bot) {

	var pattern_rate_good = [];

	for (var k in config.auction.pattern.rate) {

		var rv_quant = String(config.auction.pattern.rate[k]).match(/O/g);

		rv_quant = rv_quant ? rv_quant.length : 0;

		if (
			(rv_quant === pattern_price_optimal.length) && // во-первых количество ставок должно быть равно оптимальному
			(
				time_left === 0 ||
				time_left > (config.auction.pattern.rate[k].length * this.product.data.rateGapMin + config.auction.pattern.rate[k].length)
			) && // во-вторых, времени не осталось, либо его осталось больше чем сумма минимальных пауз между ставками
			(
				is_last_bot ||
				(time_left > this.product.data.rateTimeRatePlenty ? true : (time_left > this.product.data.rateTimeRate1 ? config.auction.pattern.rate[k].length <= 3 : config.auction.pattern.rate[k].length <= 1))
			) // в-третьих, последним ставил бот, либо количество ставок в плане более 3х если времени больше чем 15 секунд или не более 1ой
		) {
			pattern_rate_good.push(config.auction.pattern.rate[k]);
		}
	}

	var ret = '';

	if (pattern_rate_good.length === 0) {

		if (!is_last_bot && time_left !== 0) {

			ret = this.genRateOptimal(pattern_price_optimal, time_left, true);

			if (ret.length === 0) ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, is_last_bot);
		} else if (!is_last_bot) {
			ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, true);
		} else if (time_left !== 0) {
			ret = this.genRateOptimal(pattern_price_optimal, time_left + 15, is_last_bot);
		}
	}

	if (ret.length === 0 && pattern_rate_good.length > 0) {
		ret = pattern_rate_good[_.random(0, pattern_rate_good.length - 1)];
	}

	return ret;
};

LotObject.prototype.plansCreate = function(plans, callback) {

    logger.info('LotObject', 'plansCreate', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            if (!plans.length) {
                return next();
            }

            return async.eachSeries(plans, function(_plan, _next) {

                var plan = new PlanObject({
                    tokdeck: that.tokdeck,
                    auction: that,
                    lot: that,
                    rate: _plan.rate,
                    user: _plan.user,
                    date: _plan.date
                });

                that.plans.push(plan);

                return plan.start(_next);
            }, next);
        }
    ], function(err) {

        logger.info('LotObject', 'plansCreate', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'plansCreate', that.getId(), err);
        }

        return callback();
    });
};

LotObject.prototype.plansRemove = function(callback) {

    logger.info('LotObject', 'plansRemove', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            return async.eachSeries(that.plans, function(plan, _next) {

                return plan.stop(_next);
            }, next);
        }
    ], function(err) {

        logger.info('LotObject', 'plansRemove', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'plansRemove', that.getId(), err);
        } else {
            that.plans = [];
        }

        return callback();
    });
};

LotObject.prototype.start = function(callback) {

    logger.info('LotObject', 'start', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            if (that.started) {
                return next();
            }

            that.started = true;

            return next();
        },
        function(next) {

            return that.createPlan(next);
        }
    ], function(err) {

        logger.info('LotObject', 'start', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'start', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

LotObject.prototype.stop = function(callback) {

    logger.info('LotObject', 'stop', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            return that.plansRemove(next);
        }
    ], function(err) {

        logger.info('LotObject', 'stop', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'stop', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

LotObject.prototype.end = function(callback) {

    logger.info('LotObject', 'end', 'start', this.getId());

    var that = this;

    return async.series([
        function(next) {

            if (that.ended) {
                return next();
            }

            that.ended = true;

            return next();
        }
    ], function(err) {

        logger.info('LotObject', 'end', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'end', that.getId(), err);
        }

        if (callback) {
            return callback();
        }
    });
};

LotObject.prototype.rate = function(param, callback) {

    logger.info('LotObject', 'rate', 'start', this.getId());

    var that = this;
    var utilService = new UtilService();
    var user = param.user;
    var rate = param.rate;

    return async.series([
        function(next) {

            var dirty = false;
            var isUserBot = typeof that.auction.tokByOid[user] !== 'undefined';
            var rateClean = 0;
            var now = new Date();

            if (that.ended) {
                return next({code: 57, message: 'Lot closed'});
            }

            if (!that.started) {
                return next({code: 56, message: 'Lot not started'});
            }

            if (that.data.rateArchive.length > 0 && that.data.rateArchive[that.data.rateArchive.length - 1].user === user) {
                return next({code: 52, message: 'Your bid is last'});
            }

            if (that.data.price < rate) {
                rateClean = rate - that.d.price;
            }

            var rateMin = utilService.percentParam(that.product.data.rateMin, that.product.data.price);

            if (rateClean < rateMin) {
                return next({code: 54, message: 'Bid can not be less than ' + rateMin, rateMin: rateMin});
            }

            var rateMax = utilService.percentParam(that.product.data.rateMax, that.product.data.price);

            if (rateClean > rateMax) {
                return next({code: 55, message: 'Bid can not be greater then ' + rateMax, rateMax: rateMax});
            }

            if (rateClean / that.product.data.rateMultiple !== Math.floor(rateClean / that.product.data.rateMultiple)) {
                return next({code: 53, message: 'Bid should be a multiple of ' + that.product.data.rateMultiple, rateMultiple: that.product.data.rateMultiple});
            }

            if (!isUserBot) {
                dirty = true;
            }

            that.tokdeck.queue.remove('lot_end_' + that.getId(), that.data.timeEnd);
            that.data.timeEnd = new Date(that.data.timeEnd.getTime() + that.product.data.timeAdd * 1000);
            that.data.price = that.data.price + rateClean;

            that.data.rateArchive.push({
                rate: rateClean,
                user: user,
                date: now
            });

            that.save(['timeEnd', 'price', 'rateArchive']);

            that.planEnd();

            if (dirty) {
                that.createPlan();
            }

            return next();
        }
    ], function(err) {

        logger.info('LotObject', 'rate', 'finish', that.getId());

        if (err) {
            logger.error('LotObject', 'rate', that.getId(), err);
        }

        return callback();
    });
};


LotObject.prototype.getId = function() {

    return this.data.id || this.id;
};

module.exports = LotObject;