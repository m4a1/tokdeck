'use strict';

var config = require('../config');
var _ = require('lodash');
var async = require('async');
var logger = require('../lib/logger')({
    prefix: 'object.product'
});

var ProductObject = function(param) {

    this.data = {};

    this.tokdeck = param.tokdeck;
    this.auction = param.auction;
    this.model = param.model;

    this.setData(this.model);
};

ProductObject.prototype.setData = function(model) {

    var modelNonEmpty = _.omitBy(model.toJSON(), function(value) {
        return !value;
    });

    this.data = _.merge({}, config.auction.def, this.auction.data, modelNonEmpty);
};

module.exports = ProductObject;