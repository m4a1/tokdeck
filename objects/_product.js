'use strict';

var common = require('../helpers/common'),
	_ = require('lodash');

var AuctionGoods = function(auction, data) {
	this.d = {};
	this.a = auction.a;
	this.auction = auction;
	this.model = data;
	var c_goods = this.a.config.bidcloud.goods_default;
	var field = _.union(
		['oid', 'price', 'active'],
		Object.keys(c_goods)
	);
	var data_auction = common.prepareData(field, auction.d);
	field.forEach(function(k) {
		this.d[k] = typeof data[k] !== 'undefined' && String(data[k]).length > 0 ? data[k] : (typeof data_auction[k] !== 'undefined' && c_goods[k] === null ? data_auction[k] : c_goods[k]);
	}.bind(this));
};

AuctionGoods.prototype.init = function(callback) {
	callback();
};

AuctionGoods.prototype.start = function(callback) {
	if (this.d.state === 'running') {
		callback();
	}
	else callback();
};

AuctionGoods.prototype.stop = function(callback) {
	callback();
};

module.exports = AuctionGoods;