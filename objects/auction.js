'use strict';

var config = require('../config');
var _ = require('lodash');
var async = require('async');
var ProductObject = require('../objects/product');
var TokObject = require('../objects/tok');
var LotObject = require('../objects/lot');
var logger = require('../lib/logger')({
    prefix: 'object.auction'
});

var AuctionObject = function(param) {

    this.tokByOid = {};
    this.productById = {};
    this.productByOid = {};
    this.lotById = {};
    this.data = {};

    this.tokdeck = param.tokdeck;
    this.model = param.model;

    this.setData(this.model);
};

AuctionObject.prototype.setData = function(model) {

    var modelNonEmpty = _.omitBy(model.toJSON(), function(value) {
        return !value;
    });

    delete modelNonEmpty.Products;
    delete modelNonEmpty.Toks;
    delete modelNonEmpty.Lots;

    this.data = _.extend({}, config.auction.def, modelNonEmpty);
};

AuctionObject.prototype.save = function(fields) {

};

AuctionObject.prototype.init = function(param, callback) {

    logger.info('AuctionObject', 'init', 'start', this.model.id);

    var that = this;

    return async.series([
        function(next) {
            // products
            return async.eachSeries(that.model.Products, function(model, _next) {

                return that.productInit(model, _next);
            }, function() {

                return next();
            });
        },
        function(next) {
            // toks
            return async.eachSeries(that.model.Toks, function(model, _next) {

                return that.tokInit(model, _next);
            }, function() {

                return next();
            });
        },
        function(next) {
            // toks
            return async.eachSeries(that.model.Lots, function(model, _next) {

                return that.lotInit(model, _next);
            }, function() {

                return next();
            });
        }/*,
        function(cb) {
            // notify
            this.notify_array = this.model.notifies;
            cb();
        }*/
    ], function(err) {

        logger.info('AuctionObject', 'init', 'finish', that.model.id);

        if (err) {
            logger.error('AuctionObject', 'init', that.model.id, err);
        }

        return callback();
    });
};

AuctionObject.prototype.run = function(param, callback) {

    logger.info('AuctionObject', 'run', 'start', this.data.id);

    logger.info('AuctionObject', 'run', 'finish', this.data.id);

    return callback();
};

AuctionObject.prototype.productInit = function(model, callback) {

    logger.info('AuctionObject', 'productInit', 'start', model.id);

    var that = this;

    return async.series([
        function(next) {

            that.productById[model.id] = that.productByOid[model.oid] = new ProductObject({
                model: model,
                tokdeck: that.tokdeck,
                auction: that
            });

            return next();
        }
    ], function(err) {

        logger.info('AuctionObject', 'productInit', 'finish', model.id);

        if (err) {
            logger.error('AuctionObject', 'productInit', model.id, err);
        }

        return callback();
    });
};

AuctionObject.prototype.tokInit = function(model, callback) {

    logger.info('AuctionObject', 'tokInit', 'start', model.id);

    var that = this;

    return async.series([
        function(next) {

            that.tokByOid[model.oid] = new TokObject({
                model: model,
                tokdeck: that.tokdeck,
                auction: that
            });

            return next();
        }
    ], function(err) {

        logger.info('AuctionObject', 'tokInit', 'finish', model.id);

        if (err) {
            logger.error('AuctionObject', 'tokInit', model.id, err);
        }

        return callback();
    });
};

AuctionObject.prototype.lotInit = function(model, callback) {

    logger.info('AuctionObject', 'lotInit', 'start', model.id);

    var that = this;

    return async.series([
        function(next) {

            that.lotById[model.id] = new LotObject({
                model: model,
                tokdeck: that.tokdeck,
                auction: that
            });

            return next();
        },
        function(next) {

            return that.lotById[model.id].init(next);
        },
        function(next) {

            return that.lotById[model.id].run(next);
        }
    ], function(err) {

        logger.info('AuctionObject', 'lotInit', 'finish', model.id);

        if (err) {
            logger.error('AuctionObject', 'lotInit', model.id, err);
        }

        return callback();
    });
};

module.exports = AuctionObject;