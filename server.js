'use strict';

/* global __dirname, app */

process.setMaxListeners(0);

var logger = require('./lib/logger')({
    prefix: 'server'
});

var config = require('./config');
var _ = require('lodash');
var path = require('path');
var async = require('async');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var winston = require('winston');
var ejs  = require('ejs');
var qs = require('querystring');
var SwaggerDefinition = require('./api/swagger');
var SwaggerExpress = require('swagger-express-mw');
var SwaggerUi = require('swagger-tools/middleware/swagger-ui');
var WebController = require('./controllers/web');
var userApi = require('./api/controllers/user');
var AuthService = require('./services/auth');
var SocketService = require('./services/socket');
var MockService = require('./services/mock');
var TokdeckRunner = require('./runners/tokdeck');
var swaggerDefinition = new SwaggerDefinition();

global.app = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(session(config.session));
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.set('view cache', config.viewCache);

app.use('/swagger-ui/dist', express.static(__dirname + '/bower_components/swagger-ui/dist'));

app.use(function (req, res, next) {

    if (String(req.hostname).indexOf('api.') === -1) {
        return next();
    }

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,Set-Cookie,sid');
    res.header('Content-Type', 'application/json; charset=utf-8');
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');
    res.header('Expires', 0);

    return next();
});

passport.serializeUser(function(user, done) {
    return done(null, user);
});

passport.deserializeUser(function(user, done) {
    return done(null, user);
});

(new WebController).init();

var swaggerConfig = {
    appRoot: __dirname,
    swagger: swaggerDefinition.getJSON(),
    swaggerControllerPipe: 'swagger_controllers',
    bagpipes: {
        _router: {
            name: 'swagger_router',
            mockMode: false,
            mockControllersDirs: [ 'api/mocks' ],
            controllersDirs: [ 'api/controllers' ]
        },
        _swagger_validate: {
            name: 'swagger_validator',
            validateReponse: true
        },
        swagger_controllers: [
            {
                onError: 'error_handler'
            },
            'cors',
            'swagger_security',
            '_swagger_validate',
            'express_compatibility',
            '_router'
        ]
    },
    swaggerSecurityHandlers: {
        auction_auth: function(req, authOrSecDef, scopesOrApiKey, callback) {

            var authService = new AuthService();

            return authService.oauthAuthorize({
                req: req,
                scope: scopesOrApiKey
            }, function(err) {

                if (err) {
                    return callback({code: 403, message: err.message ? err.message : 'Unknown error'});
                }

                return callback();
            });
        }
    }
};

SwaggerExpress.create(swaggerConfig, function(err, swaggerExpress) {

    if (err) { throw err; }

    swaggerExpress.register(app);
});

app.get('/docs', function(req, res) {

    res.setHeader('Content-Type', 'text/html');

    return res.render('swagger', {
        url: config.apiUrl
    });
});

app.get('/docs/o2c', function(req, res) {

    res.setHeader('Content-Type', 'text/html');

    return res.render('o2c');
});

app.get('/docs/api', function(req, res) {

    return res.json(swaggerDefinition.getJSON());
});

app.get('/v1/oauth', function(req, res) {

    res.setHeader('Content-Type', 'text/html');

    req.query.url = '/v1/oauth?' + qs.stringify(req.query);

    return res.render('oauth', req.query);
});

app.post('/v1/oauth', function(req, res) {

    var swaggerPrams = {};
    var authService = new AuthService();

    var params = _.extend({
        provider: 'local'
    }, req.query, req.body);

    for (var k in params) {

        swaggerPrams[k] = {
            value: params[k]
        };
    }

    req.swagger = {
        params: swaggerPrams
    };

    req.callback = function(err, user) {

        var result = null;

        return async.series([
            function(next) {

                return next(err);
            },
            function(next) {

                delete params.username;
                delete params.password;

                params.userId = user.id;

                return authService.oauthCreateToken(params, function(err, _result) {

                    if (err) {
                        return next(err);
                    }

                    result = _result;

                    return next();
                });
            }
        ], function(err) {

            if (err) {

                var params = _.extend({}, req.query, {
                    error: err.message ? err.message : 'Unhandled error',
                    url: '/v1/oauth?' + qs.stringify(req.query)
                });

                res.setHeader('Content-Type', 'text/html');

                return res.render('oauth', params);
            }

            return res.redirect(result.redirect_uri + '?access_token=' + result.token + '&state=' + result.state);
        });
    };

    return userApi.signin(req, res);
});

// server start
app.listen(config.server.port);
logger.info('Server', 'listening on port:', config.server.port);

// socket start
var io = require('socket.io')(config.socket.port);
var socketService = new SocketService();
socketService.init(io);

// Auction runner
var tokdeckRunner = new TokdeckRunner();

(new MockService).init({
    active: true
}, function() {

    return tokdeckRunner.run();
});
