'use strict';

var config = require('../config');
var async = require('async');
//var Session = require('../models').Session;
var Auction = require('../models').Auction;
var Product = require('../models').Product;
var Tok = require('../models').Tok;
var Lot = require('../models').Lot;
var SessionObject = require('../objects/session');
var AuctionObject = require('../objects/auction');
var Queue = require('../services/queue');
var logger = require('../lib/logger')({
    prefix: 'runner.tokdeck'
});

var TokdeckRunner = function() {

    this.started = false;
    this.sessionByOid = {};
    this.auctionById = {};
	this.queue = new Queue();
};

TokdeckRunner.prototype.run = function(callback) {

    logger.info('TokdeckRunner', 'start');

    var that = this;

    return async.series([
        /*function(next) {

            return that.sessionsInit(next);
        },*/
        function(next) {

            return that.auctionsInit(next);
        },
        function(next) {

            return that.socketsInit(next);
        }
    ], function(err) {

        logger.info('TokdeckRunner', 'finish', "\n");

        if (err) {

            logger.error('TokdeckRunner', err);

            return callback ? callback(err) : null;
        }

        that.started = true;

        that.queue.tick(100);

        if (callback) {
            return callback();
        }
    });
};
/*
TokdeckRunner.prototype.sessionsInit = function(callback) {

    logger.info('TokdeckRunner', 'sessionsInit', 'start');

    var that = this;
    var list = [];

    return async.series([
        function(next) {

            return Session.findAll({}).then(function(_list) {

                if (_list) {
                    list = _list;
                }

                return next() || null;
            }).catch(function(error) {

                return next({code: 0, message: 'DB error', original: error}) || null;
            });
        },
        function(next) {

            if (!list.length) {
                return next();
            }

            return async.eachSeries(list, function(model, _next) {

                return that.sessionInit(model, _next);
            }, next);
        }
    ], function(err) {

        logger.info('TokdeckRunner', 'sessionsInit', 'finish');

        if (err) {
            logger.error('TokdeckRunner', 'sessionsInit', err);
        }

        return callback();
    });
};

TokdeckRunner.prototype.sessionInit = function(model, callback) {

    logger.info('TokdeckRunner', 'sessionInit', 'start', model.id);

    var that = this;

    return async.series([
        function(next) {

            that.sessionByOid[model.oid] = new SessionObject({
                tokdeck: that,
                model: model
            });

            return next();
        }
    ], function(err) {

        logger.info('TokdeckRunner', 'sessionInit', 'finish', model.id);

        if (err) {
            logger.error('TokdeckRunner', 'sessionInit', model.id, err);
        }

        return callback();
    });
};
*/
TokdeckRunner.prototype.auctionsInit = function(callback) {

    logger.info('TokdeckRunner', 'auctionsInit', 'start');

    var that = this;
    var list = [];

    return async.series([
        function(next) {

            return Auction.findAll({
                include: [
                    {
                        model: Product
                    },
                    {
                        model: Tok
                    },
                    {
                        model: Lot
                    }
                ]
            }).then(function(_list) {

                if (_list) {
                    list = _list;
                }

                return next() || null;
            }).catch(function(err) {

                return next({code: 0, message: 'DB error', original: err}) || null;
            });
        },
        function(next) {

            if (!list.length) {
                return next();
            }

            return async.eachSeries(list, function(model, _next) {

                return that.auctionInit(model, _next);
            }, next);
        }
    ], function(err) {

        logger.info('TokdeckRunner', 'auctionsInit', 'finish');

        if (err) {
            logger.error('TokdeckRunner', 'auctionsInit', err);
        }

        return callback();
    });
};

TokdeckRunner.prototype.auctionInit = function(model, callback) {

    logger.info('TokdeckRunner', 'auctionInit', 'start', model.id);

    var that = this;

    return async.series([
        function(next) {

            that.auctionById[model.id] = new AuctionObject({
                tokdeck: that,
                model: model
            });

            return next();
        },
        function(next) {

            return that.auctionById[model.id].init({}, next);
        },
        function(next) {

            return that.auctionById[model.id].run({}, next);
        }
    ], function(err) {

        logger.info('TokdeckRunner', 'auctionInit', 'finish', model.id);

        if (err) {
            logger.error('TokdeckRunner', 'auctionInit', model.id, err);
        }

        return callback();
    });
};

TokdeckRunner.prototype.socketsInit = function(callback) {

    return callback();
};

TokdeckRunner.prototype.addQueue = function(date, key, callback) {

    return callback();
};

TokdeckRunner.prototype.removeQueue = function(date, key) {

};

module.exports = TokdeckRunner;