'use strict';

/* global app */

var config = require('../config');
var async = require('async');
var _ = require('lodash');
var integrationStrategyService = require('../services/integrations');
var MailService = require('../services/mail');
var WebService = require('../services/web');
var CacheService = require('../services/cache');
var PageService = require('../services/page');
var logger = require('../lib/logger')({
    prefix: 'controller.web'
});

var WebController = function() {};

WebController.prototype.page = function(req, res, _next) {

    if (req.path && (req.path.match(/^\/v1\//gi) || req.path.match(/^\/integration\//gi) || req.path.match(/^\/docs/gi))) {
        return _next();
    }

    var webService = new WebService();
    var cacheService = new CacheService();
    var pageService = new PageService();
    var stitle = req.params.stitle;
    var langId = req.user ? req.user.langId : null;
    var pages = null;
    var param = {
        uri: req.path,
        frontendType: req.params.frontendType || 'main',
        user: req.user,
        appOn: ['remind', 'signin', 'signup', 'dashboard'].indexOf(stitle) !== -1
    };
    var html = '';

    return async.series([
        function(next) {

            return cacheService.get({
                type: 'page_' + langId
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                if (result) {
                    pages = result;
                }

                return next();
            });
        },
        function(next) {

            if (pages) {
                return next();
            }

            return pageService.getPage({
                langId: langId
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                if (!result) {
                    return next({code: 0, message: 'Locale not loaded'});
                }

                pages = result;

                return cacheService.set({
                    type: 'page_' + langId,
                    data: result
                }, function(err) {
                    return next(err);
                });
            });
        },
        function(next) {

            if (!pages || !pages[stitle]) {
                return next({code: 0, message: 'Page not found'});
            }

            _.extend(param, pages[stitle], {
                pages: pages
            });

            return next();
        },
        function(next) {

            return webService.render({
                langId: langId,
                template: param.message,
                data: param
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                html = result.html;

                return next();
            });
        }
    ], function() {

        return res.send(html);
    });
};

WebController.prototype.renderTemplate = function (req, res) {

    var mailService = new MailService();
    var html = null;
    var template = req.query.template;

    return async.series([
        function(next) {

            return mailService.renderMail({
                template: template,
                data: req.query
            }, function(error, _html) {

                if (error) {
                    return next(error);
                }

                html = _html;

                return next();
            });
        }
    ], function(error) {

        res.setHeader('Content-Type', 'text/html');

        if (error) {
            return res.status(500).send(error.message ? error.message : 'Unknown error');
        }

        return res.send(html);
    });
};

WebController.prototype.integration = function(req, res) {

    logger.info('WebController', 'integration', 'start');

    var webService = new WebService();
    var langId = req.user ? req.user.langId : null;
    var integrationStrategy = null;
    var provider = req.params.provider;
    var auth = null;
    var html = '';
    var param = {
        uri: req.path,
        frontendType: req.params.frontendType || 'main',
        user: req.user,
        appOn: false
    };

    return async.series([
        function(next) {

            if (auth) {
                return next();
            }

            var IntegrationStrategy = integrationStrategyService.use(provider);

            if (!IntegrationStrategy) {
                return next({code: 0, message: 'No IntegrationStrategy found', provider: provider});
            }

            integrationStrategy = new IntegrationStrategy();

            var params = {
                provider: provider
            };

            params.req = req;
            params.res = res;

            return integrationStrategy.auth(params, function(err, result) {

                if (err) {
                    return next(err);
                }

                if (result.redirectUrl) {
                    return res.redirect(result.redirectUrl);
                }

                auth = result.auth;

                return next();
            });
        },
        function(next) {
console.log(auth);
            param.message = '<div>1111</div>';

            return next();
        },
        function(next) {

            return webService.render({
                langId: langId,
                template: param.message,
                data: param
            }, function(err, result) {

                if (err) {
                    return next(err);
                }

                html = result.html;

                return next();
            });
        }
    ], function() {

        return res.send(html);
    });
};

WebController.prototype.init = function() {

    var that = this;

    app.get('/', function(req, res) {
        req.params.stitle = 'home';
        req.params.frontendType = 'landing';
        that.page(req, res);
    });
    app.get('/:stitle', this.page);
    app.get('/:stitle/:q', this.page);
    app.get('/:stitle/:q/:q1', this.page);
    app.get('/:stitle/:q/:q1/:q2', this.page);
    app.get('/:stitle/:q/:q1/:q2/:q3', this.page);
    app.get('/:stitle/:q/:q1/:q2/:q3/:q4', this.page);
    app.get('/mail/template', this.renderTemplate);
    app.get('/integration/:provider', this.integration);
};

module.exports = WebController;
