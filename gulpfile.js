'use strict';

var async = require('async');
var fs = require('fs');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gzip = require('gulp-gzip');
var html2js = require('gulp-html2js');
var compass = require('gulp-compass');
var gulpsync = require('gulp-sync')(gulp);
var watch = require('gulp-watch');
var exec = require('gulp-exec');
var path = require('path');
var batch = require('gulp-batch');
var replace = require('gulp-replace');
var gif = require('gulp-if');
var rename = require('gulp-rename');
var env = 'prod';
var buildId = 1;
var inited = false;
var watchTimer = {};
var minimist = require('minimist');
var mocha = require('gulp-mocha');
var options = minimist(process.argv.slice(2));

gulp.task('bower', function () {
    return gulp.src('./')
        .pipe(exec('bower install --allow-root'));
});

gulp.task('get-js', function () {
    gulp.src('./')
        .pipe(exec('git clone https://github.com/fians/Waves.git ./tmp/javascripts/Waves', {
            continueOnError: true
        }));
});

gulp.task('copy', function () {
    gulp.src('./bower_components/font-awesome/fonts/**', {base: './bower_components/font-awesome/fonts'})
        .pipe(gulp.dest('./www/fonts'));
    gulp.src('./bower_components/bootstrap/fonts/**', {base: './bower_components/bootstrap/fonts'})
        .pipe(gulp.dest('./www/fonts'));
    gulp.src('./bower_components/simple-line-icons/fonts/**', {base: './bower_components/simple-line-icons/fonts'})
        .pipe(gulp.dest('./www/fonts'));
});

gulp.task('css', function () {
    return gulp.src('./public/scss/*.scss')
        .pipe(compass({
            css: './tmp/styles',
            sass: './public/styles'
        }))
        .pipe(gulp.dest('./tmp/styles'));
});

gulp.task('html2js', function() {
    return gulp.src([
            './public/views/*.html',
            './public/views/**/*.html',
            './public/views/**/**/*.html'
        ])
        .pipe(html2js({
            outputModuleName: 'template',
            useStrict: true
        }))
        .pipe(concat('template.js'))
        .pipe(gulp.dest('./tmp/javascripts'));
});

gulp.task('concat-css', function() {
    return gulp.src([
            './bower_components/font-awesome/css/font-awesome.css',
            //'./bower_components/simple-line-icons/css/simple-line-icons.css',
            './bower_components/bootstrap/dist/css/bootstrap.css',
            //'./bower_components/switchery/dist/switchery.css',
            //'./bower_components/metrojs/release/MetroJs.Full/MetroJs.css',
            './public/lib/slidepushmenus/css/component.css',
            './bower_components/jquery.uniform/themes/default/uniform.default.css',
            './bower_components/pace/themes/blue/pace-theme-flash.css',
            './tmp/javascripts/Waves/dist/waves.css',
            //'./public/lib/3d-bold-navigation/css/style.css',
            './bower_components/toastr/toastr.css',
            './public/styles/modern.css',
            './public/styles/theme/main.css',
            './tmp/styles/main.css'
        ])
        .pipe(concat('main' + buildId + '.css'))
        .pipe(gulp.dest('./www/styles'));
});

gulp.task('concat-landing-css', function() {
    return gulp.src([
            './bower_components/font-awesome/css/font-awesome.css',
            './bower_components/bootstrap/dist/css/bootstrap.css',
            './bower_components/jquery.uniform/themes/default/uniform.default.css',
            './bower_components/pace/themes/blue/pace-theme-flash.css',
            './bower_components/animate.css/animate.css',
            './public/lib/tabstylesinspiration/css/tabs.css',
            './public/lib/tabstylesinspiration/css/tabstyles.css',
            './public/lib/pricing-tables/css/style.css',
            './public/styles/theme/landing.css',
            './tmp/styles/landing.css'
        ])
        .pipe(concat('landing' + buildId + '.css'))
        .pipe(gulp.dest('./www/styles'));
});

gulp.task('concat-js', function() {

    var config = require('./config');

    return gulp.src([
            './public/javascripts/pre.js',
            './bower_components/jquery/dist/jquery.js',
            //'./bower_components/jquery-ui/jquery-ui.js',
            './bower_components/lodash/lodash.js',
            './bower_components/async/lib/async.js',
            './bower_components/moment/min/moment-with-locales.js',
            './bower_components/moment-timezone/builds/moment-timezone-with-data.js',
            './bower_components/angular/angular.js',
            './bower_components/angular-ui-router/release/angular-ui-router.js',
            './bower_components/angular-resource/angular-resource.js',
            './bower_components/angular-cookies/angular-cookies.js',
            './bower_components/angular-messages/angular-messages.js',
            './bower_components/angular-validation-match/dist/angular-validation-match.js',
            './bower_components/angular-moment/angular-moment.js',
            './bower_components/bootstrap/dist/js/bootstrap.js',
            //'./bower_components/switchery/dist/switchery.js',
            //'./bower_components/classie/classie.js',
            './bower_components/jquery.uniform/jquery.uniform.js',
            './bower_components/slimScroll/jquery.slimscroll.js',
            //'./bower_components/metrojs/release/MetroJs.Full/MetroJs.js',
            './bower_components/node-uuid/uuid.js',
            './bower_components/pace/pace.js',
            './tmp/javascripts/Waves/dist/waves.js',
            './public/lib/3d-bold-navigation/js/modernizr.js',
            './public/lib/3d-bold-navigation/js/main.js',
            './bower_components/blockUI/jquery.blockUI.js',
            './bower_components/toastr/toastr.js',
            './tmp/javascripts/template.js',
            './public/javascripts/modern.js',
            './public/javascripts/config.js',
            './public/javascripts/app.js',
            './public/javascripts/services/configService.js',
            './public/javascripts/services/utilService.js',
            './public/javascripts/services/langService.js',
            './public/javascripts/services/apiService.js',
            './public/javascripts/services/socketService.js',
            './public/javascripts/services/localeService.js',
            './public/javascripts/services/domService.js',
            './public/javascripts/services/userService.js',
            './public/javascripts/services/startService.js',
            './public/javascripts/services/auctionService.js',
            './public/javascripts/services/productService.js',
            './public/javascripts/services/tokService.js',
            './public/javascripts/controllers/remind/remindController.js',
            './public/javascripts/controllers/remind/continueController.js',
            './public/javascripts/controllers/accountaddrssController.js',
            './public/javascripts/controllers/sidebarController.js',
            './public/javascripts/controllers/socialController.js',
            './public/javascripts/controllers/profileController.js',
            './public/javascripts/controllers/passwordController.js',
            './public/javascripts/controllers/signinController.js',
            './public/javascripts/controllers/signupController.js',
            './public/javascripts/controllers/homeController.js',
            './public/javascripts/controllers/auction/auctionController.js',
            './public/javascripts/controllers/auction/listController.js',
            './public/javascripts/controllers/auction/statusController.js',
            './public/javascripts/controllers/auction/activeController.js',
            './public/javascripts/controllers/auction/addController.js',
            './public/javascripts/controllers/auction/editController.js',
            './public/javascripts/controllers/auction/deleteController.js',
            './public/javascripts/controllers/product/productController.js',
            './public/javascripts/controllers/product/listController.js',
            './public/javascripts/controllers/product/activeController.js',
            './public/javascripts/controllers/product/addController.js',
            './public/javascripts/controllers/product/editController.js',
            './public/javascripts/controllers/product/deleteController.js',
            './public/javascripts/controllers/tok/tokController.js',
            './public/javascripts/controllers/tok/listController.js',
            './public/javascripts/controllers/tok/activeController.js',
            './public/javascripts/controllers/tok/addController.js',
            './public/javascripts/controllers/tok/editController.js',
            './public/javascripts/controllers/tok/deleteController.js',
            './public/javascripts/controllers/onboardingController.js'
        ])
        .pipe(concat('main' + buildId + '.js', {newLine: ';'}))
        .pipe(replace(/\%env\%/g, env))
        .pipe(replace(/\%build_id\%/g, buildId))
        .pipe(replace(new RegExp('\'%' + env + '_auction_config%\'', 'g'), JSON.stringify(config.auction.def)))
        .pipe(gif(env === 'prod', uglify()))
        .pipe(gulp.dest('./www/javascripts'))
        .pipe(gif(env === 'prod', gzip()))
        .pipe(gulp.dest('./www/javascripts'));
});

gulp.task('concat-landing-js', function() {
    return gulp.src([
            './bower_components/jquery/dist/jquery.js',
            //'./bower_components/jquery-ui/jquery-ui.js',
            './bower_components/bootstrap/dist/js/bootstrap.js',
            './bower_components/jquery.uniform/jquery.uniform.js',
            './bower_components/slimScroll/jquery.slimscroll.js',
            './bower_components/wow/dist/wow.js',
            './bower_components/pace/pace.js',
            './public/lib/tabstylesinspiration/js/cbpfwtabs.js',
            './public/lib/pricing-tables/js/main.js',
            './public/lib/3d-bold-navigation/js/modernizr.js',
            './public/javascripts/landing.js'
        ])
        .pipe(concat('landing' + buildId + '.js', {newLine: ';'}))
        .pipe(replace(/\%env\%/g, env))
        .pipe(replace(/\%build_id\%/g, buildId))
        .pipe(gif(env === 'prod', uglify()))
        .pipe(gulp.dest('./www/javascripts'))
        .pipe(gif(env === 'prod', gzip()))
        .pipe(gulp.dest('./www/javascripts'));
});

function readBuildId(next) {
    fs.readFile(__dirname + '/.build', function(err, _buildId) {
        _buildId = Number(String(_buildId).trim());
        if (err || !_buildId) {
                return next();
        }
        buildId = _buildId;
        next();
    });
}

function watchDelay(type, fn) {
    clearTimeout(watchTimer[type]);
    watchTimer[type] = setTimeout(fn, 500);
}

gulp.task('build-js', gulpsync.sync(['html2js', 'concat-js', 'concat-landing-js']));
gulp.task('build-css', gulpsync.sync(['css', 'concat-css', 'concat-landing-css']));
gulp.task('build', gulpsync.sync(['bower', 'copy', 'get-js', 'html2js', 'css', 'concat-css', 'concat-js', 'concat-landing-css', 'concat-landing-js']));

gulp.task('default', function () {
    readBuildId(function() {
        gulp.start('build');
        inited = true;
    });
});

gulp.task('development', function () {
        env = 'development';
        gulp.start('default');
});

gulp.task('watch', function () {
        gulp.start('development');

        //js
        watch([
            './public/javascripts/*',
            './public/javascripts/**/*',
            './public/views/*',
            './public/views/**/*',
            './public/views/**/**/*'
        ], function() {
            watchDelay('js', function() {
                gulp.start('build-js');
            });
        });

        //css
        watch([
            './public/styles/*'
        ], function() {
            watchDelay('css', function() {
                gulp.start('build-css');
            });
        });
});

gulp.task('start', function() {
        var options = {
                continueOnError: true,
                pipeStdout: false
        };
        var reportOptions = {
                err: true,
                stderr: true,
                stdout: true
        };
        gulp.src('./')
                .pipe(exec('forever start server.js', options))
                .pipe(exec.reporter(reportOptions));
});

gulp.task('stop', function() {
        var options = {
                continueOnError: true,
                pipeStdout: false
        };
        var reportOptions = {
                err: true,
                stderr: true,
                stdout: true
        };
        gulp.src('./')
                .pipe(exec('forever stop server.js', options))
                .pipe(exec.reporter(reportOptions));
});

gulp.task('test', function() {

    process.env.NODE_ENV = 'test';

    return  gulp.src('./tests/' + (options.suite ? options.suite : '*') + '.js', {
        read: false
    })
    .pipe(mocha({
        reporter: 'spec'
    }))
    .once('error', function () {
        process.exit(1);
    })
    .once('end', function () {
        process.exit();
    });
});