'use strict';

var config = require('../config');

var should = require('should');
var async = require('async');
var _ = require('lodash');

var content = require('./common/content');

var util = require('./common/util');

var MockService = require('../services/mock');
var UserService = require('../services/user');
var User = require('../models').User;

describe('user', function () {

    this.timeout(20000);
    var userId = 1;
    var userService = new UserService();

    before(function (done) {

        return async.series([
            function(next) {

                return (new MockService).init({}, next);
            }
        ], function() {

            return done();
        });
    });

    it('Should create local user', function(done) {

        return async.series([
            function(next) {

                return userService.createUser({
                    provider: 'local',
                    profile: {
                        langId: 1,
                        password: 'test',
                        username: 'test' + userId,
                        email: 'test+' + userId + '@tokdeck.com'
                    }
                }, function(err, user) {

                    should.not.exist(err);

                    user.should.have.property('id', userId);

                    return next();
                });
            },
            function(next) {

                return User.findById(userId).then(function(customer) {

                    customer.should.have.property('email', 'test+' + userId + '@tokdeck.com');

                    userId++;

                    return next();
                }).catch(function(err) {

                    should.not.exist(err);
                });
            }
        ], done);
    });

    it('Should create external (twitter) user', function(done) {

        return async.series([
            function(next) {

                return userService.createUser({
                    provider: 'twitter',
                    profile: {
                        langId: 1,
                        password: 'test',
                        username: 'test' + userId,
                        email: 'test+' + userId + '@tokdeck.com'
                    }
                }, function(err) {

                    should.not.exist(err);

                    return next();
                });
            },
            function(next) {

                return User.findById(userId).then(function(customer) {

                    customer.should.have.property('email', 'test+' + userId + '@tokdeck.com');

                    userId++;

                    return next();
                }).catch(function(err) {

                    should.not.exist(err);
                });
            }
        ], done);
    });

    it('Should not create local user without required fields', function(done) {

        var required = {
            username: 'Username not set',
            email: 'Email not set',
            password: 'Password not set'
        };

        return async.forEachOfSeries(required, function(message, field, next) {

            var empty = {};

            empty[field] = '';

            return userService.createUser({
                provider: 'local',
                profile: _.extend({
                    langId: 1,
                    password: 'test',
                    username: 'test' + userId,
                    email: 'test+' + userId + '@tokdeck.com'
                }, empty)
            }, function(err) {

                should.exist(err);

                err.should.have.property('message', message);

                userId++;

                return next();
            });
        }, done);
    });
});
