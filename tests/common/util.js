'use strict';

var content = require('./content');

var User = require('../../models').User;

var UtilService = {
    user: {
        regular: function(callback) {

            return User.create(content.user.regular).then(function(customer) {
                return callback(null, customer);
            }).catch(function(err) {
                return callback(err);
            });
        }
    }
};

module.exports = UtilService;
