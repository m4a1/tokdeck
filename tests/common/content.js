'use strict';

var TestContent = {
    user: {
        regular: {
            langId: 1,
            password: 'test',
            username: 'test',
            email: 'test@tokdeck.com'
        }
    }
};

module.exports = TestContent;
